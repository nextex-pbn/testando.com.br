module.exports = {                 
    clean: true,
    site: {
        name: "TexGen - Texugo Generator",
        description: "Descrição do site de teste",
        domain: "testando.com.br",
	    theme: "default",
        baseurl: "https://www.testando.com.br",
	    logo: "https://image.flaticon.com/icons/svg/1495/1495070.svg"
    }
}
