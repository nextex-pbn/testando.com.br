---
title: Terapia para TDAH
permalink: terapia-para-tdah
layout: post.hbs
---

# Terapia para TDAH

## O que é terapia para TDAH?

A terapia faz parte do tratamento para a maioria das crianças e adolescentes com diagnóstico de TDAH. As crianças têm reuniões com um terapeuta para ajuda com os problemas que o TDAH pode causar.

Na terapia, as crianças aprendem habilidades que não surgem naturalmente devido ao TDAH, como ouvir e prestando atenção melhor.

Algumas crianças com TDAH também recebem outro tipo de terapia chamada ocupacional terapia. Isso ajuda quando coisas como equilíbrio ou escrita são difíceis por causa de TDAH ou dificuldade de aprendizado.

O tratamento para o TDAH geralmente inclui medicamentos. Os medicamentos aumentam a capacidade do cérebro de prestar atenção, usar o autocontrole e ficar inquieto Menos. A medicina funciona melhor quando a criança também tem um terapeuta para ensinar como usar esses habilidades aprimoradas.

## Por que as crianças precisam de terapia para o TDAH?

A terapia ajuda crianças com TDAH a se sairem melhor na escola e em casa. Por exemplo, eles aprendem como:

* foco nos trabalhos escolares
* ouvir e prestar atenção melhor
* dê-se melhor com os outros
* tem menos problemas de comportamento

Algumas crianças precisam de terapia para ajudar a aliviar emoções difíceis causadas pelo TDAH. Por exemplo, as crianças podem perder a confiança ou sentir que estão decepcionando os outros. Algumas crianças se tornam deprimido, ansioso ou frustrado.

Quando as crianças aprendem habilidades para controlar o TDAH, elas se saem melhor. A terapia também ajuda as crianças sinta-se mais feliz e confiante.

## Como funciona a terapia para TDAH?

Na terapia para TDAH, as crianças aprendem fazendo. Com crianças mais novas, isso significa desenhar, brincar, e conversando. Para crianças mais velhas e adolescentes, um terapeuta compartilhará atividades e ideias para desenvolver as habilidades de que precisam, como bons hábitos de estudo.

Na terapia para TDAH, um relacionamento positivo com o terapeuta ajuda as crianças a se sentirem encorajadas e com suporte à medida que aprendem.

Dependendo da idade da criança, um terapeuta pode se reunir com a criança e os pais juntos ou com a criança sozinha. Quando os pais trabalham com crianças em sessões de terapia, eles aprendem dicas e ideias para manter as aulas em casa.

Os pais podem ajudar os filhos a usar e praticar o que aprenderam. As crianças constroem confiança e autoestima à medida que adquirem novas habilidades.

## O que acontece na terapia para TDAH?

No início, o terapeuta do seu filho conversará com você e fará perguntas. Ouvir suas respostas ajuda os terapeutas a aprender mais sobre seu filho. Juntos você fará metas para o que deseja melhorar.

Pais e filhos geralmente se encontram com o terapeuta uma vez por semana, durante alguns meses.

As sessões de terapia incluem atividades como:

* Falar e ouvir. Os terapeutas ensinam as crianças a falar sobre seus sentimentos. Isso ajuda as crianças a perceber seus sentimentos e expressá-los em palavras de ações. Falar e ouvir ajuda as crianças a se sentirem compreendidas e prontas para aprender. isto também ajuda as crianças a aprenderem a prestar atenção e ouvir melhor.
* Brincando com um propósito. Para as crianças, brincar pode ensinar autocontrole - como esperar para dar uma volta. Os terapeutas podem usar jogos que ensinam as crianças a desacelerar, siga as instruções e tente novamente, em vez de perder a paciência ou desistir. Jogar é também uma forma de as crianças aprenderem a planejar, organizar e guardar as coisas.
* Fazer atividades que ensinam lições. Os terapeutas podem dar aulas sobre emoções, organização do trabalho escolar, estudo ou compreensão dos outros. Atividades e planilhas ajudam a tornar essas aulas divertidas.
* Praticando novas habilidades. Um terapeuta pode ensinar às crianças habilidades como atenção plena e exercícios de respiração. Essas habilidades podem treinar a atenção e acalmar a mente e o corpo. As sessões de terapia podem ser um momento para praticar essas habilidades.
* Resolução de problemas. Terapeutas perguntarão sobre problemas de TDAH na escola e em casa. Eles vão conversar com as crianças e os pais sobre como resolver esses problemas.

## Por quanto tempo as crianças fazem terapia para TDAH?

A duração da terapia depende de seus objetivos. Na maioria das vezes, um terapeuta vai querer se encontrar com seu filho uma vez por semana durante alguns meses.

## Como os pais podem ajudar?

Ter TDAH não é culpa dos filhos - ou dos pais. Mas lá são coisas que as crianças e os pais podem aprender na terapia para ajudar a melhorar.

Veja como você pode ajudar:

* Encontre um terapeuta com quem seu filho se sinta confortável. Pergunte aos cuidados de saúde do seu filho equipe para ajudá-lo a encontrar um. Ou acesse o site CHADD para encontrar alguém perto de você.
* Leve seu filho a todas as visitas recomendadas. Aprender novas habilidades é um hábito que as crianças precisam acompanhar até que aprendam.
* Trabalhe com o terapeuta do seu filho para saber como responder melhor aos comportamentos de TDAH.
* Pergunte como você pode ajudar seu filho a praticar em casa. Existem jogos que você pode jogar ou habilidades que você pode ajudar seu filho a aprender?
* Pais com paciência e cordialidade.