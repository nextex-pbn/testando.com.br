---
title: Paternidade de um adolescente com TDAH
permalink: paternidade-de-um-adolescente-com-tdah
layout: post.hbs
---

# Paternidade de um adolescente com TDAH

Isso soa como seu filho adolescente?

* distrai-se facilmente
* parece nunca ouvir
* não consigo ficar parado
* desorganizado ou bagunçado
* age sem pensar
* tem explosões emocionais

Mesmo que você saiba que esses comportamentos são decorrentes do TDAH de seu filho, você pode se sentir frustrado, envergonhado ou desrespeitado quando eles acontecem. Paternidade um adolescente com TDAH é um desafio. Requer paciência extra. Adolescentes com TDAH estão se tornando mais independente. Mas eles ainda precisam da orientação, ajuda e apoio dos pais.

## O que os pais podem fazer

Saiba mais sobre TDAH. Recapitule o que você já sabe sobre ADHD. Aprenda tudo que puder. Isso pode ajudá-lo a se sentir mais paciente e menos frustrado com o comportamento do seu filho. Isso ajuda você a lembrar que adolescentes com TDAH não estão "sendo difíceis" de propósito. Os adolescentes com TDAH podem aprender a administrar sua atenção e energia. Eles podem faça melhor quando eles têm a ajuda de pais, professores e terapeutas.

Saiba como o TDAH afeta seu filho adolescente. Pense nas maiores dificuldades seu filho adolescente tem por causa do TDAH. Em seguida, pense nas habilidades que seu filho precisa aprender que pode reduzir esses problemas. Por exemplo:

* Adolescentes que são hiperativos podem precisar aprender a desacelerar de pressa. Eles podem precisar aprender maneiras de se acalmar fisicamente ou queimar o excesso energia.
* Adolescentes impulsivos podem precisar aprender a interromper menos, esperar mais pacientemente ou pense antes de agir de maneira arriscada ou descuidada. Eles pode precisar aprender a acalmar suas emoções perturbadas.
* Adolescentes com problemas de atenção podem precisar desenvolver habilidades para planejar, estudar e reduzir a distração. Eles podem precisar de habilidades para ajudá-los organizar suas coisas, limpar, concluir tarefas ou projetos ou chegar na hora certa.

Converse sobre TDAH e metas. Ajude seu filho a entender ADHD. Conversando com adolescentes sobre como o TDAH os afeta na escola, em casa e com os amigos realmente ajuda. Mostre compreensão. Lembre seu filho que ter TDAH não é uma falha. Ao mesmo tempo, seja claro sobre o que você quer que seu filho adolescente trabalhar em. Ajude os adolescentes a verem que é seu trabalho gerenciar sua atenção, energia, ações e emoções - e isso você vai ajudar. Faça metas claras e realista. Comece trabalhando em uma coisa.

Ofereça ajuda prática. O quarto do seu filho está tão bagunçado que ela não consegue encontrar seu dever de casa ou seus sapatos? Se seu filho não tem habilidades de organização por causa do TDAH, não adianta gritar ou dizer: "limpe isso!" Em vez disso, ajude-a a aprender como para limpar. Você pode ter que fazer isso juntos no início. Você pode ter que descobrir maneiras de organizar as coisas e planejar lugares para ir. Trabalhem nisso pacientemente juntos. Se possível, encontre uma maneira de torná-lo divertido. Saiba que as coisas provavelmente ficarão confusas novamente. Planeje repetir esse processo com frequência. É preciso prática para aprender uma nova habilidade.

Ajude seu filho a desenvolver habilidades sociais. Os adolescentes podem não perceber que O TDAH pode afetar seus relacionamentos. Se os adolescentes interrompem com muita frequência, fale demais, não ouçam bem, ou ajam de maneiras que pareçam autoritárias ou intrusivas, eles colocarão outros pessoas desligadas. Ajude seu filho a perceber quando os comportamentos podem afetar as amizades. Não culpe, mas diga que isso pode ser parte do TDAH. Diga: "Eu sei que você não quer interromper. O TDAH torna difícil esperar quando você quer dizer algo. E eu conheço seus sentimentos se machucar quando seu amigo lhe disser para parar de interromper. "Então, ajude seu filho a pensar de uma nova habilidade para praticar - como esperar para falar ou ouvir mais. Seja específico sobre como e quando experimentar.

Continue o tratamento do seu filho para o TDAH. Tratamento para TDAH geralmente inclui medicamentos, terapia, orientação dos pais e apoio escolar. Se o seu filho adolescente foi diagnosticado e tratado para TDAH em uma idade jovem, suas necessidades provavelmente mudou. Trabalhe com o médico, terapeuta e equipe da escola do seu filho para manter identificando e atendendo a novas necessidades e metas.

Atualize o IEP. Se seu filho tem um IEP, certifique-se de que ele seja atualizado para o ensino médio. Isso permite que os professores forneçam qualquer extras que seu filho pode precisar - como aulas particulares, mais tempo para concluir o trabalho, espaços de trabalho mais silenciosos ou lugares sentados com menos distrações.

Mantenha um relacionamento positivo entre pais e filhos. Seja encorajador. Preste mais atenção ao que seu filho está fazendo bem do que aos problemas. Corrigir seu filho adolescente de uma forma calma e solidária. Ajude adolescentes com TDAH a aprender como agir ou o quê para fazer antes de fazê-lo. Isso é melhor do que reagir após o fato ao que eles deveriam não fez.

Evite repreender, culpar, irritar ou dar sermões. Estes provavelmente fazer com que seu filho se desligue do que você diz. Adolescentes com TDAH costumam ser sensíveis a críticas. Eles podem ficar chateados, com raiva ou magoados quando criticados ou punidos. Essas emoções fortes pode impedi-los de realmente ouvir a mensagem que você está tentando transmitir. Encontre ensinável momentos em que vocês dois estão se sentindo calmos.

Ajude os adolescentes a desenvolver (e apreciar) seus pontos fortes. Adolescentes com O TDAH muitas vezes parece que estão decepcionando os outros ou que não conseguem fazer nada certo. Mas as pessoas com TDAH têm muitos pontos fortes. Alguns de seus pontos fortes vêm com o TDAH, como pensamento rápido, criatividade, diversão ou espontaneidade. Ajude adolescentes a descobrir seus pontos fortes e encontrar uma maneira de usá-los em seu dia a dia. Quando os adolescentes usam seus pontos fortes - e saber que um pai os vê - pode aumentar sua auto-estima, resiliência e sucesso.