---
title: TDAH
permalink: tdah
layout: post.hbs
---

# TDAH

## O que é TDAH?

TDAH significa transtorno de déficit de atenção e hiperatividade. É uma condição médica. Uma pessoa com TDAH tem diferenças no desenvolvimento do cérebro e na atividade cerebral que afetam atenção, a habilidade de ficar quieto e autocontrole. TDAH pode afetar uma criança em na escola, em casa e nas amizades.

## Quais são os sinais de TDAH?

Todas as crianças às vezes lutam para prestar atenção, ouvir e seguir instruções, sentar ainda, ou espere sua vez. Mas para crianças com TDAH, as lutas são mais difíceis e acontecem mais frequentemente.

Crianças com TDAH podem apresentar sinais de uma, duas ou todas as três categorias:

* Desatento. Crianças desatentas (facilmente distraídas) têm dificuldade em focar sua atenção, concentrar-se e permanecer na tarefa. Eles não podem ouça bem as instruções, pode perder detalhes importantes e pode não terminar o que eles começar. Eles podem sonhar acordados ou demorar muito. Eles podem parecer distraídos ou esquecidos, e perdem o controle de suas coisas.
* Hiperativo. Crianças hiperativas são irrequietas, inquietas, e facilmente entediado. Eles podem ter dificuldade em ficar sentados ou em silêncio quando necessário. Eles podem se precipitar e cometer erros descuidados. Eles podem escalar, pular ou brigar quando não deveriam. Sem querer, eles podem agir de maneiras que perturbam outros.
* Impulsivo. Crianças impulsivas agem rápido demais antes de pensar. Frequentemente interrompem, podem empurrar ou agarrar e têm dificuldade em esperar. Eles podem fazer coisas sem pedir permissão, pegue coisas que não são deles ou aja de maneiras que são arriscados. Eles podem ter reações emocionais que parecem muito intensas para a situação.

Às vezes, pais e professores notam sinais de TDAH quando a criança é muito pequena. Mas é normal que crianças pequenas sejam distraídas, inquietas, impacientes ou impulsivas - essas coisas nem sempre significam que uma criança tem TDAH.

Atenção, atividade e autocontrole se desenvolvem pouco a pouco, conforme as crianças crescem. As crianças aprendem essas habilidades com a ajuda de pais e professores. Mas algumas crianças não entendem muito melhor em prestar atenção, sossegar, ouvir ou esperar. Quando estes as coisas continuam e começam a causar problemas na escola, em casa e com os amigos, pode ser TDAH.

## Como o TDAH é diagnosticado?

Se você acha que seu filho tem TDAH, marque uma consulta com ele médico. Ele fará um check-up em seu filho, incluindo visão e audição, para ter certeza de que algo mais não está causando os sintomas. O médico pode encaminhá-lo para um psicólogo infantil ou psiquiatra, se necessário.

Para diagnosticar o TDAH, os médicos começam perguntando sobre a saúde, comportamento e atividade. Eles conversam com pais e filhos sobre as coisas que notaram. Seu médico pode pedir-lhe para preencher listas de verificação sobre o comportamento de seu filho, e pode pedir que você dê ao professor do seu filho uma lista de verificação também.

Depois de coletar essas informações, os médicos diagnosticam o TDAH se ficar claro que:

* A distração, hiperatividade ou impulsividade de uma criança vai além do normal para a idade deles.
* Os comportamentos ocorrem desde que a criança era pequena.
* Distração, hiperatividade e impulsividade afetam a criança na escola e em casa.
* Uma verificação de saúde mostra que outro problema de saúde ou de aprendizagem não está causando os problemas.

Muitas crianças com TDAH também têm problemas de aprendizagem, comportamentos de oposição e desafiadores, ou problemas de humor e ansiedade. Os médicos geralmente tratam isso junto com o TDAH.

## Como o TDAH é tratado?

O tratamento para o TDAH geralmente inclui:

* Medicina. este ativa a capacidade do cérebro de prestar atenção, desacelerar e usar mais autocontrole.
* Terapia comportamental. Terapeutas pode ajudar as crianças a desenvolver as habilidades sociais, emocionais e de planejamento que estão atrasadas com TDAH.
* Coaching aos pais. Por meio do coaching, os pais aprendem as melhores maneiras para responder a dificuldades de comportamento que fazem parte do TDAH.
* Suporte escolar. Professores pode ajudar crianças com TDAH a ter um bom desempenho e a aproveitar mais a escola.

O tratamento certo ajuda a melhorar o TDAH. Pais e professores podem ensinar mais jovens as crianças melhorem no gerenciamento de sua atenção, comportamento e emoções. Conforme eles crescem mais velhos, as crianças devem aprender a melhorar sua própria atenção e autocontrole.

Quando o TDAH não é tratado, pode ser difícil para as crianças terem sucesso. Isso pode levar a baixa autoestima, depressão, comportamento de oposição, fracasso escolar, comportamento de risco ou conflito familiar.

## O que os pais podem fazer?

Se seu filho for diagnosticado com TDAH:

* Esteja envolvido. Aprenda tudo o que puder sobre o TDAH. Siga o tratamento o profissional de saúde do seu filho recomenda. Mantenha todos os compromissos recomendados para terapia.
* Dê medicamentos com segurança. Se seu filho está tomando remédios para TDAH, sempre administre no horário e na dose recomendados. Guarde os medicamentos em um lugar seguro.
* Trabalhe com a escola de seu filho. Pergunte aos professores se seu filho deveria tem um IEP. Reúna-se frequentemente com professores para descobrir como seu filho está. Trabalhe em conjunto para ajudar seu filho a se sair bem
* Pais com propósito e calor. Aprenda o que é paternidade as abordagens são melhores para uma criança com TDAH - e podem piorar o TDAH. Fale abertamente e com apoio sobre o TDAH com seu filho. Concentre-se nos pontos fortes do seu filho e qualidades positivas.
* Conecte-se com outras pessoas para obter apoio e conscientização. Junte-se a uma organização de apoio para TDAH para obter atualizações sobre o tratamento e outros informações.

## O que causa o TDAH?

Não está claro o que causa as diferenças cerebrais do TDAH. Há fortes evidências que o TDAH é principalmente herdado. Muitas crianças com TDAH têm pais ou parentes com isso.

O TDAH não é causado por muito tempo de tela, má educação ou alimentação muito açúcar.

O TDAH pode melhorar quando as crianças recebem tratamento, comem alimentos saudáveis, dormem o suficiente e exercício e ter pais solidários que sabem como responder ao TDAH.