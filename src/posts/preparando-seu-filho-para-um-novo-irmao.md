---
title: Preparando seu filho para um novo irmão
permalink: preparando-seu-filho-para-um-novo-irmao
layout: post.hbs
---

# Preparando seu filho para um novo irmão

## Preparando as crianças

A chegada de um novo bebê pode trazer muitas mudanças para a família. Pais gastam muita energia nos preparativos, e depois que o bebê chega, grande parte da família atenção envolve cuidar do recém-nascido.

Todas essas mudanças podem ser difíceis para irmãos mais velhos. É comum para eles sentir ciúme do recém-nascido e reagir à convulsão agindo de forma dramática.

Mas os pais podem preparar os filhos para uma adição à família. Discutindo a gravidez em termos que façam sentido para as crianças, fazendo alguns arranjos e incluindo crianças em os cuidados com o recém-nascido podem tornar as coisas mais fáceis para todos.

## Durante a gravidez

Para contar a uma criança sobre um irmão iminente, considere seu próprio nível de conforto e nível de maturidade do seu filho. Crianças em idade pré-escolar, por exemplo, podem não compreender os conceitos de tempo, então pode não significar muito se você disser que o bebê vai chegar em alguns meses. Pode ser mais útil explicar que o bebê chegará em uma determinada estação, como no inverno ou quando está frio lá fora.

Quantos detalhes você deve fornecer? Deixe as perguntas do seu filho serem o seu guia. Por exemplo, uma criança de 4 anos pode perguntar: "De onde vêm os bebês?" Apesar de como parece, a criança não está pedindo para você explicar o sexo, mas provavelmente quer saber de onde, literalmente, eles vêm. Pode ser o suficiente para explicar: "O bebê vem de o útero, que fica dentro da barriga da mãe. "Uma criança que quer saber mais vai pergunte.

Se o seu filho demonstrar mais interesse pelo bebê, você pode incentivá-lo:

* ver as fotos do bebê do seu filho
* ler livros sobre parto (certifique-se de que sejam adequados para a idade)
* visitar amigos que têm filhos
* fazendo uma mala para o hospital
* pensando em nomes de bebês em potencial
* ir ao médico para ouvir os batimentos cardíacos do bebê

Analise também as aulas de parto de irmãos, que muitos hospitais oferecem orientação para futuros irmãos e irmãs. Essas aulas podem incluir aulas sobre como segurar um bebê, explicações sobre como um bebê nasce e oportunidades para crianças para discutir seus sentimentos sobre ter um novo irmão ou irmã.

## Planejamento para o parto

À medida que a data de vencimento se aproxima, providencie as crianças mais velhas para quando você está no hospital. Discuta esses planos para que as crianças saibam o que esperar quando chegar o dia chega.

Considere deixar seu filho visitá-lo no hospital o mais rápido possível após o bebê nasce, de preferência quando nenhum outro visitante está por perto - isso ajuda a reforçar o nascimento como um evento familiar íntimo.

Tente manter as rotinas o mais regulares possível nos dias e semanas em torno do bebê chegada. Se você planeja fazer alguma mudança de quarto para acomodar o bebê, faça algumas semanas antes da data de vencimento.

Se seu filho está se aproximando de um marco importante, como treinar o penico ou se movimentar do berço à cama, tente fazer essas alterações bem antes da data de vencimento ou coloque-as fora até depois que o bebê estiver em casa por um tempo.

## Trazendo o novo bebê para casa

Quando o bebê estiver em casa, você pode ajudar seus outros filhos a se adaptarem às mudanças. Incluí-los tanto quanto possível nas atividades diárias que envolvem o bebê para que eles não se sentem deixados de fora.

Muitas crianças querem ajudar a cuidar de um novo bebê. Embora essa "ajuda" possa significar que cada tarefa leva mais tempo, pode dar a uma criança mais velha a chance de interagir com o bebê de uma forma positiva. Dependendo da idade, um irmão ou irmã mais velha pode quero entreter o bebê durante a troca de fraldas, ajudar a empurrar o carrinho, conversar com o bebê, ou ajude a vestir, dar banho ou arrotar o bebê.

Se o seu filho não demonstrar interesse pelo bebê, não se assuste e não force isto. Isso pode levar algum tempo.

Algumas ocasiões, como a amamentação, excluem crianças mais velhas. Nesses momentos, tente tenha brinquedos à mão para que possa alimentar o bebê sem ser interrompido ou se preocupar sobre uma criança mais velha se sentindo excluída.

Aproveite as chances de ficar um a um com crianças mais velhas. Passar tempo juntos enquanto o bebê está dormindo e, se possível, reserve um tempo todos os dias para que as crianças mais velhas obter a atenção total de um dos pais. Saber que há um tempo especial só para eles pode ajudar a aliviar qualquer ressentimento ou raiva sobre o novo bebê.

Lembre também a parentes e amigos que seu filho mais velho pode querer conversar sobre algo diferente do novo bebê. Se parentes ou amigos perguntarem como podem ajudar, sugira uma atividade divertida ou algo especial para a criança mais velha.

Continue a mandar o seu filho mais velho para a creche ou para a escola, se puder. Está normal se sentir culpado por mandar seu filho mais velho embora, já que agora você está em casa com o novo bebê (e se você estiver em casa, você pode sentir que todos deveriam estar). Mas Manter uma rotina normal é útil para os irmãos. E este tempo pode lhe dar preciosos um momento individual com o bebê que você não teria. Quando for mais velho criança chega em casa da creche ou da escola, planeje algum tempo de qualidade para a família.

## Lidando com sentimentos

Com todas as mudanças que um novo bebê pode trazer, algumas crianças mais velhas podem ter dificuldades enquanto eles tentam se ajustar.

Incentive as crianças mais velhas a falar sobre o que sentem sobre o novo bebê. Se uma criança não consegue expressar esses sentimentos, não se surpreenda se ele testar limites ou reverter a falar em linguagem infantil.

Se seu filho se comportar mal, não viole as regras, mas entenda quais sentimentos podem ser motivando esse comportamento. Pode ser um sinal de que seu filho precisa de mais um a um tempo com você, mas deixe claro que, embora seus sentimentos sejam importantes, eles devem ser expressos de maneiras apropriadas.