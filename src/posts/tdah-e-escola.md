---
title: TDAH e escola
permalink: tdah-e-escola
layout: post.hbs
---

# TDAH e escola

## Como o TDAH pode afetar as crianças na escola?

O TDAH pode afetar a capacidade do aluno de se concentrar e pagar atenção, ouvir ou colocar esforço nas tarefas escolares. TDAH também pode deixar o aluno inquieto, inquieto, fala demais ou atrapalha a aula. Crianças com TDAH também podem ter aprendizagem deficiências que os levam a ter problemas na escola.

A maioria das crianças com TDAH começa a escola antes do TDAH é diagnosticado. Professores são às vezes, o primeiro a notar possíveis sinais de TDAH. Eles podem conversar sobre isso com o pai da criança. O pai pode, então, ter a criança avaliada por um provedor de saúde para ver se é TDAH.

## Como os professores podem ajudar crianças com TDAH?

Informe todos os professores se seu filho tem TDAH.

Os professores podem ajudá-lo a descobrir se seu filho precisa de um IEP. Um IEP (educação individual programa) é um plano escrito de metas para um aluno e coisas que os professores farão para apoiar o progresso do aluno. O professor do seu filho pode sugerir uma avaliação para ver se seu filho pode se beneficiar de um IEP.

Os professores podem falar com você sobre o seu filho progresso. Peça ao professor para informá-lo sobre o estado de seu filho. Usando uma pasta que vai e volta entre você e o professor de seu filho é uma maneira para compartilhar notas sobre o progresso.

Os professores podem se concentrar nas necessidades do seu filho. Todos os alunos com TDAH é diferente. Alguns precisam de ajuda para prestar atenção e controlar as distrações. Alguns precisam ajudar a se manter organizado. Outros precisam de ajuda para começar o trabalho ou terminar trabalho que eles começam. Alguns alunos com TDAH têm dificuldade em permanecer sentados ou trabalhar silenciosamente. Pergunte ao professor como o TDAH afeta seu filho na sala de aula e o que você pode fazer para ajude seu filho com os trabalhos escolares.

Os professores podem ajudar seu filho a ter sucesso. Dependendo do que um aluno necessidades, um professor pode fazer coisas como:

* Coloque o aluno em um lugar onde haja menos distrações.
* Dê instruções claras e breves.
* Tenha rotinas e regras simples para a sala de aula.
* Seja caloroso, encorajador e positivo.
* Elogie os esforços.
* Ajuda com a organização.
* Oriente as crianças a irem mais devagar e com calma.
* Dê instruções para permanecer na tarefa.
* Faça intervalos para se movimentar na sala de aula.
* Dê mais tempo para concluir o trabalho.
* Ensine os alunos a verificar seus trabalhos e detectar erros imprudentes.

Para alunos mais velhos, os professores também podem:

* Ensine habilidades de estudo, como fazer anotações, ler em voz alta e se preparar para os testes.
* Divida projetos e atribuições de várias etapas em partes menores.

Os professores podem extrair o melhor de seu filho. Quando os professores veem o melhor em seus alunos, os alunos veem o melhor em si mesmos. Os professores podem transmitir que cada aluno pode crescer, aprender e ter sucesso - tenham ou não ADHD.