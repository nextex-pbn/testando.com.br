---
title: Violência na escola e as notícias
permalink: violencia-na-escola-e-as-noticias
layout: post.hbs
---

# Violência na escola e as notícias

Incidentes graves de violência escolar são terríveis e assustadores. Felizmente, eles são raros. Mas é natural que crianças e adolescentes se preocupem se algo pode acontecer com eles ou seus amigos.

Para ajudá-los a lidar com esses medos, é importante falar sobre essas tragédias quando acontecem e para saber o que seus filhos assistem ou ouvem sobre eles. Isso ajuda coloque informações assustadoras em contexto.

## Conversando com seus filhos## Conversando com seus filhos

Não espere que seus filhos se aproximem de você - considere iniciar a conversa. Pergunte o que eles entendem e como se sentem a respeito.

Compartilhe seus próprios sentimentos também. Durante uma tragédia, as crianças costumam olhar para os adultos para seus reações. Ajuda as crianças a saber que não são as únicas a se sentirem ansiosas. Saber o fato de os pais terem sentimentos semelhantes ajuda os filhos a aceitarem os seus. No mesmo tempo, as crianças geralmente precisam dos pais para ajudá-las a se sentirem seguras.

## Lidando com as muitas fontes de notícias

Crianças e adolescentes têm muitas fontes de informações sobre tiroteios em escolas ou outros eventos trágicos. Eles podem ver ou ouvir notícias ou imagens gráficas na TV, rádio, ou online, repetidamente. Esses relatórios podem ensiná-los a ver o mundo como algo confuso, local ameaçador ou hostil.

Os detalhes de uma notícia sobre violência na escola podem fazer algumas crianças sentirem que pode acontecer com eles. Uma criança pode se preocupar: "Eu poderia ser o próximo? Isso poderia acontecer com eu? "

Para acalmar os medos, esteja preparado para dizer a verdade, mas de uma forma que se adapte ao seu filho nível emocional. Não entre em mais detalhes do que seu filho está interessado ou pode lidar.

Embora seja verdade que algumas coisas não podem ser controladas, os pais ainda devem dar crianças o espaço para compartilhar seus medos. Incentive-os a falar abertamente sobre o que assusta eles.

Crianças mais velhas e adolescentes têm menos probabilidade de aceitar uma explicação pelo seu valor nominal. Seus o ceticismo crescente pode esconder o fato de que eles estão preocupados com uma história. Sua vontade ouvir enviará uma mensagem poderosa e os ajudará a lidar com esses medos.

## O que as escolas estão fazendo

Converse com seus filhos sobre o que as escolas fazem para ajudar a proteger seus alunos. Muitos escolas estão tomando precauções extras - algumas se concentram em manter as armas longe verificação aleatória de armários e bolsas, limitando os pontos de entrada e saída na escola e mantendo as entradas sob a supervisão do professor. Outros usam detectores de metal.

Lições sobre como lidar com problemas de maneiras não violentas foram adicionadas a muitos cursos para escolas. O aconselhamento de pares e outros programas ajudam os alunos a aprender a assistir em busca de sinais de que um colega pode estar se tornando mais problemático ou violento.

Outra coisa que ajuda a tornar as escolas mais seguras é uma maior conscientização sobre problemas como bullying e discriminação. Muitas escolas agora temos programas para combater esses problemas, e professores e administradores sabem mais sobre como proteger os alunos da violência.

## Dicas para pais

* Saiba onde seus filhos obtêm notícias e informações, estejam eles assistindo TV ou ficar online.
* Reconheça que as notícias não precisam ser motivadas por imagens perturbadoras. Televisão pública programas, jornais ou revistas projetadas especificamente para crianças podem custar menos maneiras sensacionais - e menos perturbadoras - para eles obterem informações.
* Discuta eventos atuais com seus filhos com frequência. É importante ajudá-los a pensar por meio de histórias que ouvem. Faça perguntas: O que você acha desses eventos? Como você acha que essas coisas acontecem? Essas perguntas também incentivam a conversa sobre tópicos não noticiosos.
* Coloque as notícias no contexto adequado. Mostrar que eventos violentos são isolados ou explicar como um evento se relaciona com outro ajuda as crianças a entender melhor o que ouvir.
* Assistir às notícias com seus filhos para filtrar histórias juntos.
* Saiba quando a orientação é necessária e evite programas que não sejam apropriados para o seu idade da criança ou nível de desenvolvimento.
* Se você não se sentir confortável com o conteúdo da notícia ou se for impróprio para idade do seu filho, desligue-o.