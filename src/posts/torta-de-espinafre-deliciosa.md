---
title: Torta De Espinafre Deliciosa
permalink: torta-de-espinafre-deliciosa
layout: post.hbs
---

# Torta De Espinafre Deliciosa

Tempo de preparação: 5 minutos

Tempo de cozimento: 40–45 minutos

## O que você precisa:

* 3 ovos grandes
* 2 claras de ovo
* 1/2 xícara de leite desnatado
* 3/4 xícara de mussarela desnatada
* 1 xícara de espinafre picado fresco ou congelado (se congelado, descongele e escorra bem)
* 1 colher de chá de sal
* 1/2 colher de chá de pimenta preta
* 1 torta congelada em prato fundo profundo (9 polegadas)

## O que fazer:

1. Pré-aqueça o forno a 176 ° C (350 ° F).
2. Em uma tigela grande, misture os ovos, claras de ovo, leite desnatado, mussarela, espinafre, sal, e pimenta. Mexa até misturar bem.
3. Despeje a mistura na casca da torta e asse por 40–45 minutos ou até que a torta esteja firme e a crosta é marrom-dourada.
4. Fatie e sirva quente, em temperatura ambiente ou até mesmo frio.

## Análise nutricional (por porção):

* 240 calorias
* 10g de proteína
* 15g de gordura
* 4g sat. gordura
* 16g de carboidrato
* 0g de fibra
* 115mg de colesterol
* 660mg de sódio
* 2g de açúcares

Serve: 6

Tamanho da porção: 1 fatia (110 g)

Observação: a análise nutricional pode variar dependendo das marcas de ingredientes usadas.

Sugestão: substitua alcachofras enlatadas e escorridas pelo espinafre para fazer uma alcachofra quiche.