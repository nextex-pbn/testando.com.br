---
title: Ajudando adolescentes que cortam
permalink: ajudando-adolescentes-que-cortam
layout: post.hbs
---

# Ajudando adolescentes que cortam

Cortar - use um objeto afiado como uma lâmina de barbear, faca ou tesoura para fazer marcas, cortes ou arranhões no próprio corpo - é uma forma de automutilação.

Pode ser difícil entender por que alguém se machucaria de propósito. Saber que seu próprio adolescente está fazendo isso pode deixá-lo chocado e chateado - e não tenho certeza do que fazer ou como ajudar.

## Sobre o corte

Para a maioria, cortar é uma tentativa de interromper emoções fortes e pressões que parecem impossíveis de tolerar. Pode estar relacionado para questões emocionais mais amplas que precisam de atenção. Na maioria das vezes, o corte não é uma tentativa de suicídio.

A mutilação afeta muitos adolescentes e pré-adolescentes - mesmo além daqueles que se machucam. Muitos adolescentes se preocupam com um amigo que corta ou enfrenta a pressão de colegas para tentar cortar como algo online para fazer.

Em muitos casos, o corte e as emoções que o acompanham são algo adolescente lutar sozinho. Mas devido à crescente conscientização, mais adolescentes podem obter ajuda eles precisam.

Os pais podem ajudar os adolescentes que cortam - e quanto mais cedo, melhor. Lata de corte ser viciante e, infelizmente, muitas pessoas subestimam os riscos de ficar seriamente doente ou ferido que vem junto com isso.

## O que os pais podem fazer

Se o seu filho adolescente está se cortando, existem maneiras de ajudar. Ao lidar com seus próprios sentimentos, aprender sobre corte, encontrar ajuda profissional e apenas estar lá para amar e acreditar em seu filho adolescente, você fornecerá o apoio calmo e constante de que ele precisa.

Aceite suas próprias emoções. Se você conhece ou suspeita que seu filho adolescente é cortante, é natural sentir toda uma gama de emoções. Você pode se sentir chocado, zangado, triste, desapontado, confuso ou com medo. Você pode se sentir magoado que seu filho adolescente não veio a você em busca de ajuda ou se sentiu culpado por não saber disso. Todos esses emoções são completamente compreensíveis. Mas não é sua culpa, e não é sua culpa do adolescente.

Reserve um tempo para identificar seus próprios sentimentos e encontrar uma maneira de expressá-los. Isso pode significa chorar bem, conversar com um amigo ou dar uma caminhada para desabafar ou refletir calmamente. Se você se sentir sobrecarregado, conversar com um terapeuta pode ajudá-lo classificar as coisas e obter alguma perspectiva para que você possa fornecer o suporte suas necessidades de adolescente.

Aprenda tudo o que puder sobre o corte. Descubra tudo o que puder sobre corte, por que os adolescentes fazem isso e o que pode ajudá-los a parar. Alguns adolescentes cortam por causa da pressão dos colegas - e uma vez que eles começam, eles não podem parar facilmente. Outros adolescentes se sentem pressionados a seja perfeito e lute para aceitar falhas ou erros. E ainda outros lutam com humores poderosos como raiva, tristeza, inutilidade e desespero que parecem difíceis de controlar ou muito pesado para suportar. O corte às vezes é resultado de trauma e experiências dolorosas que ninguém conhece.

Pode doer pensar que seu filho pode ter algum desses sentimentos. NOS por mais difícil que seja, tente ter em mente que explorar quais pressões levam a adolescente a se ferir é um passo necessário para a cura.

## Comunicação É a chave

Converse com seu filho. Pode ser difícil falar sobre uma tópico. Você pode não saber o que vai dizer. Isso está ok. O que você diz não será quase tão importante quanto como você o diz. Para abrir a conversa, você pode simplesmente diga que você sabe sobre o corte e, em seguida, transmita sua preocupação, amor e sua vontade de ajudar seu filho a parar.

Provavelmente será difícil para seu filho adolescente falar sobre isso também. Ele ou ela pode sentir envergonhado ou envergonhado, ou preocupado sobre como você reagirá ou quais serão as consequências pode ser. Você pode ajudar a aliviar essas preocupações fazendo perguntas e ouvindo o que seu filho adolescente tem a dizer sem reagir com punições, repreensões ou sermões.

Informe ao seu filho que o corte costuma estar relacionado a experiências dolorosas ou intensas pressões e pergunte que questões difíceis seu filho pode estar enfrentando. Seu filho adolescente pode não esteja pronto para falar sobre isso ou mesmo saber por que ele ou ela corta. Mesmo que seja o caso, explique que você deseja entender e encontrar maneiras de ajudar.

Não se surpreenda se seu filho resistir aos seus esforços para falar sobre cortes. Ele ou ela pode negar o corte, ficar com raiva ou chateado, chorar, gritar ou sair furioso. Um adolescente pode se calar ou dizer que você simplesmente não entende. Se algo assim acontecer, tente ficar calmo e paciente. Não desista - encontre outro momento para se comunicar e tente novamente.

Procure ajuda profissional. É importante procurar ajuda de um profissional de saúde mental qualificado que pode ajudá-lo a entender por que seu adolescente corta e também a curar velhas feridas e desenvolver novas habilidades de enfrentamento.

A terapia pode permitir que os adolescentes contem suas histórias, coloquem suas experiências difíceis em palavras e aprender habilidades para lidar com o estresse que faz parte da vida. Terapia também pode ajudar a identificar qualquer condição de saúde mental subjacente que precisa de avaliação e tratamento. Para muitos adolescentes, cortar é uma pista para a depressão ou problemas bipolares (humor), luto não resolvido, comportamentos compulsivos ou lutas com perfeccionismo.

É importante encontrar um terapeuta com o qual seu filho possa se sentir aberto e confortável. Se você precisar de ajuda para encontrar alguém, seu médico ou conselheiro escolar pode ser capaz de fornecer orientação.

## Mantendo-se positivo

Ofereça incentivo e apoio. Enquanto seu filho adolescente está se profissionalizando ajudar, permaneça envolvido no processo tanto quanto possível. Peça ao terapeuta para orientar em como falar e apoiar seu filho adolescente. E pergunte ao seu filho adolescente como você pode melhor ajuda.

Por exemplo, pode ajudar:

* Deixe seu filho saber que você estará disponível para conversar quando os sentimentos forem dolorosos ou difíceis parece muito difícil de suportar.
* Ajude seu filho a criar um plano sobre o que fazer em vez de cortar quando houver pressão fique forte.
* Incentive seu filho a falar sobre experiências cotidianas e colocar sentimentos, necessidades, decepções, sucessos e opiniões em palavras.
* Esteja lá para ouvir, confortar e ajudar seu filho adolescente a pensar em soluções para os problemas e oferecer suporte quando surgirem problemas.
* Passe algum tempo juntos fazendo algo divertido, relaxante ou simplesmente curtindo. Você pode dê um passeio, dê um passeio, compartilhe um lanche ou faça algumas coisas.
* Concentre-se nos aspectos positivos. Embora ajude falar sobre problemas, evite insistir neles. Certifique-se de que o que há de bom na vida também tenha tempo de antena.

Dê um bom exemplo. Esteja ciente de que você pode influenciar como seu filho Responde ao estresse e pressão dando um bom exemplo. Observe como você gerencia seu próprias emoções e lidar com as frustrações, o estresse e a pressão do dia a dia. Aviso prévio se você tende a rebaixar os outros, se é autocrítico ou se irrita rapidamente. Considere fazer alterações em quaisquer padrões que você não gostaria que seu filho adolescente imitasse.

Seja paciente e esperançoso. Descobrir que seu filho adolescente está cortando pode ser o início de um longo processo. Pode levar algum tempo para parar de cortar - e às vezes, um adolescente não quer parar ou não está pronto para fazer as mudanças que isso envolve.

Parar de cortar exige motivação e determinação. Também requer autoconsciência e praticar novas habilidades para administrar pressões e angústia emocional. Essas coisas pode levar tempo e geralmente requer ajuda profissional.

Como pai, você precisa ser paciente. Com a orientação adequada, amor e suporte, saiba que seu filho pode parar de se cortar e aprender maneiras saudáveis ​​de lidar com isso.