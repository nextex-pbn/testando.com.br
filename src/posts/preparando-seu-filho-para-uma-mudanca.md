---
title: Preparando seu filho para uma mudança
permalink: preparando-seu-filho-para-uma-mudanca
layout: post.hbs
---

# Preparando seu filho para uma mudança

Cedo ou tarde, muitas famílias enfrentam a perspectiva de se mudar. Disruptivo como em movimento pode ser para os pais, a experiência pode ser ainda mais traumática para as crianças, que podem não ser parte da decisão de mudança e pode não entendê-la.

As crianças podem precisar de algum tempo e atenção especial durante a transição. Tente esse dicas para tornar o processo menos estressante para todos.

## Tomando a decisão de mudar

Muitas crianças adoram familiaridade e rotina. Então, ao considerar uma mudança, pese o benefícios dessa mudança em relação ao conforto que estabeleceu o ambiente, a escola, e a vida social dá aos seus filhos.

Se sua família passou recentemente por uma grande mudança em sua vida, como o divórcio ou morte, você pode adiar uma mudança, se possível, para dar às crianças tempo para se ajustarem.

A decisão de se mudar pode estar fora de suas mãos, talvez devido a uma transferência de emprego ou problemas financeiros. Mesmo que você não esteja feliz com a mudança, tente manter uma atitude positiva atitude sobre isso. Durante os tempos de transição, o humor e as atitudes dos pais podem afetam muito as crianças, que podem estar procurando por garantias.

## Discutindo a mudança com crianças

Não importa quais sejam as circunstâncias, a maneira mais importante de preparar as crianças para a mudança é falar sobre isso.

Tente dar a eles o máximo de informações sobre a mudança o mais rápido possível. Responda perguntas de forma completa e verdadeira, e seja receptivo a questões positivas e negativas reações. Mesmo que a mudança signifique uma melhoria na vida familiar, as crianças nem sempre entenda isso e possa se concentrar nos aspectos assustadores da mudança.

Envolver as crianças no planejamento tanto quanto possível faz com que se sintam participantes no processo de procura de uma casa ou na procura de uma nova escola. Isso pode fazer a mudança parece menos que está sendo forçado a eles.

Se você estiver se mudando pela cidade, tente levar seus filhos para visitar a nova casa (ou ver sendo construído) e explorar o novo bairro.

Para mudanças distantes, forneça o máximo de informações possível sobre a nova casa, cidade, e estado (ou país). Acesse a Internet para aprender sobre a comunidade. Saiba onde as crianças podem participar de atividades favoritas. Veja se um parente, amigo ou mesmo um o corretor de imóveis pode tirar fotos da nova casa e da nova escola para seu filho.

## Movendo-se com bebês e crianças em idade pré-escolar

Crianças com menos de 6 anos podem ser mais fáceis de se movimentar, pois têm capacidade limitada para entender as mudanças envolvidas. Ainda assim, sua orientação é crucial.

Estas são maneiras de facilitar a transição para crianças pequenas:

* Mantenha as explicações claras e simples.
* Use uma história para explicar a mudança ou use caminhões de brinquedo e móveis para encená-la.
* Ao embalar os brinquedos do seu filho em caixas, certifique-se de explicar que você não jogá-los fora.
* Se sua nova casa estiver próxima e vazia, vá até lá para visitá-la antes de se mudar e alguns brinquedos de cada vez.
* Evite se livrar da mobília do quarto de seu filho, o que pode fornecer uma sensação de conforto na nova casa. Pode até ser uma boa ideia organizar os móveis de forma semelhante no novo quarto.
* Evite fazer outras grandes mudanças durante a mudança, como treinamento ou avanço do banheiro uma criança pequena para uma cama de um berço.
* Providencie para que seu filho pequeno ou pré-escolar fique com uma babá no dia da mudança.

## Movendo-se com crianças em idade escolar

Crianças na escola primária podem estar relativamente abertas a mudanças, mas ainda precisam ser sérias consideração e ajuda durante a transição.

Existem duas escolas de pensamento sobre "a hora certa para se mudar". Alguns especialistas dizem aquele verão é a melhor época porque evita atrapalhar o ano letivo. Outras diga que o meio do ano é melhor porque uma criança pode conhecer outras crianças imediatamente.

Para evitar falhas que aumentam o estresse, reúna todas as informações da nova escola precisará processar a transferência. Isso pode incluir o boletim mais recente ou transcrição, certidão de nascimento e registros médicos.

## Seguindo com Adolescentes

É comum que os adolescentes se rebelem ativamente contra um movimento. Seu filho adolescente provavelmente investiu energia considerável em um determinado grupo social e pode estar envolvido em um relacionamento romântico. Uma mudança pode significar que seu filho vai perder um evento tão esperado, como um baile.

É especialmente importante que os adolescentes saibam que você deseja ouvir suas preocupações e que você os respeita. Embora garantias gerais possam soar desdenhosas, são legítimas para sugerir que a mudança pode servir como ensaio para mudanças futuras, como faculdade ou um novo trabalho. No entanto, certifique-se também de informá-los de que você ouviu suas preocupações.

Após a mudança, considere planejar uma visita de volta ao bairro antigo, se for factível. Além disso, veja se o adolescente pode voltar para eventos como baile ou volta ao lar.

Se você estiver mudando no meio de um ano letivo, pode considerar a possibilidade de um adolescente mais velho fica no antigo local com um amigo ou parente, se houver opção.

## Após o dia da mudança

Após a mudança, tente deixar o quarto do seu filho em ordem antes de voltar sua atenção para o resto da casa. Além disso, tente manter sua programação regular de refeições e hora de dormir para dar às crianças uma sensação de familiaridade.

Quando seu filho começar a escola, você pode querer ir junto para conhecer o maior número de professores quanto possível ou para apresentar seu filho ao diretor.

Defina expectativas realistas sobre a transição. Geralmente, os professores esperam novos as crianças se sintam um pouco confortáveis ​​em suas aulas em cerca de 6 semanas. Algumas crianças precisam menos tempo; outros podem precisar de mais. Incentive seu filho ou adolescente a acompanhar os velhos amigos por meio de chamadas telefônicas, chats de vídeo, mídias sociais aprovadas pelos pais e outros maneiras de se manter conectado.

Após a mudança, se você ainda estiver preocupado com a transição do seu filho, uma família terapeuta pode fornecer alguma orientação útil.

Uma mudança pode apresentar muitos desafios, mas coisas boas também vêm desse tipo de mudança. Sua família pode ficar mais próxima e vocês podem aprender mais uns sobre os outros indo através dele juntos.