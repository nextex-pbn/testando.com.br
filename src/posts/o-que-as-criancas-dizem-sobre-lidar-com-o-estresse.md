---
title: O que as crianças dizem sobre: ​​lidar com o estresse
permalink: o-que-as-criancas-dizem-sobre-lidar-com-o-estresse
layout: post.hbs
---

# O que as crianças dizem sobre: ​​lidar com o estresse

Comparado com o que os adultos enfrentam, pode parecer que as crianças não têm muito que estresse sobre. Mas as crianças têm suas próprias preocupações - e às vezes sentem estresse, assim como os adultos fazem. E o estresse das crianças pode ser tão opressor, especialmente se eles não têm estratégias eficazes de enfrentamento.

Um KidsHealth® KidsPoll explorou o que as crianças mais estressam, como lidam com com esses sentimentos e o que eles querem que seus pais façam a respeito.

A pesquisa mostrou que as crianças estão lidando com o estresse de formas saudáveis ​​e não saudáveis maneiras, e embora possam não dizer isso, eles querem que seus pais estendam a mão e ajudem eles lidam com seus sentimentos.

A pesquisa ressaltou a importância dos pais ensinarem os filhos a reconhecer e expressar suas emoções, e usar maneiras saudáveis ​​de lidar com o estresse que experimentam. Ao orientá-los para habilidades de enfrentamento saudáveis, os pais podem ajudar a preparar as crianças para enfrentar qualquer estresse que eles encontrem ao longo de suas vidas.

## Resultados da votação

Pedimos às crianças que nos contassem as coisas que lhes causam mais estresse. Crianças disseram isso eles se estressaram mais com: notas, escola e dever de casa (36%); família (32%); e amigos, colegas, fofoca e provocações (21%).

Estas são as estratégias de enfrentamento que as crianças disseram que mais usam (poderiam dar mais de uma resposta):

* 52% jogam ou fazem algo ativo
* 44% ouvem música
* 42% assistem TV ou jogam videogame
* 30% falam com um amigo
* 29% tentam não pensar nisso
* 28% tentam resolver as coisas
* 26% comem algo
* 23% perdem a paciência
* 22% falam com um dos pais
* 11% choram

Cerca de 25% das crianças que entrevistamos disseram que, quando estão chateadas, elas o tiram sobre si mesmos, seja batendo a cabeça em algo, batendo ou mordendo a si próprios ou a fazer outra coisa para se magoar. Essas crianças também eram mais propensos a ter outras estratégias de enfrentamento prejudiciais, como comer, perder a paciência, e manter os problemas para si mesmos.

A ideia de que crianças fariam coisas para tentar se prejudicar pode ser chocante para pais. Mas para algumas crianças, sentimentos de estresse, frustração, impotência, mágoa ou a raiva pode ser opressora. E sem uma forma de expressar ou liberar os sentimentos, um criança pode se sentir como um vulcão pronto para entrar em erupção - ou pelo menos desabafar.

Às vezes, as crianças se culpam quando as coisas dão errado. Eles podem se sentir envergonhados, constrangidos ou com raiva de si mesmos pelo papel que desempenharam na situação. Machucando eles próprios podem ser uma forma de expressar o estresse e se culpar ao mesmo tempo.

A enquete também revelou notícias importantes para os pais. Embora falar com os pais classificado Oitavo na lista de métodos de enfrentamento mais populares, 75% das crianças entrevistadas disseram que querem e precisam da ajuda dos pais em tempos difíceis. Quando eles estão estressados, eles como seus pais para conversar com eles, ajudá-los a resolver o problema, tentar animá-los ou apenas passem um tempo juntos.

## O que os pais podem fazer

Você pode não conseguir evitar que seus filhos se sintam frustrados, tristes ou com raiva, mas você pode fornecer as ferramentas de que precisam para lidar com essas emoções.

Observe em voz alta. Diga às crianças quando notar algo que elas podem estar sentindo ("Parece que você ainda pode ficar bravo com o que aconteceu no playground"). Isso não deve soar como uma acusação (como em: "OK, o que aconteceu agora? Você ainda louco com isso? ") ou fazer uma criança se sentir prejudicada. É apenas uma observação casual que você está interessado em saber mais sobre a preocupação de seu filho.

Ouça seus filhos. Peça que digam o que está errado. Ouço com atenção e calma - com interesse, paciência, franqueza e carinho. Evitar qualquer desejo de julgar, culpar, dar um sermão ou dizer a seus filhos o que deveriam ter feito em vez disso. A ideia é permitir que as preocupações (e sentimentos) da criança sejam ouvidos. Incentive seu filho para contar toda a história fazendo perguntas. Não tenha pressa, e deixe uma criança levar o tempo dele também.

Comente brevemente sobre os sentimentos que você acha que seu filho estava experimentando como você escuta. Por exemplo, você pode dizer algo como: "Deve ter sido perturbador "ou" Não é de admirar que você tenha ficado bravo quando eles não o deixaram entrar no jogo. " isso mostra que você entende o que seu filho sentiu, por que ele ou ela se sentiu assim, e que você se importa. Sentir-se compreendido e ouvido para ajudar as crianças a se sentirem conectadas a você, e isso é especialmente importante em tempos de estresse.

Coloque uma etiqueta nele. Muitas crianças ainda não têm palavras para expressar seus sentimentos. Se seu filho parece zangado ou frustrado, use essas palavras de sentimento para ajudá-lo aprenda a identificar as emoções pelo nome. Isso ajudará a colocar os sentimentos em palavras, eles podem ser expressos e comunicados mais facilmente, o que ajuda as crianças a se desenvolverem consciência - a habilidade de reconhecer seus próprios estados emocionais. Crianças que podem reconhecer e identificar emoções têm menos probabilidade de atingir o ponto de ebulição comportamental onde emoções fortes são demonstradas por meio de comportamentos em vez de comunicadas com palavras.

Ajude as crianças a pensar em coisas para fazer. Sugerir atividades que as crianças podem fazer para se sentir melhor agora e resolver o problema em questão. Incentive-os a pensar em um algumas idéias. Você pode iniciar o brainstorm se necessário, mas não faça tudo o trabalho. A participação ativa de uma criança criará confiança. Apoie boas ideias e adicione-os conforme necessário. Pergunte: "Como você acha que isso vai funcionar?" Às vezes falando e ouvir e sentir-se compreendido é tudo o que é necessário para ajudar nas frustrações das crianças dissipar. Outras vezes, mudam de assunto e passam para algo mais positivo e relaxante. Não dê ao problema mais atenção do que ele merece.

Basta estar lá. Às vezes, as crianças não querem falar sobre o que é incomodando-os. Tente respeitar isso, dar espaço a eles e ainda deixar claro que você estará lá quando eles quiserem conversar. Mesmo quando as crianças não têm vontade de falar, geralmente não querem que os pais os deixem em paz. Você pode ajudá-los a se sentirem melhor apenas por estar lá - para fazer companhia ao seu filho e passar algum tempo juntos. assim se você notar que seu filho parece estar deprimido, estressado ou tendo um mau dia - mas não tem vontade de falar - inicie algo que possam fazer juntos. Faça uma caminhada, assista a um filme, faça algumas bolas de basquete ou asse alguns biscoitos. Não é legal saber que sua presença realmente conta?

Seja paciente. Dói ver seus filhos infelizes ou preocupados. Mas tente resistir ao impulso de resolver todos os problemas. Em vez disso, concentre-se em ajudá-los a crescer para bons solucionadores de problemas - crianças que sabem como lidar com os altos e baixos da vida, transforme seus sentimentos em palavras, acalme-se quando necessário e volte para tentar novamente. Lembrar que você não pode consertar tudo, e que você não estará lá para resolver cada problema como seu filho passa pela vida. Mas, ao aprender estratégias de enfrentamento saudáveis, as crianças podem gerenciar tensões no futuro.

## Sobre a votação

O KidsPoll nacional pesquisou 875 meninos e meninas de 9 a 13 anos sobre como eles lidaram com o estresse. O KidsPoll é uma colaboração da Nemours Foundation / KidsHealth, o Departamento de Educação em Saúde e Recreação da Southern Illinois University - Carbondale, a Associação Nacional de Centros de Educação em Saúde (NAHEC), e centros de educação em saúde participantes em todos os Estados Unidos. Esses centros incluem:

* Robert Crown Center for Health Education - Hinsdale, Illinois
* HealthWorks! Kids Museum - South Bend, Indiana
* Museu Mundial da Saúde das Crianças - Barrington, Illinois
* Ruth Lilly Health Education Center - Indianápolis, Indiana
* Susan P. Byrnes Health Education Center - York, Pensilvânia
* Poe Center for Health Education - Raleigh, Carolina do Norte