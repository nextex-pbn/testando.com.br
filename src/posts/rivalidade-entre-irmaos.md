---
title: Rivalidade entre irmãos
permalink: rivalidade-entre-irmaos
layout: post.hbs
---

# Rivalidade entre irmãos

## Sobre rivalidade entre irmãos

Embora muitas crianças tenham a sorte de se tornarem as melhores amigas de seus irmãos, é comum irmãos e irmãs brigarem. (Também é comum eles balançarem para frente e para trás entre adorar e detestar um ao outro!)

Muitas vezes, a rivalidade entre irmãos começa antes mesmo do nascimento do segundo filho e continua conforme as crianças crescem e competem por tudo, desde brinquedos até atenção. Conforme as crianças alcançam diferentes estágios de desenvolvimento, suas necessidades em evolução podem afetar significativamente como eles se relacionam entre si.

Pode ser frustrante e perturbador ver - e ouvir - seus filhos lutar um com o outro. Uma casa cheia de conflitos é estressante para todos. No entanto, muitas vezes é difícil saber como parar a luta, e ou mesmo se você deve envolva-se em tudo. Mas você pode tomar medidas para promover a paz em sua casa e ajude seus filhos a se darem bem.

## Por que as crianças lutam

Muitas coisas diferentes podem fazer com que irmãos briguem. A maioria dos irmãos e irmãs experimentam algum grau de ciúme ou competição, e isso pode se transformar em disputas e brigas. Mas outros fatores também podem influenciar a frequência com que as crianças brigam e a intensidade das brigas obtém. Isso inclui:

* Necessidades em evolução. É natural que as necessidades e ansiedades em constante mudança, e identidades para afetar como eles se relacionam uns com os outros. Por exemplo, crianças são naturalmente protetores de seus brinquedos e pertences, e estão aprendendo a fazer valer seus vontade, o que eles farão a cada passo. Então, se um irmãozinho ou irmãzinha pegar o brinquedo da criança, a criança mais velha pode reagir agressivamente. Crianças em idade escolar costumam ter um forte conceito de justiça e igualdade, então pode não entender por que irmãos de outras idades são tratadas de maneira diferente ou acham que uma criança recebe tratamento preferencial. Os adolescentes, por outro lado, estão desenvolvendo um senso de individualidade e independência, e pode se ressentir de ajudar com as responsabilidades domésticas, cuidando dos irmãos mais novos, ou mesmo tendo que passar um tempo juntos. Todas essas diferenças podem influenciar o maneira como as crianças brigam umas com as outras.
* temperamentos individuais. Os temperamentos individuais de seus filhos - incluindo humor, disposição e adaptabilidade - e suas personalidades únicas desempenham um grande papel em como eles se dão bem. Por exemplo, se uma criança é descontraída e outro é facilmente sacudido, eles podem frequentemente entrar nele. Semelhante, uma criança que é especialmente pegajoso e atraído pelos pais em busca de conforto e amor pode ser ressentido por irmãos que veem isso e desejam a mesma atenção.
* Necessidades especiais / crianças doentes. Às vezes, as necessidades especiais de uma criança devido Para doenças ou problemas de aprendizagem / emocionais podem exigir mais tempo dos pais. Outras crianças pode perceber essa disparidade e agir para chamar a atenção ou por medo do que é acontecendo com a outra criança.
* Modelos de papéis. A maneira como os pais resolvem problemas e desacordos é um exemplo forte para as crianças. Então, se você e seu cônjuge resolverem os conflitos em de uma forma que seja respeitosa, produtiva e não agressiva, você aumenta as chances que seus filhos vão adotar essas táticas quando tiverem problemas uns com os outros. Se seus filhos o virem gritar, bater portas e argumentar em voz alta quando você tem problemas, eles provavelmente adquirem esses hábitos ruins.

## O que fazer quando a luta começar

Embora seja comum que irmãos e irmãs briguem, certamente não é agradável para qualquer pessoa da casa. E uma família só pode tolerar uma certa quantidade de conflito. Então, o que você deve fazer quando a luta começar?

Sempre que possível, não se envolva. Entrar somente se houver perigo de dano físico. Se você sempre intervém, corre o risco de criar outros problemas. As crianças podem começar a esperar sua ajuda e esperar que você venha para o resgate, em vez de aprender a resolver os problemas por conta própria. Há também o risco de você - inadvertidamente - dar a impressão de que outra criança está sempre sendo "protegida", o que pode gerar ainda mais ressentimento. Pelo mesmo crianças resgatadas podem sentir que podem se safar mais porque estão sempre sendo "salvo" por um dos pais.

Se você está preocupado com a linguagem usada ou xingamentos, é apropriado "treinar" crianças através do que estão sentindo, usando palavras apropriadas. Isso é diferente de intervir ou intervir e separar as crianças.

Mesmo assim, incentive-os a resolver a crise sozinhos. Se você intervir, tente resolver problemas com seus filhos, não para eles.

Ao se envolver, aqui estão alguns passos a considerar:

* Separe as crianças até que fiquem calmas. Às vezes é melhor apenas dar-lhes espaço por um tempo e não imediatamente refazer o conflito. Caso contrário, a luta pode escalar novamente. Se você quiser fazer disso uma experiência de aprendizado, espere até que as emoções morreram.
* Não coloque muito foco em descobrir qual criança é a culpada. Leva dois lutar - qualquer pessoa envolvida é parcialmente responsável.
* Em seguida, tente criar uma situação "ganha-ganha" para que cada criança ganhe algo. Quando os dois querem o mesmo brinquedo, talvez haja um jogo que eles possam jogar juntos em vez disso.

Lembre-se, conforme as crianças lidam com as disputas, elas também aprendem habilidades importantes que servi-los por toda a vida - como como valorizar a perspectiva de outra pessoa, como transigir e negociar, e como controlar impulsos agressivos.

## Ajudando Crianças se dão bem

Coisas simples que você pode fazer todos os dias para evitar lutas incluem:

* Estabeleça regras básicas para um comportamento aceitável. Diga às crianças para manterem as mãos e que não há xingamentos, xingamentos, gritos, portas batendo. Solicite sua opinião sobre as regras - bem como as consequências quando elas violam eles. Isso ensina as crianças que são responsáveis ​​por suas próprias ações, independentemente da situação ou como se sentiram provocados, e desestimula qualquer tentativa de negociação sobre quem estava "certo" ou "errado".
* Não deixe as crianças fazerem você pensar que tudo sempre tem que ser "justo" e "igual" - às vezes uma criança precisa mais do que a outra.
* Seja proativo ao dar a seus filhos atenção individual direcionada aos interesses deles e necessidades. Por exemplo, se gosta de passear ao ar livre, dê um passeio ou vá ao parque. Se outra criança gosta de sentar e ler, reserve tempo para isso também.
* Certifique-se de que as crianças tenham seu próprio espaço e tempo para fazer suas próprias coisas - para brincar com brinquedos sozinhos, brincar com os amigos sem um irmão acompanhando, ou para desfrutar de atividades sem ter que compartilhar 50-50.
* Mostre e diga a seus filhos que, para você, amor não é algo que vem com limites.
* Diga a eles que são seguros, importantes e amados e que suas necessidades serão ser conhecido.
* Divirta-se em família. Esteja você assistindo um filme, jogando uma bola, ou jogando um jogo de tabuleiro, você está estabelecendo uma maneira pacífica para seus filhos gastarem tempo juntos e se relacionar uns com os outros. Isso pode ajudar a aliviar as tensões entre eles e também mantém você envolvido. Uma vez que a atenção dos pais é algo pelo qual muitas crianças lutam, atividades familiares divertidas podem ajudar a reduzir conflitos.
* Se seus filhos brigam frequentemente sobre as mesmas coisas (como videogames ou dibs no controle remoto da TV), poste uma programação mostrando qual criança "possui" aquele item em que horas durante a semana. (Mas se eles continuarem lutando por isso, leve o "prêmio" completamente afastado.)
* Se as brigas entre seus filhos em idade escolar forem frequentes, faça reuniões familiares semanais em que você repete as regras sobre luta e analisa os sucessos anteriores na redução conflitos. Considere estabelecer um programa em que as crianças ganham pontos para se divertir atividade voltada para a família quando eles trabalham juntos para parar de lutar.
* Reconheça quando as crianças só precisam de um tempo longe umas das outras e da dinâmica familiar. Tente arranjar datas de jogo ou atividades separadas para cada criança ocasionalmente. E quando uma criança está em um encontro, você pode ficar um a um com outra.

Lembre-se de que às vezes as crianças lutam para chamar a atenção dos pais. Nesse caso, considere dar um tempo para você. Quando você sai, o incentivo para lutar se foi. Além disso, quando seu próprio fusível estiver ficando curto, considere entregar as rédeas para o outro pai, cuja paciência pode ser maior naquele momento.

## Obtendo Ajuda profissional

Em uma pequena porcentagem de famílias, o conflito entre irmãos e irmãs é tão grave que perturba o funcionamento diário, ou afeta particularmente as crianças emocionalmente ou psicologicamente. Nesses casos, é aconselhável obter ajuda de um profissional de saúde mental. Procure ajuda para conflito entre irmãos se:

* é tão grave que leva a problemas conjugais
* cria um perigo real de dano físico a qualquer membro da família
* é prejudicial à auto-estima ou ao bem-estar psicológico de qualquer membro da família
* pode estar relacionado a outras preocupações significativas, como depressão

Se você tiver dúvidas sobre as brigas de seus filhos, converse com seu médico, que pode ajudá-lo a determinar se sua família pode se beneficiar de ajuda profissional e encaminhar você a recursos locais de saúde comportamental.