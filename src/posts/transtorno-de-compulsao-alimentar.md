---
title: Transtorno de compulsão alimentar
permalink: transtorno-de-compulsao-alimentar
layout: post.hbs
---

# Transtorno de compulsão alimentar

Você às vezes acha que seus filhos podem comê-lo fora de casa e em casa? Pode sinto assim às vezes, especialmente durante a adolescência. Eles pegam um punhado de biscoitos aqui, um saco de batatas fritas ali, e termine as sobras da noite anterior em um flash. Eles estão crescendo como ervas daninhas, é claro, então você acha que comer tudo isso está bem. E mais do tempo, é.

Mas às vezes, lanches pesados ​​não são o que parecem. Uma criança que come de forma incomum grandes quantidades de comida - e se sente culpado ou reservado sobre isso - poderia estar lutando com um distúrbio alimentar comum chamado de transtorno da compulsão alimentar periódica.

## Sobre o transtorno da compulsão alimentar periódica

Muitas pessoas encontram conforto na comida. Afinal, muitas vezes está no cerne de nossa celebrações mais felizes. Aniversários podem significar bolo com amigos; Ação de Graças muitas vezes significa peru e recheio com a família. Muitas pessoas às vezes comem muito mais do que eles normalmente fazem (ou até querem) em ocasiões especiais.

Mas as pessoas com transtorno da compulsão alimentar periódica têm uma relação diferente com a comida - eles sentem que perderam todo o controle sobre o quanto estão comendo, como se não pudessem Pare. Eles também comem compulsivamente com mais frequência - pelo menos duas vezes por semana durante vários meses.

Para pessoas com transtorno da compulsão alimentar periódica, a primeira comida pode proporcionar uma sensação de calma ou confortá-los ou impedi-los de se sentirem chateados. Mas se a compulsão continuar, pode causar ansiedade, culpa e angústia. Uma farra geralmente envolve comer grandes quantidades de comida rapidamente. Enquanto comia, a pessoa se sente completamente fora de controle. Esses comportamentos pode se tornar um hábito, que geralmente é alternado com fazer dieta.

O transtorno da compulsão alimentar periódica é mais comum em pessoas com sobrepeso ou obesas, mas também afeta pessoas com peso saudável. No entanto, há poucas informações sobre quantas crianças e adolescentes têm, porque a condição só recentemente foi reconhecido. Algumas pessoas podem ficar com vergonha de procurar ajuda para isso.

Além disso, porque a maior parte da compulsão alimentar é feita sozinhos, mesmo que seus filhos estejam ganhando peso, os pais podem não estar cientes de que é devido à compulsão alimentar.

Embora outros distúrbios alimentares (como anorexia e bulimia) sejam muito mais comuns em mulheres, cerca de um terço das pessoas com transtorno da compulsão alimentar periódica são do sexo masculino. Adultos em tratamento (incluindo 2% dos americanos adultos - cerca de 1 milhão a 2 milhões de pessoas) costumam dizer que problemas que começaram na infância ou adolescência.

## Sinais e sintomas

Crianças e adolescentes que às vezes comem muito não têm necessariamente transtorno da compulsão alimentar periódica. As crianças podem ter apetite enorme, especialmente durante surtos de crescimento, quando precisam de mais nutrientes para abastecer seus corpos em crescimento. Portanto, pode ser difícil determinar se uma criança tem transtorno da compulsão alimentar periódica. Mas vários sinais distinguem alguém que bebe come de alguém com um "apetite saudável".

Os pais e outros membros da família podem primeiro suspeitar de um problema quando notam um grande quantidades de comida faltando na despensa ou na geladeira, embora seja difícil imaginar uma criança poderia ter comido tanto.

Outros sinais incluem:

* uma criança comendo muito rapidamente
* um padrão de alimentação em resposta ao estresse emocional, como conflito familiar, rejeição de colegas ou baixo desempenho acadêmico
* uma criança se sentindo envergonhada ou enojada com a quantidade que comeu
* encontrar recipientes para comida ou embalagens escondidas no quarto de uma criança
* um padrão de alimentação cada vez mais irregular, como pular refeições, comer muito de junk food e comer em horários incomuns (como tarde da noite)

Pessoas que comem compulsivamente podem ter sentimentos comuns a muitos transtornos alimentares, como depressão, ansiedade, culpa ou vergonha. Eles podem evitar a escola, o trabalho ou se socializar com amigos porque eles têm vergonha do problema de compulsão alimentar ou das mudanças na forma e no peso do corpo.

## Causas

As causas do transtorno da compulsão alimentar não são claras, embora os National Institutes da Saúde (NIH) relatam que até metade de todas as pessoas que têm também têm um histórico de depressão. Não se sabe, porém, se a compulsão alimentar provoca depressão ou se pessoas com depressão são propensas ao distúrbio.

Muitas pessoas que comem compulsivamente dizem que os episódios podem ser desencadeados por sentimentos de estresse, raiva, tristeza, tédio ou ansiedade. No entanto, mesmo se alguém se sentir melhor temporariamente ao comer, geralmente está associado a sentimentos de angústia. Mais comumente, depois uma farra que a pessoa se sente ansiosa, culpada e chateada por perder o controle.

## Como ele difere de outros transtornos alimentares

O transtorno da compulsão alimentar periódica é ligeiramente diferente de outros transtornos alimentares.

Pessoas com bulimia nervosa (às vezes chamada de síndrome do binge-purge) comer demais e depois purgar (vomitar ou usar laxantes) para evitar o ganho de peso. Eles também podem jejuar (parar de comer por um tempo) ou compulsivamente exercício após uma compulsão alimentar. Como pessoas que sofrem de transtorno da compulsão alimentar periódica, aqueles com bulimia nervosa comem repetidamente grandes quantidades de comida e sentem culpado ou envergonhado por isso. Ao contrário da Bulimia, no entanto, aqueles com transtorno da compulsão alimentar periódica não fazem ou são incapazes de limpar e, portanto, estão frequentemente acima do peso.

A anorexia nervosa também envolve sentimentos de culpa por comer. Enquanto as pessoas com transtorno da compulsão alimentar periódica comem sempre demais, as pessoas com anorexia morrer de fome, causando danos potencialmente fatais a seus corpos. Eles também pode se exercitar compulsivamente para perder peso, uma condição conhecida como anorexia atletica.

## Diagnóstico

Se o médico achar que seu filho pode ter um distúrbio alimentar, ele tomará um histórico médico completo. O médico também discutirá a história da família, padrões de comer na família e questões emocionais. Além disso, uma vontade física completa ser feito e testes de laboratório podem ser solicitados. Os exames de sangue podem procurar problemas médicos (como colesterol alto ou problemas de tireoide) que podem estar relacionados a peso pouco saudável.

Médicos e profissionais de saúde mental usam os critérios no Diagnóstico e Manual Estatístico IV (DSM-IV) quando identificam transtorno da compulsão alimentar periódica. Isso inclui:

* compulsão alimentar mais do que a maioria das pessoas poderia consumir em curtos períodos de tempo
* uma sensação de falta de controle sobre a alimentação
* sentimentos de angústia sobre comportamentos alimentares
* compulsão alimentar que acontece, em média, pelo menos 1 dias por semana durante 3 meses
* as compulsões não estão associadas a purgação regular com laxantes ou vômitos ou exercício excessivo
* episódios de compulsão alimentar associados a:
 * comer mais rápido do que o normal
 * comer até ficar desconfortavelmente cheio
 * comer quando não está com fome
 * comer sozinho ou em segredo
 * sentimentos de nojo, depressão ou culpa

## Tratamento

Como acontece com qualquer transtorno alimentar, também é importante que a criança tenha terapia para suporte e para ajudar a mudar comportamentos não saudáveis.

Diferentes tipos de terapia podem ajudar a tratar o transtorno da compulsão alimentar. Por exemplo, família terapia e terapia cognitivo-comportamental ensinam às pessoas técnicas para monitorar e mudar seus hábitos alimentares e a forma como respondem ao estresse. A terapia familiar inclui toda a família no processo de ajudar o indivíduo.

A terapia cognitivo-comportamental combina a abordagem de ajudar as pessoas a mudar a autodestruição pensamentos junto com a mudança de comportamento. O aconselhamento também ajuda os pacientes a olhar para relacionamentos que eles têm com outras pessoas e os ajuda a trabalhar nas áreas que os causam ansiedade. Em alguns casos, os médicos podem prescrever medicamentos para serem usados ​​na terapia.

Mas não existe uma solução rápida para qualquer distúrbio alimentar. O tratamento pode levar vários meses ou mais enquanto a pessoa aprende como ter uma abordagem mais saudável da alimentação. Apesar programas de controle de peso são úteis para algumas pessoas afetadas pelo transtorno da compulsão alimentar periódica, crianças e adolescentes não devem iniciar um programa de dieta ou controle de peso sem o conselho e supervisão de um médico.

Para alguns pais e familiares, o longo caminho para a recuperação pode ser frustrante e caro. Obtenha apoio para si mesmo através de grupos de pais ou lendo sobre o transtorno para que você possa ajudar seu filho e sua família a superar isso.

## Riscos e complicações

Muitas crianças e adolescentes com transtorno da compulsão alimentar periódica ficam acima do peso após meses de comer demais. Os riscos mais comuns para a saúde são os mesmos que acompanham a obesidade, incluindo diabetes, altos pressão arterial, níveis elevados de colesterol, doenças da vesícula biliar, doenças cardíacas, alguns tipos de câncer, depressão e ansiedade.

## Ajudando seu filho

Se você suspeita que seu filho tem problemas com compulsão alimentar, ligue para o médico para aconselhamento e encaminhamento para profissionais de saúde mental qualificados com experiência tratamento de distúrbios alimentares em crianças.

Tranquilize seu filho que você está lá para ajudar ou apenas ouvir. Comendo desordem pode ser difícil de admitir, e seu filho pode não estar pronto para reconhecer tendo um problema. Você também pode incentivar hábitos alimentares mais saudáveis, modelando seus própria relação positiva com alimentação e exercícios e por não usar comida como recompensa.

Com a ajuda de sua família, amigos e profissionais de apoio, seu filho pode começar a comer porções saudáveis ​​de alimentos e aprender a controlar o estresse de maneiras mais saudáveis.