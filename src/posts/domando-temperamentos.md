---
title: Domando temperamentos
permalink: domando-temperamentos
layout: post.hbs
---

# Domando temperamentos

Os pais esperam acessos de raiva das crianças de 2 a 3 anos. Mas explosões de raiva não sempre pare depois dos anos de criança. Crianças mais velhas às vezes têm problemas para lidar com a raiva e frustração também.

Algumas crianças perdem a calma de vez em quando, mas outras parecem ter mais dificuldade momento em que as coisas não saem do seu jeito. Crianças que tendem a ter reações fortes por natureza precisará de mais ajuda dos pais para controlar seus temperamentos.

Controlar as explosões pode ser difícil para as crianças - e ajudá-las a aprender a fazer portanto, é uma tarefa difícil para os pais que os amam. Tente ser paciente e positivo, e saiba que essas habilidades levam tempo para se desenvolver e que quase todas as crianças podem melhorar com o treinamento certo.

## Papel dos pais

Gerenciar crianças pode ser um desafio. Alguns dias mantendo a paz enquanto mantém seu legal parece impossível. Mas se você está reagindo a um surto de temperamento ocasional ou um padrão de explosões, controlar sua própria raiva quando as coisas esquentarem fará é mais fácil ensinar as crianças a fazerem o mesmo.

Para ajudar a domar um temperamento, tente ser o aliado de seu filho - vocês dois estão torcendo para seu filho triunfar sobre o temperamento que continua causando problemas.

Embora sua própria paciência possa ser desgastada por explosões de raiva, oposição, desafio, discutindo e respondendo, é durante esses episódios que você precisa de paciência a maioria. Claro que você está com raiva, mas o que conta é como você lida com isso.

Reagir ao colapso das crianças com gritos e acessos de raiva só vai ensinar que façam o mesmo (e na verdade está associado a um aumento no número de crianças comportamentos). Mas manter a calma e trabalhar com calma em uma situação frustrante permite mostrar - e ensinar - maneiras adequadas de lidar com a raiva e a frustração.

Digamos que você ouça seus filhos brigando por um brinquedo na outra sala. Você ignorou , na esperança de que eles mesmos resolvessem isso. Mas a discussão se transforma em gritos e logo você ouve portas batendo, o barulho de batidas e choro. Você decide envolva-se antes que alguém se machuque de verdade.

Quando você chega ao local da luta, pode estar no fim de sua própria corda. Afinal, o som de gritos é perturbador e você pode ficar frustrado que seus filhos não estão compartilhando ou tentando se dar bem. (E você sabe que este brinquedo eles estão lutando para se perder, quebrar ou ser ignorados em breve, de qualquer maneira!)

Qual é a melhor maneira de reagir? Com seu próprio autocontrole intacto. Ensino por exemplo, é sua ferramenta mais poderosa. Fale com calma, clareza e firmeza - não com raiva, culpa, críticas severas, ameaças ou humilhações.

Claro, é mais fácil falar do que fazer. Mas lembre-se de que você está tentando ensinar seus filhos como lidar com a raiva. Se você gritar ou ameaçar, você modelará e incutirá o tipos exatos de comportamento que você deseja desencorajar. Seus filhos verão que você é tão zangado e incapaz de controlar seu próprio temperamento que você não pode deixar de gritar - e isso não os ajudará a aprender a não gritar.

## O que você pode fazer

Regular as emoções e gerenciar o comportamento são habilidades que se desenvolvem lentamente ao longo do tempo durante a infância. Assim como qualquer outra habilidade, seus filhos precisarão aprender e praticar eles, com sua ajuda.

Se é incomum que seu filho tenha uma birra, quando uma acontece, é claro mas reveja calmamente as regras. Dizendo algo como "Eu sei que você está chateado, mas não grite e sem xingamentos, por favor "pode ​​ser tudo que seu filho precisa ouvir para recuperar a compostura. Em seguida, pacientemente dê uma instrução, como "diga-me por que você está chateado" ou "por favor peça desculpas ao seu irmão por chamá-lo desse nome. "Desta forma, você está orientando seu filho de volta a um comportamento aceitável e incentiva o autocontrole.

Além disso, diga ao seu filho o que acontecerá se ele ou ela não se acalmar - por exemplo, "Se você não se acalmar, você precisa ir para o seu quarto até poder pare de gritar. "

Crianças cujos acessos de raiva são rotineiros podem não ter o autocontrole necessário para lidar com a frustração e a raiva e precisa de mais ajuda para controlar essas emoções. Estes etapas podem ajudar:

Ajude as crianças a colocar em palavras. Se seu filho está no meio de uma explosão, descubra o que está errado. Se necessário, dê um tempo para seu filho para se acalmar ou lembrá-lo das regras e expectativas da casa - "Não há gritos ou coisas jogando; por favor, pare com isso agora e acalme-se." Lembrar seu filho para falar com você sem reclamar, ficar de mau humor ou gritar. Não se envolva com eles se continuarem a gritar ou reclamar, pois queremos ensiná-los que podem ganhe sua atenção por meio de um comportamento calmo. Assim que seu filho se acalmar, pergunte o que aconteceu ele ou ela tão chateado. Você pode dizer: "Use suas palavras para me dizer o que está errado e o que Você está louco por isso. "Isso ajuda seu filho a colocar as emoções em palavras e descobrir o que, se algo, precisa ser feito para resolver o problema. No entanto, não pressione muito para seu filho para falar naquele momento. Ele ou ela pode precisar de algum tempo para refletir antes de ser pronto para falar.

Ouça e responda. Quando seu filho coloca os sentimentos em palavras, cabe a você ouvir e dizer que entende. Se seu filho está lutando como palavras, ofereça alguma ajuda: "então isso o deixou com raiva", "você deve ter se sentido frustrado", ou "isso deve ter ferido seus sentimentos". Ofereça-se para ajudar a encontrar uma resposta se houver um problema a ser resolvido, um conflito a ser consertado ou um pedido de desculpas a ser feito. Muitas vezes, sentir-se ouvido e compreendido é tudo o que as crianças precisam para se acalmar. Mas enquanto reconhece os sentimentos de seu filho, deixe claro que emoções fortes não são uma desculpa para o mal comportamento. Deixe claro que não há problema em ficar com raiva, mas não há problema em reagir a essa raiva gritando ou batendo. "Eu sei que você está bravo, mas ainda não está certo bater. "Em seguida, diga ao seu filho algumas coisas para tentar. Algumas crianças realmente só precisam para ser "ouvido" primeiro.

Crie regras básicas claras e cumpra-as. Falar sobre a casa regras regularmente para que seus filhos saibam o que você espera deles. Seja claro sobre o que é e o que não é aceitável sem usar ameaças, acusações ou humilhações. Seus filhos receberá a mensagem se você fizer afirmações claras e simples sobre o que está fora dos limites e explique o que você deseja que eles façam. Você pode dizer: "Não há gritos nisso casa. Use suas palavras para me dizer o que está incomodando você. "

Aqui estão algumas outras regras de bom comportamento para experimentar:

* Nesta família, não batemos, empurramos ou empurramos.
* Não é permitido gritar.
* Não há portas batendo em nossa casa.
* Não há xingamentos.
* Não dizemos coisas más nesta família.
* Você não pode jogar ou quebrar coisas de propósito.

## Estratégias de enfrentamento para crianças

Crianças que aprenderam que não é certo gritar, bater e jogar coisas quando estão chateado precisa de outras estratégias para se acalmar quando está com raiva. Ofereça algumas ideias para ajudá-los a aprender maneiras seguras de liberar a raiva ou encontrar outras atividades que pode criar um humor melhor.

Faça uma pausa na situação. Diga a seus filhos que está tudo bem Afaste-se de um conflito para evitar uma explosão de raiva. Mudando para outra parte de na casa ou no quintal, uma criança pode conseguir algum espaço e trabalhar para se acalmar.

Encontre uma maneira (com segurança) de liberar a raiva. Pode não haver perfuração paredes, mas você pode sugerir algumas boas maneiras para uma criança desabafar. Fazendo um monte de pulos macacos, dançar pelo quarto ou sair e fazer piruetas são bons escolhas. Ou seu filho pode escolher escrever ou fazer um desenho do que é tão perturbador.

Aprenda a mudar. Este é difícil para crianças - e adultos, também. Explique que parte de se acalmar é passar de um estado de espírito realmente raivoso para um mais humor sob controle. Em vez de pensar na pessoa ou situação que causou a raiva, Incentive as crianças a pensar em outra coisa para fazer que pode melhorar o humor - como uma caminhada ao redor do quarteirão, um passeio de bicicleta, jogar um jogo, ler um favorito livro, cavando no jardim ou ouvindo uma música favorita. Experimente uma dessas coisas juntos para que vejam como fazer algo diferente pode mudar a maneira como uma pessoa sente.

## Construindo uma base sólida

Felizmente, episódios de muita raiva não acontecem com muita frequência para a maioria das crianças. Essa com problemas de temperamento muitas vezes têm um estilo ativo, obstinado e energia extra que precisa ser descarregado.

Experimente estes passos durante os tempos calmos - eles podem evitar problemas antes eles começam ajudando as crianças a aprender e praticar as habilidades necessárias para controlar o calor do o momento:

Certifique-se de que as crianças durmam o suficiente. Dormir é muito importante ao seu bem-estar. A ligação entre a falta de sono e o comportamento de uma criança não é sempre óbvio. Quando os adultos estão cansados, podem ficar mal-humorados ou com pouca energia, mas as crianças podem ficar hiperativas ou desagradáveis, ou ter comportamentos extremos.

A maioria dos requisitos de sono das crianças se enquadra um intervalo previsível de horas com base em sua idade, mas cada criança é um indivíduo único com necessidades distintas de sono.

Ajude-os a rotular as emoções. Ajude as crianças a adquirir o hábito de dizer o que eles estão sentindo e por que - por exemplo, "Estou bravo porque tenho que limpar meu quarto enquanto meus amigos estão brincando. "Usar palavras não tira uma criança de fazer uma tarefa árdua, mas a discussão pode acalmar a situação. Você está conversando em vez de um argumento. Elogie seu filho por falar sobre isso em vez de bater a porta, por exemplo.

Veja se as crianças praticam muita atividade física. Jogo ativo pode realmente ajude crianças que têm temperamento forte. Incentive seu filho a brincar fora de casa e praticar esportes gosta. Caratê, luta livre e corrida podem ser especialmente bons para crianças que estão tentando para colocar seus temperamentos sob controle. Mas qualquer atividade que faça o coração bater pode ajuda a queimar energia e estresse.

Incentive as crianças a assumir o controle. Compare um temperamento com um cachorro que ainda não aprendeu a se comportar e isso está correndo por todo o lugar recebendo nas coisas. Filhotes podem não querer ser maus - mas precisam ser treinados para que eles possam aprender que não há como comer sapatos, não pular sobre as pessoas ou certas móveis, etc. A questão é que o temperamento do seu filho - como um cachorrinho - precisa ser treinado para aprender quando está tudo bem para jogar, como usar toda essa energia extra, e como seguir as regras.

Reconheça os sucessos. Muitas vezes isso passa despercebido, então tenha certeza comentar sobre o quão bem seu filho lidou com uma situação difícil quando você vê comportamentos. Aponte especificamente o que você gostou na maneira como eles lidaram com isso, para eles estarão mais propensos a usar essas estratégias em situações futuras.

Tente ser flexível. Paternidade pode ser uma experiência cansativa, mas tente não ser muito rígido. Ouvir um coro constante de "não" pode ser desanimador para crianças. Às vezes, é claro, "não" é absolutamente a única resposta - "não, você não pode ande de bicicleta sem capacete! "Mas, outras vezes, você pode deixar as crianças ganharem um. Por exemplo, se seus filhos quiserem manter o jogo de bola wiffle um pouco mais e eles perguntam apropriadamente, talvez dê mais 15 minutos.

Tente identificar situações de "risco" e seja proativo. Por exemplo, se seu filho tem dificuldade com transições, avisa com antecedência. Da mesma forma, se seus filhos têm dificuldade em desligar a televisão quando solicitado, deixe claro por quanto tempo eles podem assistir TV ou jogue videogame e defina um timer de aviso de 5 minutos. Certifique-se de aplicar o acordo.

Como qualquer pessoa que esteja realmente zangada sabe, seguir um conselho sensato pode ser difícil quando as emoções estão altas. Dê aos seus filhos a responsabilidade de ficar sob controle, mas esteja presente para lembrá-los de como fazer.

A maioria das crianças pode aprender a lidar melhor com a raiva e a frustração. Mas se o seu criança muitas vezes se envolve em brigas e discussões com amigos, irmãos e adultos, adicionalmente pode ser necessária ajuda. Converse com os outros adultos na vida de seu filho - professores, conselheiros escolares e treinadores podem ajudar, e o médico do seu filho pode recomendar um conselheiro ou psicólogo.