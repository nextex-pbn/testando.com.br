---
title: Sujidade (Encoprese)
permalink: sujidade-encoprese
layout: post.hbs
---

# Sujidade (Encoprese)

## O que é encoprese?

Se o seu filho tem evacuações em outros lugares que não o banheiro, você sabe como isso pode ser frustrante. Os pais podem presumir que as crianças que sujam as calças estão comportando-se mal ou com preguiça de usar o banheiro quando têm vontade de ir.

Mas muitas crianças além da idade de ir ao banheiro ensinar (geralmente com mais de 4 anos) que suja suas roupas íntimas tem uma condição conhecido como encoprese (en-kah-PREE-sis). Eles têm um problema com seus intestinos que embotam o desejo normal de ir ao banheiro. Então eles não podem controlar os acidentes que geralmente se seguem.

A encoprese não é uma doença. É um sintoma que pode ter diferentes causas.

## Quais são os sinais e sintomas da encoprese?

No início, os pais podem pensar que seu filho tem um simples caso de diarreia. Mas quando continua acontecendo, fica claro que há outro problema, especialmente porque a criança não está doente.

À medida que o acúmulo de fezes alonga o cólon, os nervos têm dificuldade para identificar cérebro que é hora de um BM. Se não for tratada, a sujeira vai piorar. Então crianças podem perder o apetite ou reclamar de dores de estômago.

Um cocô grande e duro também pode causar um rasgo na pele ao redor do ânus que irá deixe sangue nas fezes, no papel higiênico ou no banheiro.

Os pais podem ficar frustrados se seus filhos parecerem não se incomodar com os acidentes de cocô, que acontecem principalmente durante as horas de vigília. A negação pode ser uma razão para uma criança parecer calma - as crianças não conseguem enfrentar a vergonha e a culpa que sentem sobre a doença. Alguns até tentam esconder suas cuecas sujas de seus pais.

Outro motivo pode ser mais científico: porque o cérebro se acostuma com o cheiro de cocô, a criança pode não perceber mais o odor.

## O que causa a encoprese?

A maioria dos casos de encoprese é devido à constipação. As fezes (cocô) são duras, secas e difíceis de evacuar quando uma pessoa está constipada. Muitos as crianças "seguram" seus BMs para evitar a dor que sentem quando vão ao banheiro, o que prepara o terreno para um acidente de cocô.

### Sobre constipação

Há uma grande variedade quando se trata de cocô "normal". Uma criança pode ter um ou dois BMs por dia, enquanto outro vai apenas três ou quatro vezes por semana.

Uma criança que passa em um BM leve e de tamanho médio sem problemas a cada 3 dias é não constipado. Mas uma criança que passa por um BM difícil (pequeno ou grande) todos os dias é. Assim como outras crianças que podem ir todos os dias, mas só passam bolinhas duras e sempre deixou cocô no reto.

As causas de cocô forte podem incluir:

* dieta
* doença
* não beber líquidos suficientes
* medo de ir ao banheiro durante o uso do penico
* acesso limitado a um banheiro ou banheiro que não seja particular (como na escola)

Algumas crianças podem desenvolver constipação crônica após eventos estressantes de vida, como um divórcio ou a morte de um parente próximo.

Cirurgia retal ou defeitos congênitos, como Hirschsprung doença e espinha bífida podem causar constipação ou encoprese sem constipação, mas isso é incomum.

## O que acontece com a encoprese?

Quando uma criança segura em BMs, o cocô começa a se acumular no reto e pode de volta ao cólon - e um ciclo frustrante começa.

A função do cólon é remover a água do cocô antes que ele passe. Quanto mais o cocô fica preso lá, quanto mais água é removida - e mais difícil é empurre o cocô grande e seco para fora. O grande cocô também estende o cólon, enfraquecendo os músculos lá e afetando os nervos que dizem a uma criança quando é hora de ir para o banheiro.

Então, o cólon não consegue empurrar facilmente o cocô duro para fora, e é doloroso passar. Assim, a criança continua a evitar ter BM, muitas vezes dançando, cruzando as pernas, fazendo caretas ou andando na ponta dos pés.

Com o tempo, o reto e a parte inferior do cólon ficam tão cheios que é difícil para o esfíncter (válvula muscular que controla a passagem das fezes para fora do ânus) para segurar o cocô. BMs parciais podem passar, fazendo com que a criança suja seu ou as calças dela. O cocô mais macio também pode vazar ao redor da grande massa de fezes e manchar a roupa íntima da criança quando o esfíncter relaxa.

As crianças não podem evitar essa sujeira - nem têm ideia do que está acontecendo - porque os nervos não estão enviando os sinais que regulam o cocô.

## Como a encoprese é diagnosticada?

Ligue para o médico se seu filho tiver algum destes sintomas de encoprese:

* cocô ou fezes líquidas na cueca quando seu filho não está doente
* cocô forte ou dor ao ter um BM
* BM entupidor de banheiros
* dor de barriga
* perda de apetite
* sangue no papel higiênico ao limpar ou pingar no vaso sanitário

Sujeira e constipação são os principais razão pela qual as crianças vão ver gastroenterologistas pediátricos (médicos que diagnosticam e tratam distúrbios do estômago e intestinos).

## Como a encoprese é tratada?

A encoprese não é um problema de comportamento ou uma simples falta de autocontrole. Punir ou humilhar uma criança com encoprese só piorará as coisas.

Em vez disso, converse com seu médico para obter ajuda para superar este desafio, mas tratável problema. O médico também pode recomendar que seu filho consulte um gastroenterologista.

O tratamento ocorre em três fases:

1. Esvaziamento do reto e cólon do cocô duro. Dependendo na idade da criança e outras coisas, o médico pode recomendar medicamentos, incluindo um amaciante de fezes, laxantes e / ou enemas. Dê laxantes e enemas apenas sob a supervisão de um médico. Nunca dê esses tratamentos em em casa sem primeiro consultar o seu médico.
2. Ajudar seu filho a começar a ter BMs regulares. Isso está feito com a ajuda de agentes amolecedores de fezes. É importante continuar usando o banco amaciantes / laxantes para dar aos intestinos a chance de encolher de volta ao tamanho normal. o os músculos do intestino são alongados, por isso precisam de tempo para se recuperar. Os pais também serão solicitados a agendar horários de uso de penico após as refeições (quando os intestinos são naturalmente estimulados). A criança ficará sentada no banheiro por cerca de 5 a 10 minutos. Isso ajuda as crianças a aprenderem a prestar atenção ao impulso de ir.
3. Reduzindo o uso de medicamentos de fezes. Como BMs regulares acontecem, o o médico diminuirá o uso de amaciantes e / ou laxantes de fezes por uma criança.

Lembre-se de que recaídas são normais, por isso não desanime. Seu filho pode ficar constipado novamente ou sujar as calças durante o tratamento, especialmente quando estiver desmamado dos amaciantes de fezes.

Uma boa maneira de acompanhar o progresso do seu filho é manter um calendário diário de cocô. Certifique-se de observar a frequência, consistência (dura, macia, seca) e o tamanho (grande, pequeno) dos BMs.

Paciência é a chave para tratar a encoprese. Pode demorar vários meses a um ano para o cólon alongado retornar ao seu tamanho normal e para os nervos no dois pontos para ter efeito novamente.

## A importância da dieta e dos exercícios

Dieta e exercícios são muito importantes para manter as fezes moles e regulares. Certifique-se de que seu filho tenha muitos alimentos ricos em fibras. Sirva fresco frutas, frutas secas como ameixas e passas, feijão seco, vegetais e alto teor de fibra pão e cereais.

Experimente estas formas criativas de adicioná-lo à dieta do seu filho:

* Asse biscoitos ou muffins usando farinha de trigo integral em vez da farinha normal. Adicionar passas, maçãs picadas ou em purê ou ameixas à mistura.
* Adicione farelo a itens de panificação, como biscoitos e muffins, ou bolo de carne ou hambúrgueres, ou polvilhado no cereal. (O truque é não adicionar muito farelo ou a comida terá gosto como serragem.)
* Sirva maçãs com manteiga de amendoim.
* Crie guloseimas saborosas com manteiga de amendoim e biscoitos de trigo integral.
* Top sorvete, iogurte congelado ou iogurte normal com cereais ricos em fibras para alguns adicionado crunch.
* Sirva waffles de farelo cobertos com frutas.
* Faça panquecas com mistura para panquecas de grãos inteiros e cubra com pêssegos, damascos ou uvas.
* Os principais cereais ricos em fibras com frutas.
* Transfira algumas passas ou purê de ameixas ou abobrinhas em panquecas de trigo integral.
* Adicione cenouras raladas ou purê de abobrinha ao molho de espaguete ou macarrão com queijo.
* Adicione lentilhas à sopa.
* Faça burritos de feijão com cascas de taco macio de grãos inteiros.

Ajude seu filho a beber muitos líquidos todos os dias, especialmente água. 100% diluído suco de frutas (como pêra, pêssego ou ameixa) é uma opção se seu filho não estiver bebendo água suficiente. Além disso, limitar a ingestão diária de laticínios de seu filho (incluindo leite, queijo, e iogurte) podem ajudar.

## Olhando para a Frente

O sucesso do tratamento da encoprese depende do apoio que a criança recebe. Alguns pais descobrir que o reforço positivo ajuda a encorajar a criança durante o tratamento. Por exemplo, coloque uma estrela ou adesivo no calendário de cocô por ter um BM (ou mesmo por tentar), sentar no banheiro ou tomar remédios.

Não culpe ou grite - isso só fará seu filho se sentir mal e não vai ajudar a controlar a condição. Com muito amor, apoio e garantia de que ele ou ela não é a única no mundo com esse problema, seu filho pode superar a encoprese.