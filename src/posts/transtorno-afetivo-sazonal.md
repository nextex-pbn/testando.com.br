---
title: Transtorno afetivo sazonal
permalink: transtorno-afetivo-sazonal
layout: post.hbs
---

# Transtorno afetivo sazonal

No início, os pais de Maggie pensaram que ela estava relaxando. Depois do Dia de Ação de Graças pausa, ela não conseguia se concentrar na aula, e depois da escola tudo o que ela queria fazer era dormir. Suas notas começaram a cair e ela raramente sentia mais vontade de se socializar. Eles estavam chateados com ela, mas percebi que era apenas uma fase - especialmente porque ela a energia finalmente parecia boa na primavera.

Mas quando a mesma coisa aconteceu no outono seguinte, e o humor de Maggie e suas notas despencou novamente, eles a levaram ao médico, que a diagnosticou com um tipo de depressão chamado de transtorno afetivo sazonal (SAD).

## Sobre o transtorno afetivo sazonal

Uma forma de depressão que segue um padrão sazonal, SAD aparece e desaparece nas mesmas épocas do ano. Pessoas com SAD geralmente apresentam sintomas de depressão como o inverno se aproxima e o dia fica mais curto. Quando a primavera voltar e os dias tornam-se mais longos novamente, eles experimentam alívio dos sintomas e um retorno ao normal humor e nível de energia.

## Sinais e sintomas

Como outras formas de depressão, os sintomas do TAS podem ser leves, graves ou em qualquer lugar entre. Sintomas mais leves interferem minimamente na capacidade de alguém de participar nas atividades cotidianas, enquanto os sintomas mais graves podem interferir muito mais.

Os sintomas do TAS são iguais aos da depressão, mas ocorrem durante um período específico época do ano. É o padrão sazonal de SAD - o fato de que os sintomas ocorrem apenas por alguns meses a cada inverno (por pelo menos 2 anos consecutivos), mas não durante outros estações - isso o distingue de outras formas de depressão.

Os sintomas de SAD podem incluir:

* Mudanças no humor: tristeza, irritabilidade e / ou sensação de desespero ou inutilidade na maioria das vezes por pelo menos 2 semanas; tendência a ser mais autocrítico e mais sensível do que o normal às críticas; chorar ou ficar chateado com mais frequência ou mais facilmente
* Falta de prazer: perda de interesse em coisas que são normalmente agradável; sentir que as tarefas não podem ser realizadas tão bem quanto antes; sentimentos de insatisfação ou culpa
* Baixa energia: cansaço incomum ou fadiga inexplicável
* Mudanças no sono: dormindo muito mais do que o normal (o que pode tornar difícil para as crianças com TAS se levantarem e se prepararem para a escola pela manhã)
* Mudanças na alimentação: desejo de carboidratos simples (ou seja, conforto alimentos e alimentos açucarados); tendência a comer demais (o que pode resultar em ganho de peso durante os meses de inverno)
* Dificuldade de concentração: mais dificuldade do que o normal para completar atribuições na hora; falta de motivação usual (o que pode afetar o desempenho escolar e notas)
* Menos tempo de socialização: passando menos tempo com amigos nas redes sociais ou atividades extracurriculares

Os problemas causados ​​pelo SAD - como notas abaixo do normal ou menos energia para socializar com amigos - pode afetar a auto-estima e deixar as pessoas sentindo desapontado, isolado e solitário, especialmente se eles não percebem o que está causando as mudanças de energia, humor e motivação.

## Causas de TAS

Acredita-se que com SAD, a depressão é de alguma forma desencadeada pela resposta do cérebro para diminuir a exposição à luz do dia. Como e por que isso acontece ainda não é totalmente compreendido. As teorias atuais enfocam o papel da luz solar na produção do cérebro de certos hormônios essenciais que ajudam a regular os ciclos de sono-vigília, energia e humor.

Acredita-se que dois produtos químicos que ocorrem naturalmente no corpo estejam envolvidos no SAD:

1. Melatonina, que está ligada ao sono. É produzido em maior quantidades quando está escuro ou quando os dias são mais curtos. Aumento da produção de melatonina pode causar sonolência e letargia.
2. Serotonina, cuja produção aumenta com a exposição à luz do sol. Baixos níveis de serotonina estão associados à depressão, aumentando a disponibilidade de serotonina ajuda a combater a depressão.

Dias mais curtos e horas mais longas de escuridão no outono e inverno podem aumentar a melatonina níveis e diminuir os níveis de serotonina, o que pode criar as condições biológicas para depressão.

## Quem conseguiu?

Cerca de 6 em cada 100 pessoas (6%) experimentam SAD. Embora possa afetar crianças e adolescentes, é mais comum em adolescentes mais velhos e adultos jovens, geralmente começando em o início dos anos vinte. Como outras formas de depressão, as mulheres são cerca de quatro vezes mais mais propensos do que os homens a desenvolver TAS, assim como as pessoas com parentes que tiveram depressão. Biologia individual, química do cérebro, história familiar, meio ambiente e experiências de vida também pode tornar certas pessoas mais propensas a SAD e outras formas de depressão.

A prevalência de SAD varia de região para região e é muito mais abundante entre pessoas que vivem em latitudes mais altas. Por exemplo, um estudo encontrou as taxas de SAD foram sete vezes maiores entre as pessoas em New Hampshire do que na Flórida, sugerindo que a vida mais longe do equador é um fator de risco para SAD.

No entanto, a maioria das pessoas não sofre de depressão sazonal, mesmo que more áreas onde os dias são muito mais curtos durante os meses de inverno. Aqueles que fazem podem ser mais sensíveis às variações de luz e sofrem mudanças mais dramáticas no hormônio produção dependendo de sua exposição à luz.

## Tratamento

O tratamento para SAD, que varia dependendo da gravidade dos sintomas, inclui:

Maior exposição à luz. Porque os sintomas de SAD são desencadeados por falta de exposição à luz e tendem a desaparecer por conta própria quando há luz disponível aumenta, o tratamento para SAD frequentemente envolve maior exposição à luz durante o inverno meses. Para alguém com sintomas leves, pode ser o suficiente para passar mais tempo fora durante o dia, talvez fazendo exercícios ao ar livre ou fazendo uma caminhada diária. Lâmpadas de espectro total (luz do dia) que se encaixam em lâmpadas regulares podem ajudar a trazer um pouco mais luz do dia nos meses de inverno e pode ajudar com sintomas leves.

Terapia de luz (fototerapia). Sintomas mais incômodos podem ser tratada com uma luz mais forte que simula a luz do dia. Uma caixa de luz ou painel especial é colocado em uma mesa ou escrivaninha, e a pessoa se senta na frente dela brevemente a cada dia (45 minutos ou mais, geralmente pela manhã) com os olhos abertos, olhando - não olhando - ocasionalmente para a luz (para funcionar, a luz tem que ser absorvida através retinas). Os sintomas tendem a melhorar em alguns dias ou semanas. Geralmente leve a terapia é usada até que luz solar suficiente esteja disponível ao ar livre. Efeitos colaterais leves de a fototerapia pode incluir dor de cabeça ou fadiga ocular.

Luzes usadas para fototerapia SAD devem filtrar os raios UV prejudiciais. Camas de bronzeamento ou cabines não devem ser usadas para aliviar os sintomas do SAD. Seus raios ultravioleta podem danificam a pele e causam rugas e manchas senis, e até levam ao câncer de pele, como melanoma. A fototerapia deve ser usada com cautela se alguém tiver outro tipo de transtorno depressivo, pele sensível à luz ou condições médicas que podem tornam os olhos vulneráveis ​​a danos causados ​​pela luz.

Como qualquer tratamento, a fototerapia deve ser usada sob a supervisão de um médico.

Medicação (farmacoterapia). Medicamentos, que podem ser usados em combinação com psicoterapia e terapia de luz, pode ser prescrito para uma criança ou adolescente com SAD e deve ser monitorado por um médico. Medicamentos antidepressivos ajudam para regular o equilíbrio da serotonina e outros neurotransmissores no cérebro que afetam o humor e a energia. Informe o seu médico sobre quaisquer outros medicamentos que seu filho tome, incluindo medicamentos sem receita ou fitoterápicos, que podem interferir na prescrição medicamentos.

Terapia da conversa (psicoterapia). Ajudando a aliviar a sensação de isolamento ou solidão, a psicoterapia se concentra em revisar os pensamentos e sentimentos negativos associado à depressão. Também pode ajudar as pessoas com SAD a compreender a sua condicionar e aprender maneiras de prevenir ou minimizar ataques futuros.

## O que Os pais podem fazer

Converse com seu médico se você suspeitar que seu filho tem SAD. Médicos e saúde mental profissionais fazem um diagnóstico de SAD após uma avaliação cuidadosa e um checkup para Certifique-se de que os sintomas não sejam causados ​​por uma condição médica que precise de tratamento. Cansaço, fadiga, alterações no apetite e no sono e baixa energia podem ser sinais de outros sintomas problemas, como hipotireoidismo, hipoglicemia ou mononucleose.

Quando os primeiros sintomas de SAD se desenvolvem, os pais podem atribuir baixa motivação, energia, e interesse por uma atitude intencional de má qualidade. Aprender sobre SAD pode ajudá-los a entender outra possível razão para as mudanças, amenizando sentimentos de culpa ou impaciência com seu filho ou adolescente.

Às vezes, os pais são elementos sobre como discutir suas preocupações e observações. A melhor abordagem geralmente é aquela que apóia e não faz julgamentos. Tente abrir a discussão com algo como: "Você não parece ser você mesmo recentemente - você tem estado tão triste e rabugento e cansado, e você não parece estar se divertindo muito ou recebendo sono suficiente. Então, marquei uma consulta para você fazer um check-up. eu quero ajudar você se sentir melhor e voltar a fazer o seu melhor e se divertir novamente. "

Aqui estão algumas coisas que você pode fazer se seu filho ou adolescente foi diagnosticado com SAD:

* Participe do tratamento do seu filho. Pergunte ao médico como você pode ajudar melhor seu filho.
* Ajude seu filho a entender o SAD. Aprenda sobre a doença e fornecer explicações simples. Lembre-se, a concentração pode ser difícil, por isso é improvável seu filho vai querer ler ou estudar muito sobre o SAD - em caso afirmativo, basta recapitular o pontos principais.
* Incentive seu filho a fazer bastante exercício e a passar o tempo ao ar livre. Faça uma caminhada diária juntos.
* Encontre tempo de qualidade. Passe um pouco mais de tempo com seu filho - nada de especial, apenas algo discreto que não requer muita energia. Leve para casa um filme de que você goste ou faça um lanche juntos. Sua empresa e carinho são importantes e fornecem contato pessoal e um senso de conexão.
* Seja paciente. Não espere que os sintomas desapareçam imediatamente. Lembrar que baixa motivação, baixa energia e baixo humor fazem parte do SAD - é improvável que seu filho responderá com alegria aos seus esforços para ajudar.
* Ajuda com o dever de casa. Você pode ter que fornecer ajuda temporária assistência para ajudar seu filho a organizar tarefas ou concluir o trabalho. Explique isso problemas de concentração fazem parte do SAD e que as coisas vão melhorar novamente. Crianças e adolescentes com TAS podem não perceber isso e se preocupar por serem incapazes de fazer o trabalho escolar. Você também pode querer falar com os professores e pedir extensões em atribuições até que as coisas melhorem com o tratamento.
* Ajude seu filho a comer bem. Incentive seu filho a evitar o carregamento com carboidratos simples e lanches açucarados. Fornece muitos grãos inteiros, vegetais, e frutas.
* Estabeleça uma rotina de sono. Incentive seu filho a seguir um hora de dormir regular todos os dias para colher os benefícios da luz do dia para a saúde mental.
* Leve isso a sério. Não adie a avaliação se você suspeitar de seu criança tem SAD. Se diagnosticado, seu filho deve aprender sobre o padrão sazonal de a depressão. Fale frequentemente sobre o que está acontecendo e ofereça garantias de que as coisas ficará melhor, embora possa parecer impossível agora.