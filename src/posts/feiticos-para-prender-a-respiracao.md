---
title: Feitiços para prender a respiração
permalink: feiticos-para-prender-a-respiracao
layout: post.hbs
---

# Feitiços para prender a respiração

## Sobre feitiços para prender a respiração

Muitos de nós já ouvimos histórias sobre crianças teimosas ou obstinadas que seguram seus respire até ficarem com o rosto azul. Isso pode parecer divertido "terrível contos de dois ", mas não são engraçados para os pais dessas crianças. Prender a respiração feitiços podem ser aterrorizantes para os pais porque as crianças muitas vezes prendem a respiração até que desmaiar.

Mas esses feitiços não são intencionais - eles são um reflexo involuntário, o que significa que as crianças não têm controle sobre eles. Embora sejam perturbadores de assistir, prendem a respiração feitiços não são prejudiciais e não representam riscos sérios à saúde. Um feitiço normalmente dura menos mais de um minuto antes de uma criança começar a respirar normalmente de novo.

Feitiços de retenção da respiração podem acontecer em crianças saudáveis ​​entre 6 meses e 6 anos velhos, mas são mais comuns durante o segundo ano de vida. Eles podem ser mais comuns em crianças com histórico familiar deles.

Na maioria dos casos, os feitiços de prender a respiração podem ser previstos e até mesmo evitados quando disparados são identificados. As crianças geralmente os superam aos 5 ou 6 anos.

## Tipos de feitiços de retenção da respiração

Os feitiços de retenção da respiração diferem por causa e características:

* Feitiços cianóticos de prender a respiração acontecem quando uma criança para de respirar e fica com o rosto azulado. Esses feitiços são frequentemente desencadeados por algo que perturba a criança, como ser disciplinado. Enquanto chorando, a criança expira (expira) e depois não inspira novamente por Um tempo. Pais que testemunharam cianose anterior feitiços sabem exatamente quando outro está para acontecer, porque o rosto de seu filho lentamente muda para um tom de azul, variando de azul claro a quase roxo.
* Feitiços de prender a respiração são menos comuns e mais imprevisíveis porque acontecem depois que uma criança tem teve um susto ou sobressalto repentino (como ser surpreendido por trás). Ao contrário de feitiços cianóticos, as crianças ficam muito pálidas, quase brancas, durante o feitiço.

Ambos os tipos de feitiços fazem com que as crianças parem de respirar e às vezes percam a consciência por até um minuto. Nos casos mais extremos, as crianças podem ter convulsões. Ter uma convulsão não causa nenhum dano a longo prazo nem coloca uma criança em risco de desenvolver um distúrbio convulsivo.

## Se uma criança desmaiar durante um feitiço

Na maioria das vezes, você não precisa fazer nada durante um período de prender a respiração. Seu filho deve ficar deitado até o feitiço acabar.

Se seu filho desmaiar por um breve período, fique calmo e:

* verifique a boca do seu filho para ver se há comida ou qualquer objeto que possa causar asfixia perigo uma vez que seu filho recupere a consciência
* remova todos os objetos ou móveis ao seu alcance, caso seu filho tenha uma convulsão

Crianças com crises de prender a respiração geralmente começam a respirar em um minuto.

Ligue para o 911 se o seu filho continuar azul ou não respirar por mais de um minuto.

## Quando consultar o médico

Se esta é a primeira vez de seu filho prender a respiração, procure atendimento médico. Apesar Feitiços de prender a respiração não são prejudiciais, é bom levar seu filho para ser examinado. Um médico pode determinar se foi de fato um feitiço de prender a respiração ou outra condição médica parece um.

Esses feitiços são uma resposta involuntária a emoções fortes (como estar com raiva, assustado ou frustrado) e tendem a acontecer em crianças saudáveis. Porque são involuntários (não feito de propósito), eles não são um problema de comportamento. Um médico pode ajudar os pais entender o que desencadeia um feitiço em seu filho, como prevenir feitiços futuros e como lidar com eles se acontecerem.

Às vezes, a anemia por deficiência de ferro pode fazer com que as crianças tenham feitiços de prender a respiração. Então, um médico pode fazer um exame de sangue teste para verificar se há anemia. O tratamento da anemia pode ajudar a reduzir o número de feitiços.

## Prevenção de feitiços futuros

Depois que as crianças amadurecem e desenvolvem melhores habilidades de enfrentamento, geralmente deixam de prender a respiração feitiços. Mas, enquanto isso, os pais podem enfrentar um desafio maior do que testemunhar os feitiços em si: encontrando maneiras de disciplinar seu filho que não irá desencadear outro feitiço.

Seu médico pode trabalhar com você para ajudá-lo a encontrar estratégias de enfrentamento para você e seu criança. Tente não ceder a acessos de raiva e comportamento teimoso - crianças pequenas precisam de limites e diretrizes para ajudá-los a permanecer seguros e a se ajustar emocionalmente.

Com experiência, coragem e a ajuda do seu médico, você pode aprender a lidar com a prender a respiração feitiços enquanto fornece um ambiente seguro e estruturado até que seu filho supere eles.