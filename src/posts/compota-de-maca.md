---
title: Compota de maçã
permalink: compota-de-maca
layout: post.hbs
---

# Compota de maçã

Tempo de preparação: 10 minutos

## O que você precisa:

* 2 pequenas maçãs vermelhas
* 2 colheres de sopa. suco de limão
* 2 colheres de chá açúcar
* 2 pitadas de canela

## O que fazer:

1. Descasque as maçãs e corte-as em pedaços pequenos. Jogue fora os núcleos.
2. Coloque os pedaços de maçã e o suco de limão no liquidificador ou processador de alimentos. Misture até a mistura é muito lisa.
3. Despeje a mistura em duas tigelas pequenas e acrescente o açúcar e a canela.
4. Aproveite a sua compota de maçã!

## Análise nutricional (por porção):

* 84 calorias
* 0g de proteína
* 0g de gordura
* 22g de carboidrato
* fibra 2g
* 0mg de colesterol
* 3mg de sódio
* 14 mg de cálcio
* 0,3 mg de ferro

Serve: 2

Tamanho da porção: 1 tigela (metade da receita)

Observação: a análise nutricional pode variar dependendo das marcas de ingredientes usadas.