---
title: Corte
permalink: corte
layout: post.hbs
---

# Corte

Cortar não é novidade, mas essa forma de automutilação (SI) tem sido mais divulgada nos últimos anos, retratado em filmes e na TV - até mesmo falado por celebridades que admitiram ter se cortado em algum momento.

O corte é um problema sério que afeta muitos adolescentes. Mesmo que você não tenha ouvido falar sobre corte, são boas as chances de seu filho adolescente ter conhecido, ou até mesmo conhecer, alguém que faça isto. Como outros comportamentos de risco, o corte pode ser perigoso e criar hábitos. Na maioria casos, é também um sinal de sofrimento emocional mais profundo. Em alguns casos, os pares podem influenciar adolescentes experimentarão cortes.

O tema do corte pode ser preocupante para os pais. Pode ser difícil de entender por que um adolescente se machucaria deliberadamente, e preocupante pensar que seu filho - ou um dos amigos do seu filho adolescente - pode estar em risco.

Mas os pais que estão cientes deste importante problema e entendem o aspecto emocional dor que ele pode sinalizar estão em uma posição de ajudar.

## O que está cortando?

Alguém que corta usa um objeto pontiagudo para fazer marcas, cortes ou arranhões no corpo propositalmente - o suficiente para romper a pele e causar sangramento. Pessoas normalmente cortam -se nos pulsos, antebraços, coxas ou barriga. Eles podem usar uma lâmina de barbear, faca, tesoura, uma aba de metal de uma lata de refrigerante, a ponta de um clipe de papel, uma lixa de unha, ou uma caneta. Algumas pessoas queimam a pele com a ponta de um cigarro ou fósforo aceso.

A maioria das pessoas que se autoflagelam são meninas, mas os caras também fazem isso. Geralmente começa durante a adolescência e pode continuar na idade adulta. Em alguns casos, há uma história familiar de corte.

Um sentimento de vergonha e sigilo geralmente acompanha o corte. A maioria dos adolescentes que cortam esconda as marcas e se elas forem percebidas, invente desculpas a respeito delas. Alguns adolescentes não tente esconder cortes e pode até chamar atenção para eles.

O corte geralmente começa como um impulso. Mas muitos adolescentes descobrem que assim que começam para cortar, eles fazem isso cada vez mais e podem ter problemas para parar. Muitos adolescentes que se machucam relatam que o corte proporciona uma sensação de alívio de emoções profundas e dolorosas. Porque disso, o corte é um comportamento que tende a se reforçar.

Cortar-se pode se tornar a maneira habitual de um adolescente responder a pressões insuportáveis sentimentos. Muitos dizem que se sentem "viciados" no comportamento. Alguns gostariam de parar, mas não sabem ou sentem que não podem. Outros adolescentes não querem parar de cortar.

Na maioria das vezes, cortar não é uma tentativa de suicídio. Mas, infelizmente, as pessoas muitas vezes subestimam o potencial de ficar gravemente doente ou ferido por meio de sangramento ou infecções que vão junto com o corte.

## Por que os adolescentes cortam?

Adolescentes cortam por muitos motivos diferentes:

Emoções poderosas e avassaladoras. A maioria dos adolescentes que cortam estão lutando com emoções poderosas. Para eles, cortar pode parecer a única forma de expressar ou interromper sentimentos que parecem intensos demais para suportar. Dor emocional por causa da rejeição, relacionamentos perdidos ou rompidos ou tristeza profunda podem ser opressores para alguns adolescentes.

E muitas vezes eles estão lidando com dor emocional ou situações difíceis que ninguém sabe sobre. Pressão para ser perfeito ou viver de acordo com padrões impossíveis - seu próprio ou de outra pessoa - pode causar dor insuportável a alguns adolescentes. Alguns adolescentes que cortaram foram profundamente feridos por um tratamento duro ou por situações que os deixaram sentindo-se sem apoio, impotente, indigno ou não amado.

Alguns adolescentes sofreram traumas, que podem causar ondas de dormência emocional chamada dissociação. Para eles, o corte pode ser uma forma de testar se ainda podem "sentir dor. Outros descrevem o corte como uma forma de "acordar" desse entorpecimento emocional.

A dor física autoinfligida é específica e visível. Para alguns, a dor física do corte pode parecer preferível à dor emocional. Dor emocional pode parecer vago e difícil de identificar, falar ou acalmar.

Quando se cortam, os adolescentes dizem que há uma sensação de controle e alívio ao ver e saber de onde vem a dor específica e uma sensação de conforto quando ela pára. Corte pode simbolizar a dor interior que pode não ter sido verbalizada, confiada, reconhecida, ou curado. E porque é auto-infligido, o adolescente controla a dor.

Uma sensação de alívio. Muitos adolescentes que cortam descrevem a sensação de alívio eles sentem que estão cortando, o que é comum com comportamentos compulsivos. Alguns as pessoas acreditam que as endorfinas podem adicionar ao alívio que os adolescentes descrevem quando cortam. As endorfinas são os hormônios do "bem-estar" liberados durante o esforço físico intenso. E eles podem ser liberados durante uma lesão.

Outros acreditam que o alívio é simplesmente o resultado de ser distraído de emoções dolorosas pela intensa dor física e pela visão dramática de sangue. Alguns adolescentes dizem que não sentir a dor ao cortar, mas sinta-se aliviado porque o SI visível "mostra" dor que sentem.

Sentindo-se "viciado". O corte pode ser viciante. Embora seja apenas fornece alívio temporário do estresse emocional, quanto mais uma pessoa corta, mais ele ou ela sente necessidade de fazê-lo. Tal como acontece com outros comportamentos compulsivos, o cérebro começa para conectar uma sensação momentânea de alívio de sentimentos ruins com o ato de cortar.

Sempre que a tensão aumenta, o cérebro anseia por esse alívio e leva o adolescente a busque alívio cortando novamente. Então, cortar pode se tornar um hábito de alguém se sentir impotente parar. O desejo de cortar - para obter alívio - pode parecer muito difícil de resistir quando a pressão emocional é alta.

Outras condições de saúde mental. O corte está frequentemente ligado a - ou parte de - outra condição de saúde mental. Alguns adolescentes que se cortam também estão lutando com outros impulsos, obsessões ou comportamentos compulsivos. Para alguns, depressão ou transtorno bipolar pode contribuir para o humor opressor isso pode ser difícil para um adolescente regular. Para outros, problemas de saúde mental que afetam a personalidade podem fazer com que os relacionamentos pareçam intensos e desgastantes, mas instável. Para esses adolescentes, apegos positivos intensos podem de repente se tornar terrivelmente decepcionar e deixá-los se sentindo magoados, com raiva ou desespero muito fortes para lidar.

Outros adolescentes lutam com traços de personalidade que os atraem para o perigoso excitação de comportamento de risco ou atos autodestrutivos. Alguns são propensos a dramáticos maneiras de obter a garantia de que são amados e preocupados. Para outros, pós-traumático o estresse afetou sua capacidade de enfrentamento. Ou eles estão lutando com problemas com álcool ou substâncias.

Pressão dos pares. Alguns adolescentes são influenciados a começar a cortar por outro pessoa que faz isso. Por exemplo, uma adolescente pode tentar cortar porque seu namorado cortes. A pressão do grupo também pode desempenhar um papel. Alguns adolescentes se separam em grupos e podem pressione outros para cortar. Um adolescente pode ceder à pressão do grupo para tentar cortar como um maneira de parecer descolado ou ousado, de pertencer ou de evitar o bullying social.

Qualquer um desses fatores pode ajudar a explicar por que um determinado adolescente corta. Mas cada adolescente também tem sentimentos e experiências únicas que desempenham um papel. Alguns que cortam podem não ser capaz de explicar por que eles fazem isso.

Independentemente dos fatores que podem levar um adolescente à automutilação, cortar não é um maneira saudável de lidar com as emoções ou pressões mais extremas.

## Confrontando Corte

Alguns adolescentes chamam atenção para a automutilação. Ou se o SI requer atenção médica, essa pode ser uma maneira de outras pessoas descobrirem. Mas muitos adolescentes cortam por muito tempo antes que qualquer um mais sabe. Alguns adolescentes eventualmente contam a alguém sobre sua automutilação - porque eles querem ajuda e querem parar, ou porque eles apenas querem que alguém entenda o que eles estão passando.

É preciso coragem e confiança para entrar em contato. Muitos adolescentes hesitam em contar aos outros porque temem ser mal compreendidos ou temem que alguém possa estar com raiva, chateado, desapontado, chocado ou crítico. Alguns adolescentes confiam em amigos, mas pergunte a eles não contar. Isso pode criar peso e preocupação para um amigo que conhece.

Se confrontados com o corte, os adolescentes podem responder de maneiras diferentes, dependendo em parte no adolescente e em parte em como eles foram abordados por ele. Alguns podem negar o corte, enquanto outros podem admitir, mas negam que seja um problema. Alguns podem ficar com raiva e chateado ou rejeitar esforços para ajudar. Alguns adolescentes ficam aliviados que alguém sabe, se importa e quer ajudar.

## Trazendo uma parada para o corte

Quer alguém saiba ou tenha tentado ajudar, alguns adolescentes cortam por muito tempo tempo antes de tentarem parar. Adolescentes cujo corte faz parte de outra saúde mental condição geralmente precisa de ajuda profissional. Às vezes, um corte ou outro sintoma leva à admissão de um adolescente a uma saúde mental hospital ou clínica. Alguns adolescentes têm mais de uma internação hospitalar por automutilação antes eles se sentem prontos para aceitar ajuda para cortes ou outros problemas.

Alguns adolescentes descobrem uma maneira de parar de cortar por conta própria. Isso pode acontecer se um adolescente encontra um motivo poderoso para parar (como perceber o quanto dói um amigo), precisava de apoio, ou encontra maneiras de resistir ao poderoso impulso de cortar. Para parar de cortar, uma pessoa também precisa encontrar novas maneiras de lidar com situações problemáticas e regular emoções que parecem opressoras. Isso pode levar tempo e frequentemente requer a ajuda de um profissional de saúde mental.

Pode ser difícil parar de cortar e um adolescente pode não ter sucesso no início. Alguns as pessoas param um pouco e depois começam a cortar novamente. É preciso determinação, coragem, força - bem como o apoio de outros que entendem e se preocupam - para pare com esse poderoso hábito.