---
title: Treinamento de banheiro
permalink: treinamento-de-banheiro
layout: post.hbs
---

# Treinamento de banheiro

## Quando as crianças estão prontas para treinar no banheiro?

Muitos pais não têm certeza de quando começar o treinamento para usar o banheiro ou "usar o penico". Nem todas as crianças estão prontas na mesma idade, por isso é importante cuidar de seu filho por sinais de prontidão, como interromper uma atividade por alguns segundos ou agarrar seu ou a fralda dela.

Em vez de usar a idade, procure sinais de que seu filho pode estar pronto para começar a jogar para o penico, como ser capaz de:

* siga instruções simples
* entender e usar palavras sobre como usar o penico
* faça a conexão entre a vontade de fazer xixi ou cocô e usar o penico
* mantenha a fralda seca por 2 horas ou mais
* vá até o penico, sente-se nele por tempo suficiente e depois saia do penico
* abaixar fraldas, calças de treino descartáveis ​​ou cuecas
* mostrar interesse em usar o penico ou usar cuecas

A maioria das crianças começa a mostrar esses sinais entre 18 e 24 meses, embora alguns possam não estar prontos até mais tarde. E os meninos muitas vezes começam mais tarde e demoram mais para aprender a usar o penico do que as meninas.

Há momentos em que você pode adiar o início do treinamento de toalete, como como:

* ao viajar
* em torno do nascimento de um irmão
* mudar do berço para a cama
* mudança para uma nova casa
* quando seu filho está doente (especialmente se diarreia é um fator)

## Quanto tempo leva o treinamento para usar o banheiro?

Ensinar uma criança a usar o penico não é tarefa da noite para o dia. Geralmente leva entre 3 e 6 meses, mas pode levar mais ou menos tempo para algumas crianças. Se você começar também logo, o processo tende a demorar mais. E isso pode levar meses a até anos para conseguir ficar seco à noite.

### Tipos de potty

As duas opções básicas de potty são:

* uma cadeirinha autônoma pequena com uma tigela que pode ser esvaziada no banheiro
* um assento pequeno que pode ser colocado em cima de um assento de vaso sanitário que permitirá que o seu criança sente-se mais segura e não tem medo de cair. Se você escolher isso, dê uma olhada banquinho para que seu filho possa alcançar o assento confortavelmente e se sentir apoiado enquanto evacuação.

Geralmente é melhor para os meninos aprenderem a usar o banheiro sentados antes aprendendo a fazer xixi em pé. Para meninos que se sentem constrangidos - ou com medo - sobre de pé em um banquinho para fazer xixi no banheiro, uma cadeira com penico pode ser uma opção melhor.

Você pode querer obter um penico ou assento de treinamento para todos os banheiros da sua casa. Você pode até querer manter um penico no porta-malas do seu carro para emergências. Ao viajar longas distâncias, certifique-se de levar um penico com você e parar a cada 1 a 2 horas. Caso contrário, pode demorar muito para encontrar um banheiro.

## Sobre Calças de Treinamento

Calças de treino descartáveis ​​são uma etapa útil entre as fraldas e as roupas íntimas. Porque o controle noturno da bexiga e do intestino das crianças geralmente fica para trás em relação ao controle diurno, alguns pais gostam de usar calças de treino à noite. Outros preferem que seus filhos usem calças de treino quando estão fora de casa. Uma vez que as calças de treinamento permanecem secas por alguns dias, as crianças podem mudar para usar roupas íntimas.

Mas algumas pessoas pensam que calças de treino descartáveis ​​podem fazer as crianças pensarem que é OK para usá-los como fraldas, assim retardando o processo de ensino do banheiro.

Pergunte ao seu médico se seu filho gostaria de usar calças esportivas descartáveis como uma etapa de transição.

## Dicas para treinar o banheiro

Mesmo antes de seu filho estar pronto para experimentar o penico, você pode preparar seu filho ensinando sobre o processo:

* Use palavras para expressar o ato de usar o banheiro ("xixi", "cocô" e "penico").
* Peça ao seu filho para avisar quando uma fralda estiver molhada ou suja.
* Identifique comportamentos ("Você está fazendo cocô?") para que seu filho possa aprender a reconhecer a vontade de fazer xixi e cocô.
* Pegue uma cadeirinha onde seu filho possa praticar sentado. No início, seu filho pode sente-se com roupas ou fraldas. Quando estiver pronto, seu filho pode ficar sem camisa.

Se você decidiu que seu filho está pronto para começar a aprender a usar o penico, essas dicas podem ajudar:

* Reserve algum tempo para se dedicar ao processo de treinamento do penico.
* Não faça seu filho sentar no banheiro contra a vontade dele.
* Mostre a seu filho como você senta no banheiro e explique o que está fazendo (porque seu filho aprende observando você). Você também pode fazer seu filho sentar no penico e observe enquanto você (ou um irmão) usa o banheiro.
* Estabeleça uma rotina. Por exemplo, você pode querer começar fazendo seu filho sentar no penico depois de acordar com uma fralda seca, ou 45 minutos a uma hora depois de beber muitos líquidos. Apenas coloque seu filho no penico por alguns minutos algumas vezes um dia e deixe seu filho levantar se ele quiser.
* Faça seu filho sentar no penico de 15 a 30 minutos após as refeições para aproveitar da tendência natural do corpo de evacuar após comer (isso é chamado reflexo gastro-cólica). Além disso, muitas crianças têm uma hora do dia em que tendem a ter intestinos movimento.
* Peça a seu filho para sentar no penico se você vir pistas claras de que precisa ir o banheiro, como cruzar as pernas, grunhir ou agachar.
* Esvazie o intestino (cocô) da fralda de seu filho no banheiro e diga seu filho aquele cocô vai para o penico.
* Evite roupas difíceis de tirar, como macacões e camisetas que se agarram na virilha. As crianças que estão treinando o penico precisam ser capazes de se despir.
* Ofereça ao seu filho pequenas recompensas, como adesivos ou tempo lendo, sempre que seu criança vai no penico. Mantenha um gráfico para controlar os sucessos. Uma vez seu pequeno parece estar dominando o uso do banheiro, deixe-o escolher alguns novos pares de roupa íntima infantil para vestir.
* Certifique-se de que todos os cuidadores - incluindo babás, avós e creches trabalhadores - seguem a mesma rotina e usam os mesmos nomes para partes do corpo e atos de banheiro. Deixe-os saber como você está lidando com o treinamento do banheiro e peça que eles use as mesmas abordagens para que seu filho não fique confuso.
* Elogie todas as tentativas de usar o banheiro, mesmo que nada aconteça. E lembre-se disso acidentes vão acontecer. É importante não punir crianças que treinam penico ou programas decepção quando se molham ou sujam a si próprios ou a cama. Em vez disso, diga ao seu filho que foi um acidente e ofereça seu apoio. Tranquilize seu filho que ele ou ela está no bom caminho para usar o penico como uma criança grande.

## Problemas comuns de treinamento de toalete

Muitas crianças que usam o penico têm alguns problemas durante os períodos de estresse. Por exemplo, uma criança de 2 ou 3 anos lidando com um novo irmão pode começar a ter acidentes.

Mas se seu filho foi treinado para usar o penico e tem problemas regularmente, converse com seu médico.

Converse com seu médico se tiver alguma dúvida sobre o treinamento para usar o banheiro ou sobre seu filho tem 4 anos ou mais e ainda não aprendeu a usar o penico.