---
title: Quando um animal de estimação morre
permalink: quando-um-animal-de-estimacao-morre
layout: post.hbs
---

# Quando um animal de estimação morre

Para a maioria das crianças, os animais de estimação são mais do que apenas os animais de sua família - eles são membros da família e os melhores amigos.

Infelizmente, a alegria de ter um animal de estimação anda de mãos dadas com o desgosto de perder um, seja por causa da velhice, doença ou acidente.

E isso pode ser muito difícil. Afinal, os animais de estimação costumam ser os primeiros a cumprimentar crianças de manhã e depois da escola. Seu animal de estimação pode ser aquele para quem seu filho olha para conforto e companhia quando estiver doente ou se sentindo impopular ou chateado.

Embora seja impossível proteger as crianças da perda de um animal de estimação, você pode ajudá-las lide com isso. E porque a morte de um animal de estimação pode ser a primeira vez que perde um ente querido, o processo de luto pode ajudar as crianças a aprender como lidar com outras perdas durante vida.

## Compartilhando as notícias e o luto

Uma das partes mais difíceis de perder um animal de estimação pode ser dar as más notícias para crianças. Tente fazer isso individualmente em um lugar onde se sintam seguros e confortáveis ​​e não se distrai facilmente.

Como você faria com qualquer problema difícil, tente avaliar a quantidade de informações que as crianças precisam ouvir com base em sua idade, nível de maturidade e experiência de vida.

Se o seu animal de estimação for muito velho ou tiver uma doença longa, considere falar com as crianças antes a morte acontece. Se você tiver que sacrificar seu animal de estimação, você pode querer explicar que:

* os veterinários fizeram tudo o que podiam
* seu animal de estimação nunca ficaria melhor
* esta é a maneira mais gentil de aliviar a dor do animal
* o animal morrerá em paz, sem se sentir ferido ou com medo

Mais uma vez, a idade, o nível de maturidade e as perguntas de uma criança ajudarão a determinar se para oferecer uma explicação clara e simples sobre o que vai acontecer. Se sim, tudo bem usar palavras como "morte" e "morrer" ou dizer algo como "O veterinário vai dê ao nosso animal de estimação uma injeção que primeiro o coloque para dormir e depois pare de bater o coração. " Muitas crianças querem uma chance de se despedir de antemão, e algumas podem ter idade suficiente ou ser emocionalmente maduro o suficiente para estar presente para confortar o animal durante o processo.

Se você tiver que sacrificar seu animal de estimação, tenha cuidado ao dizer que o animal foi "para dormir "ou" foi colocado para dormir ". Crianças pequenas tendem a interpretar as coisas ao pé da letra, então isso pode evocar ideias assustadoras sobre sono ou cirurgia e anestesia.

Se a morte do animal de estimação for mais repentina, explique com calma o que aconteceu. Seja breve, e deixe as perguntas do seu filho orientarem a quantidade de informações que você fornecer.

## Furar para a verdade

Evite tentar encobrir o evento com uma mentira. Dizendo a uma criança que "Buster correu longe "ou" Max saiu de viagem "não é uma boa ideia. Provavelmente não vai aliviar o tristeza por perder o animal de estimação, e se a verdade for revelada, seu filho provavelmente fique com raiva por ter mentido.

Se for perguntado o que acontece com o animal de estimação depois que ele morre, use seu próprio entendimento da morte, incluindo, se for o caso, o ponto de vista de sua fé. E já que nenhum de nós sabe perfeitamente, um "não sei" honesto certamente pode ser uma resposta apropriada - não há problema em dizer às crianças que a morte é um mistério.

## Ajudando seu filho a superar

Como qualquer pessoa que enfrenta uma perda, as crianças geralmente sentem uma variedade de emoções além tristeza após a morte de um animal de estimação. Eles podem sentir solidão, raiva se o animal de estimação foi sacrificado, frustração porque o animal de estimação não poderia melhorar ou culpa com o tempo que eles foram malvados ou não cuidaram do animal, conforme prometido.

Ajude as crianças a entender que é natural sentir todas essas emoções, que é Tudo bem em não querer falar sobre eles no início, e que você estará lá quando eles estiverem prontos para conversar.

Não se sinta compelido a esconder sua própria tristeza por perder um animal de estimação. Mostrando como você sentir e falar abertamente sobre isso é um exemplo para as crianças. Você mostra que está tudo bem para ficar triste quando você perde um ente querido, para falar sobre seus sentimentos e chorar quando você sentir-se triste. E é reconfortante para as crianças saber que não são as únicas que se sentem tristes. Compartilhe histórias sobre os animais de estimação que você teve - e perdeu - quando era jovem e como foi difícil dizer adeus.

## Olhando para a Frente

Depois que o choque da notícia acabar, é importante ajudar seu filho a se curar e se mover ligado.

Pode ajudar as crianças a encontrar maneiras especiais de se lembrar de um animal de estimação. Você pode ter uma cerimônia para enterrar seu animal de estimação ou apenas compartilhar memórias de momentos divertidos que passaram juntos. Escrever uma oração juntos ou oferecer idéias sobre o que o animal de estimação significava para cada membro da família. Compartilhe histórias dos momentos engraçados do seu animal de estimação. Ofereça muitos abraços amorosos. Você também poderia fazer um projeto, como fazer um álbum de recortes.

Lembre-se de que lamentar a perda de um animal de estimação, principalmente de uma criança, é semelhante ao luto por uma pessoa. Para as crianças, perder um animal de estimação que ofereceu amor e companhia pode ser muito mais difícil do que perder um parente distante. Você pode ter que explicar isso para amigos, familiares ou outras pessoas que não têm animais de estimação ou não entendem isso.

Talvez o mais importante, fale sobre o seu animal de estimação, frequentemente e com amor. Deixe seu filho saiba que enquanto a dor vai embora, as memórias felizes do animal de estimação sempre permanecerão. Quando chegar a hora certa, você pode considerar a adoção de um novo animal de estimação - não como um substituto, mas como uma forma de receber outro amigo animal em sua família.