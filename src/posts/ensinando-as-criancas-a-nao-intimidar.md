---
title: Ensinando as crianças a não intimidar
permalink: ensinando-as-criancas-a-nao-intimidar
layout: post.hbs
---

# Ensinando as crianças a não intimidar

Pode ser chocante e perturbador saber que seu filho se meteu em problemas por implicar com os outros ou ser rotulado de valentão.

Por mais difícil que seja processar essas notícias, é importante para lidar com isso imediatamente. Se o bullying é físico ou verbal, se não for interrompido pode levar a um comportamento anti-social mais agressivo e interferir com o seu o sucesso da criança na escola e a capacidade de formar e manter amizades.

## Compreendendo o comportamento de intimidação

As crianças fazem bullying por vários motivos. Alguns intimidam porque se sentem inseguros. Mexendo com alguém que parece emocionalmente ou fisicamente mais fraco proporciona uma sensação de ser mais importante, popular ou no controle. Em outros casos, as crianças intimidam porque simplesmente não sei que é inaceitável escolher crianças que são diferentes por causa do tamanho, aparência, raça ou religião.

Em alguns casos, o bullying faz parte de um padrão contínuo de atitude desafiadora ou agressiva comportamento. Essas crianças provavelmente precisarão de ajuda para aprender a controlar a raiva e a mágoa, a frustração, ou outras emoções fortes. Eles podem não ter as habilidades necessárias para cooperar com outras. O aconselhamento profissional muitas vezes pode ajudá-los a aprender a lidar com seus sentimentos, conter o bullying e melhorar suas habilidades sociais.

Algumas crianças que fazem bullying na escola e em ambientes com seus colegas estão copiando comportamentos que eles veem em casa. Crianças expostas a interações agressivas e rudes na família, muitas vezes aprende a tratar os outros da mesma maneira. E crianças que estão recebendo fim da provocação, aprenda que o bullying pode se traduzir em controle sobre as crianças perceber como fraco.

## Ajudando as crianças a parar com o bullying

Deixe seu filho saber que o bullying é inaceitável e que haverá casos graves Consequências em casa, na escola e na comunidade se continuar.

Tente entender as razões por trás do comportamento de seu filho. Em alguns casos, crianças valentão porque têm dificuldade em controlar emoções fortes, como raiva, frustração, ou insegurança. Em outros casos, as crianças não aprenderam maneiras cooperativas de resolver conflitos e entender as diferenças.

## Táticas a serem tentadas

Certifique-se de:

* Leve o bullying a sério. Certifique-se de que seus filhos entendam que você não tolerará o bullying em casa ou em qualquer outro lugar. Estabeleça regras sobre o bullying e cumpri-los. Se você punir seu filho tirando privilégios, certifique-se de que é significativo. Por exemplo, se seu filho intimida outras crianças por e-mail, mensagens de texto, ou um site de rede social, telefone dock ou privilégios de computador por um período de tempo. Se seu filho age agressivamente em casa, com irmãos ou outras pessoas, pare com isso. Ensine maneiras mais apropriadas (e não violentas) de reagir, como ir embora.
* Ensine as crianças a tratar os outros com respeito e bondade. Ensine seu criança que é errado ridicularizar as diferenças (por exemplo, raça, religião, aparência, necessidades especiais, gênero, situação econômica) e tente incutir um senso de empatia por aqueles que são diferentes. Considere se envolver em um grupo comunitário onde seu filho pode interagir com crianças que são diferentes.
* Saiba mais sobre a vida social de seu filho. Procure informações sobre fatores que podem estar influenciando o comportamento de seu filho no ambiente escolar (ou onde quer que o bullying esteja ocorrendo). Converse com os pais dos amigos do seu filho e colegas, professores, orientadores e o diretor da escola. As outras crianças intimidam? E os amigos do seu filho? Que tipo de pressão as crianças enfrentam na escola? Converse com seus filhos sobre esses relacionamentos e sobre as pressões para se adaptarem. Obter eles envolvidos em atividades fora da escola para que eles se encontrem e desenvolvam amizades com outras crianças.
* Incentive o bom comportamento. O reforço positivo pode ser mais poderoso do que disciplina negativa. Veja seus filhos sendo bons - e quando eles lidam com Situações de forma construtiva ou positiva, preste atenção e elogie-as para isso.
* Dê um bom exemplo. Pense bem em como você fala seus filhos e como você lida com conflitos e problemas. Se você se comportar de forma agressiva - em direção ou na frente de seus filhos - é provável que eles sigam seu exemplo. Em vez de, apontar pontos positivos em outras pessoas, em vez de negativos. E quando surgem conflitos em sua própria vida, seja aberto sobre as frustrações que você tem e como você lida com sua sentimentos.

## Começando em casa

Ao procurar as influências no comportamento do seu filho, observe primeiro o que acontecendo em casa. Crianças que vivem com gritos, xingamentos, críticas severas, ou raiva física de um irmão ou pai / responsável pode representar isso em outras situações.

É natural - e comum - as crianças brigarem com os irmãos em casa. E, a menos que haja risco de violência física, é aconselhável não se envolver. Mas monitore os xingamentos e quaisquer altercações físicas e certifique-se de falar com cada criança regularmente sobre o que é aceitável e o que não é.

É importante controlar seu próprio comportamento também. Observe como você fala com o seu crianças, e como você reage às suas próprias emoções fortes quando elas estão por perto. Haverá ser situações que justifiquem disciplina e crítica construtiva. Mas tome cuidado não para deixar isso escapar para xingamentos e acusações. Se você não está satisfeito com o seu o comportamento da criança, enfatize que é o comportamento que você gostaria que seu filho mudasse, e você tem a confiança de que ele pode fazer isso.

Se sua família está passando por um evento estressante que você acha que pode ter contribuído ao comportamento de seu filho, peça ajuda dos recursos da escola e de sua comunidade. Conselheiros de orientação, pastores, terapeutas e seu médico podem ajudar.

## Obtendo ajuda

Para ajudar uma criança a parar de intimidar, converse com professores, orientadores e outros funcionários da escola que podem ajudá-lo a identificar situações que levam ao bullying e fornecer assistência.

Seu médico também pode ajudá-lo. Se seu filho tem um histórico de brigas, desafio e dificuldade em controlar a raiva, considere uma avaliação com um terapeuta ou profissional de saúde comportamental.

Por mais difícil e frustrante que seja ajudar as crianças a pararem de intimidar, lembre-se esse mau comportamento não vai parar por conta própria. Pense no sucesso e na felicidade você quer que seus filhos encontrem na escola, no trabalho e nos relacionamentos ao longo da vida, e saiba que conter o bullying agora é um progresso em direção a esses objetivos.