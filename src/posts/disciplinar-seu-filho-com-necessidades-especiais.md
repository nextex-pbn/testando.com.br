---
title: Disciplinar seu filho com necessidades especiais
permalink: disciplinar-seu-filho-com-necessidades-especiais
layout: post.hbs
---

# Disciplinar seu filho com necessidades especiais

Desde o momento em que ouviu o diagnóstico, soube que a vida seria mais desafiadora para seu filho do que para a maioria. Então, quando você pede a ele para fazer algo e não é feito, você deixa ir. Ele realmente precisa que você aponte suas limitações? Ou talvez você medo de que o que você gostaria que ele fizesse ou deixasse de fazer seja impossível para ele realizar.

Mas aqui está a verdade: se você acha que seu filho ou filha não merece disciplina, é como dizer a seu filho: "Não acredito que você possa aprender". E se você não acredita isso, como vai seu filho?

O que os especialistas chamam de "gerenciamento de comportamento" não tem a ver com punir ou desmoralizar seu filho. Em vez disso, é uma maneira de definir limites e comunicar expectativas em uma maneira carinhosa e amorosa. Disciplina - corrigindo as ações das crianças, mostrando-lhes o que é certo e errado, o que é aceitável e o que não é - é um dos mais maneiras importantes de todos os pais mostrarem aos filhos que amam e se preocupam com eles.

Aqui estão algumas estratégias para ajudar os pais a disciplinar uma criança com necessidades especiais.

## Seja consistente

Os benefícios da disciplina são os mesmos, independentemente de as crianças terem necessidades especiais ou não. Na verdade, as crianças que têm problemas para aprender respondem muito bem à disciplina e à estrutura. Mas para que isso funcione, os pais precisam fazer da disciplina uma prioridade e ser consistentes.

Corrigir crianças é estabelecer padrões - seja definir uma rotina matinal ou boas maneiras na hora do jantar - e depois ensiná-los a se encontrar essas expectativas. Todas as crianças, independentemente de suas necessidades e habilidades, anseiam por isso consistência. Quando eles podem prever o que vai acontecer em seu dia, eles se sentem confiantes e seguro.

Sim, eles vão testar esses limites - todas as crianças fazem. Mas depende de você afirme que esses padrões são importantes e deixe seu filho saber que você acredita ele ou ela pode conhecê-los.

## Saiba mais sobre a condição do seu filho

Para compreender o comportamento do seu filho, você deve compreender as coisas que afetam isso - incluindo sua condição. Então, não importa o desafio que seu filho rostos, tente aprender o máximo sobre os aspectos médicos, comportamentais e psicológicos exclusivos fatores que afetam seu desenvolvimento.

Leia sobre a condição e pergunte ao médico sobre qualquer coisa que você não entende. Converse também com os membros da equipe de cuidados do seu filho e outros pais (especialmente aqueles com filhos que têm problemas semelhantes) para ajudar a determinar se o comportamento desafiador de seu filho é típico ou relacionado ao seu indivíduo desafios. Por exemplo, outro pai pode se relacionar com o problema que você está tendo para seu filho de 5 anos se veste todas as manhãs? Compartilhar experiências lhe dará uma maneira de medir suas expectativas e aprenda quais comportamentos estão relacionados ao diagnóstico de seu filho e que são puramente de desenvolvimento. Você também pode pegar algumas dicas úteis sobre como lidar com o comportamento que você está observando.

Se você tiver dificuldade para encontrar pais de crianças com desafios semelhantes, considere ingressar um suporte online ou grupos de defesa para famílias de crianças com necessidades especiais. Uma vez você sabe qual é o comportamento típico para a idade e os problemas de saúde do seu filho, você pode definir expectativas comportamentais realistas.

## Definindo expectativas

Estabelecer regras e disciplina é um desafio para qualquer pai. Portanto, mantenha o seu plano de comportamento simples e trabalhar em um desafio de cada vez. E quando seu filho conhece uma meta comportamental, ele ou ela pode se esforçar para alcançar a próxima.

Aqui estão algumas dicas.

## Use recompensas e consequências

Trabalhe dentro de um sistema que inclui recompensas (reforço positivo) para o bom comportamento e as consequências naturais para o mau comportamento. Natural As consequências são punições diretamente relacionadas ao comportamento. Por exemplo, se seu filho está jogando comida, você tiraria o prato.

Mas nem toda criança reage às consequências naturais, então você pode ter que corresponder a conseqüência para os valores de seu filho. Por exemplo, uma criança com autismo quem gosta de ficar sozinho pode considerar gratificante um "tempo livre" tradicional - em vez disso, tire um brinquedo ou videogame favorito por um período de tempo.

Depois de corrigir seu filho por fazer algo errado, ofereça um substituto comportamento. Então, se seu filho está falando muito alto ou batendo em você para obter seu atenção, trabalhe para substituir isso por um comportamento apropriado, como dizer ou sinalizar "ajude-me" ou chame sua atenção de maneiras apropriadas, como tocando seu ombro. Ignorar ativamente é uma boa consequência para o mau comportamento destinado a obter sua atenção. Isso significa não recompensar o mau comportamento com sua atenção (mesmo que é atenção negativa, como ralhar ou gritar).

## Use mensagens claras e simples

Comunique as suas expectativas ao seu filho de uma forma simples. Para crianças com especial necessidades, isso pode exigir mais do que apenas dizer a eles. Você pode precisar usar fotos, dramatização ou gestos para garantir que seu filho saiba no que está trabalhando.

Mantenha a linguagem verbal e visual simples, clara e consistente. Explique tão simplesmente quanto possível, quais comportamentos você deseja ver. Consistência é a chave, então certifique-se de que avós, babás, irmãos e professores estão todos a bordo com suas mensagens.

## Ofereça elogios

Incentive as realizações, lembrando a seu filho o que ele pode ganhar para cumprir as metas que você definiu, seja obter adesivos, tempo de tela ou ouvir a uma música favorita. E certifique-se de elogiar e recompensar seu filho pelo esforço também como sucesso. Portanto, uma criança que se recusa a fazer cocô no banheiro pode ser recompensada por usar um penico perto do banheiro.

Outra estratégia: pratique o "intervalo" - quando você pegar seu filho fazendo algo certo, elogie-o por isso. Em certos casos, o time-in pode ser mais eficaz do que punição, porque as crianças naturalmente querem agradar aos pais. Obtendo crédito para fazer algo certo, eles provavelmente vão querer fazer de novo.

p2

## Estabeleça uma rotina

Crianças com certas condições, como autismo e TDAH (transtorno de déficit de atenção e hiperatividade), responde particularmente bem à disciplina isso se baseia em saber exatamente o que vai acontecer a seguir. Portanto, tente manter o mesmo rotina todos os dias. Por exemplo: se seu filho tende a derreter à tarde depois da escola, estabeleça um horário para o tempo livre. Talvez ele ou ela precise fazer um lanche primeiro e depois faça o dever de casa antes do recreio.

Os gráficos podem ser úteis. Se seu filho for não-verbal ou pré-verbal, faça desenhos ou use adesivos para indicar o que vem a seguir. Defina um cronograma realista e encorajador opinião de seu filho, quando apropriado.

## Acredite no seu filho

Se, depois de dar os primeiros passos, o seu filho continuasse caindo, você deu a ele algumas muletas ou uma cadeira de rodas? Não. Portanto, não faça o mesmo com uma criança com necessidades especiais. Talvez seu filho não consiga calçar os sapatos na primeira vez, ou na décima tempo, mas continua tentando. Incentive isso!

Quando você acredita que seu filho pode fazer algo, você o capacita a alcançar isso objetivo. O mesmo se aplica ao comportamento. Por exemplo, se seu filho for muito agressivo quando brincar com outras crianças, não interrompa a brincadeira completamente. Em vez disso, trabalhe com o seu criança para limitar a fisicalidade da brincadeira. Você pode querer planejar para não físicos atividades durante datas de jogo, como projetos de arte e artesanato. Use a disciplina onde necessário na forma de tempos limite, execução forçada de turnos e regras como "não tocar" - e oferecer recompensas quando seus desejos forem atendidos.

Faça o que fizer, não desista de seu filho quando as coisas ficarem difíceis. Mau comportamento que é ignorado nos primeiros anos pode se tornar insuportável, até mesmo perigoso, no adolescente anos e idade adulta. Seja paciente e dedique um tempo para trabalhar com seu filho para ajudar atingir seu melhor potencial. Seu voto de confiança às vezes é todo seu filho precisa ter sucesso.

## Tenha confiança em suas habilidades

A disciplina é uma tarefa exaustiva. Haverá bons dias em que você ficará surpreso pelo progresso do seu filho, dias ruins em que parece que todo o seu trabalho duro foi esquecido, e planaltos onde parece que mais progresso é impossível. Mas lembre-se disso: A gestão do comportamento é um desafio para todos os pais, mesmo para os filhos que são tipicamente em desenvolvimento. Portanto, não desista!

Se você definir uma expectativa de acordo com as habilidades de seu filho e acreditar ele ou ela pode realizá-lo, é provável que isso aconteça. Se seus esforços não resultarem nas mudanças, converse com o médico, terapeuta ou especialista em comportamento do seu filho para ajude a alcançar seus objetivos. Ele ou ela pode trabalhar com você para desenvolver um plano de comportamento que seja adaptado às necessidades especiais do seu filho.