---
title: Preparando as crianças para os furacões
permalink: preparando-as-criancas-para-os-furacoes
layout: post.hbs
---

# Preparando as crianças para os furacões

Os furacões podem ser assustadores, tanto para adultos como para crianças. As crianças aprendem como responda a situações com base nos comportamentos e atitudes das pessoas ao seu redor.

Aqui estão algumas dicas para ajudá-los - e você - a estarem prontos durante o furacão temporada.

## Fale sobre furacões

As crianças podem ficar confusas sobre o que é um furacão, então use um método simples apropriado para a idade descrições do que esperar se alguém estiver vindo em sua direção. Para uma criança mais nova, você pode dizer: "Um furacão é uma tempestade tropical com ventos muito fortes e muita chuva, relâmpago e trovão. "

Também é importante dizer às crianças que os adultos farão o possível para mantê-las seguro.

## Tente permanecer calmo

As crianças podem sentir facilmente as emoções das pessoas ao seu redor. Quando um pai parece excessivamente chateado ou preocupado, isso pode piorar os próprios medos ou preocupações da criança.

## Deixe as crianças ajudarem nos preparativos antes da tempestade

Mantê-los ocupados pode ajudar a manter as crianças longe de suas preocupações. Ajudando a preparar de maneiras adequadas à idade também pode aumentar o senso de controle da criança sobre a situação.

Para envolver seus filhos:

* Prepare um kit de emergência familiar para desastres. As crianças podem ajudar a coletar produtos enlatados e prepare as lanternas.
* Peça para seus filhos ajudarem a trazer itens de exterior para dentro.
* Discuta o plano de desastre de sua família juntos. Você precisará evacuar - e como seria isso? Quais adultos farão o quê? Isso ajudará as crianças a saber o que esperar.

## Durante a tempestade

* Deixe as crianças escolherem alguns itens de conforto, jogos não eletrônicos e brinquedos em caso de energia interrupções.
* Tente manter uma rotina o mais normal possível. Isso pode ajudar as crianças a se sentirem calmas e seguro.
* Incentive as crianças a falar sobre seus sentimentos ou pensamentos sobre o que está acontecendo. Algumas crianças podem preferir não falar imediatamente - e isso também está OK. Perder tempo juntos e diga a eles que você estará lá quando eles estiverem prontos.

## Depois da tempestade

* Monitore a exposição na mídia. Lá pode ser "cobertura demais" antes e especialmente após a chegada de um furacão. Estes imagens podem ser demais para olhos jovens e corações sensíveis.
* Deixe as crianças ajudarem na limpeza.
* Preste atenção aos sinais de estresse, incluindo pesadelos, comportamento regressivo / agindo mais jovem do que sua idade, e aderência extra. Estes são comuns em crianças que passaram por um evento traumático. Se você vir algum desses sinais, converse com seu médico e saiba que conselheiros treinados pode ajudar.