---
title: Transtorno de estresse pós-traumático (PTSD)
permalink: transtorno-de-estresse-pos-traumatico-ptsd
layout: post.hbs
---

# Transtorno de estresse pós-traumático (PTSD)

## O que é transtorno de estresse pós-traumático (PTSD)?

Alguém que é vítima de (ou ameaçado por) violência, lesão ou dano pode desenvolver-se um problema de saúde mental denominado transtorno de estresse pós-traumático (PTSD). O PTSD pode acontecer nas primeiras semanas após um evento ou mesmo anos depois.

Pessoas com PTSD costumam reviver seu trauma na forma de "flashbacks", memórias, pesadelos ou pensamentos assustadores, especialmente quando são expostos a eventos ou objetos que os lembrem do trauma.

Psicólogos, terapeutas ou psiquiatras podem ajudar pessoas com PTSD a lidar com situações dolorosas pensamentos e sentimentos ruins e volte a uma vida normal.

## O que causa PTSD?

PTSD é frequentemente associado a soldados e outras pessoas na linha de frente da guerra. Mas qualquer pessoa - até crianças - pode desenvolvê-lo após um evento traumático.

Traumas que podem causar PTSD incluem morte inesperada ou violenta de um membro da família ou amigo próximo e ferimentos graves ou ameaça de morte ou ferimentos para si mesmo ou para um ente querido.

As situações que podem causar esse trauma incluem:

* ataques violentos, como estupro
* fogo
* abuso físico ou sexual
* atos de violência (como tiroteios na escola ou na vizinhança)
* desastres naturais ou provocados pelo homem
* acidentes de carro
* combate militar (às vezes chamado de "choque de granada")
* testemunhar outra pessoa passar por esses tipos de eventos traumáticos
* sendo diagnosticado com uma doença potencialmente fatal

Em alguns casos, PTSD pode ocorrer após exposição repetida a esses eventos. Sobrevivente culpa (sentimentos de culpa por ter sobrevivido a um evento em que amigos ou familiares morreu) também pode contribuir para PTSD.

## Quais são os sinais e sintomas do PTSD?

Pessoas com PTSD apresentam sintomas de estresse, ansiedade e depressão que incluem muitos dos seguintes:

Pensamentos intrusivos ou memórias do evento

* memórias indesejadas do evento que sempre voltam
* sonhos ou pesadelos perturbadores
* agir ou sentir como se o evento estivesse acontecendo novamente (flashbacks)
* dor de cabeça e medo ao ser lembrado do evento
* sentir-se apreensivo, assustado ou nervoso quando algo desencadeia memórias do evento
* as crianças podem reencenar o que aconteceu em suas brincadeiras ou desenhos

Evitar qualquer lembrete do evento

* evitando pensar ou falar sobre o trauma
* evitando atividades, lugares ou pessoas que sejam lembretes do evento
* ser incapaz de lembrar partes importantes do que aconteceu

Pensamento ou humor negativo desde o evento aconteceu

* preocupações e crenças duradouras sobre as pessoas e o mundo serem inseguros
* culpar a si mesmo pelo evento traumático
* falta de interesse em participar de atividades regulares
* sentimentos de raiva, vergonha, medo ou culpa pelo que aconteceu
* sentir-se separado ou distante das pessoas
* incapaz de ter emoções positivas (felicidade, satisfação, sentimentos amorosos)

Sentimentos duradouros de ansiedade ou reações físicas

* problemas para adormecer ou manter o sono
* sentindo-se mal-humorado, rabugento ou com raiva
* problemas para prestar atenção ou foco
* estando sempre atento a sinais de perigo ou alerta
* se assusta facilmente

Os sinais de PTSD em adolescentes são semelhantes aos dos adultos. Mas PTSD em crianças pode parece um pouco diferente. Crianças mais novas podem mostrar comportamentos mais medrosos e regressivos. Eles podem reconstituir o trauma por meio de brincadeiras.

Os sintomas geralmente começam no primeiro mês após o trauma, mas podem não aparecer até meses ou mesmo anos. Esses sintomas geralmente continuam por anos após o trauma. Em alguns casos, eles podem aliviar e retornar mais tarde na vida se outro evento desencadeia memórias do trauma. (Na verdade, os aniversários do evento muitas vezes podem causar uma enxurrada de emoções e memórias ruins.)

O PTSD também pode surgir como uma resposta repentina de curto prazo (chamada de estresse agudo desordem) a um evento e pode durar muitos dias ou até um mês.

Pessoas com PTSD podem não obter ajuda profissional porque acham que é compreensível sentir medo depois de passar por um evento traumático. Às vezes, as pessoas podem não reconhecer a ligação entre os sintomas e o trauma.

Professores, médicos, conselheiros escolares, amigos e outros membros da família que conhecem bem uma criança ou adolescente podem desempenhar um papel importante papel no reconhecimento de sintomas de PTSD.

## Quem tem PTSD?

Nem todo mundo que passa por um evento traumático tem PTSD. As chances de desenvolver e quão grave é, variam com base em coisas como personalidade, histórico de saúde mental questões, suporte social, história familiar, experiências de infância, níveis atuais de estresse, e a natureza do evento traumático.

Crianças e adolescentes que passam pelos traumas mais graves tendem a ter os mais níveis de sintomas de PTSD. Quanto mais frequente o trauma, maior a taxa de PTSD.

Estudos mostram que pessoas com PTSD costumam ter níveis atípicos de chave hormônios envolvidos na resposta ao estresse. Por exemplo, a pesquisa mostrou que eles têm abaixo do normal cortisol níveis e níveis de epinefrina e norepinefrina acima do normal - todos os quais desempenham um grande papel na reação de "luta ou fuga" do corpo para estresse repentino. (É conhecido como "lutar ou fugir" porque é exatamente isso que o corpo está se preparando para fazer - para lutar contra o perigo ou fugir dele.)

## Como o PTSD é tratado?

Muitas pessoas se recuperam de um evento traumático após um período de adaptação. Mas se seu filho ou adolescente passou por um evento traumático e tem sintomas de PTSD para mais de um mês, obtenha ajuda de um especialista.

A terapia pode ajudar a tratar os sintomas de evitação, pensamentos intrusivos e negativos e um humor deprimido ou negativo. UMA terapeuta trabalhará com sua família para ajudar você e seu filho ou adolescente a se ajustarem para o que aconteceu e volte a viver a vida.

Profissionais de saúde mental que podem ajudar incluem:

* psicólogos
* psiquiatras
* assistentes sociais clínicos licenciados
* conselheiros profissionais licenciados
* profissionais de trauma licenciados
* especialistas em luto

A terapia cognitivo-comportamental é muito eficaz para pessoas que desenvolver PTSD. Este tipo de terapia ensina maneiras de substituir pensamentos negativos e inúteis e sentimentos com pensamento mais positivo. Estratégias comportamentais podem ser usadas em uma criança próprio ritmo para ajudar a dessensibilizar a criança para as partes traumáticas do que aconteceu ele ou ela não tem tanto medo deles.

Combinações de dessensibilização do movimento ocular e terapia de reprocessamento (EMDR) terapia cognitiva com movimentos oculares direcionados. Isso tem se mostrado eficaz no tratamento de pessoas de todas as idades com PTSD.

A ludoterapia é usada para tratar crianças pequenas com PTSD que não conseguem lidar diretamente com o trauma.

Em alguns casos, os medicamentos podem ajudar a tratar sintomas graves de depressão e ansiedade. Isso pode ajudar as pessoas com PTSD a lidar com a escola e outras atividades diárias enquanto estão tratado. A medicina geralmente é usada apenas até que alguém se sinta melhor, então a terapia pode ajude a colocar a pessoa de volta nos trilhos.

Finalmente, terapia de grupo ou grupos de apoio são úteis porque permitem que as crianças e os adolescentes sabem que não estão sozinhos. Os grupos também fornecem um lugar seguro para compartilhar sentimentos. Peça ao terapeuta do seu filho referências ou sugestões.

## Como posso Eu ajudo meu filho?

Acima de tudo, seu filho precisa do seu apoio e compreensão. Às vezes outra família membros como pais e irmãos também precisarão de apoio. Enquanto família e amigos pode desempenhar um papel fundamental em ajudar alguém a se recuperar, a ajuda geralmente é necessária de um terapeuta.

Aqui estão algumas outras coisas que os pais podem fazer para apoiar as crianças com PTSD:

* A maioria das crianças precisará de um período de adaptação após um evento estressante. Durante este tempo, é importante que os pais ofereçam apoio, amor e compreensão.
* Tente manter os horários e a vida das crianças o mais semelhantes possível antes do evento. Isso significa não permitir que seu filho tire muito tempo da escola ou das atividades, mesmo que seja difícil no início.
* Deixe-os falar sobre o evento traumático quando e se eles se sentirem prontos. Elogie-os por serem fortes quando falam sobre isso, mas não force o assunto se eles não falarem sentir vontade de compartilhar seus pensamentos. Algumas crianças podem preferir desenhar ou escrever sobre eles experiências. De qualquer forma, o incentivo e o elogio podem ajudá-los a expressar seus sentimentos.
* Tranquilize-os de que seus sentimentos são típicos e que eles não "vão louco. "O apoio e a compreensão dos pais podem ajudar a lidar com dificuldades sentimentos.
* Algumas crianças e adolescentes acham útil se envolver em um grupo de apoio para traumas sobreviventes. Procure online ou consulte o seu pediatra ou o conselheiro escolar para encontrar grupos próximos.
* Obtenha ajuda profissional imediatamente se tiver alguma preocupação de que uma criança tenha pensamentos de automutilação. Pensamentos de suicídio são sérios em qualquer idade e deve ser tratado imediatamente.
* Ajude a aumentar a autoconfiança, incentivando as crianças a tomar decisões diárias onde apropriado. PTSD pode fazer as crianças se sentirem impotentes, então os pais podem ajudar mostrando-lhes crianças que têm controle sobre algumas partes de suas vidas. Dependendo de uma criança idade, os pais podem considerar deixá-lo escolher uma atividade de fim de semana ou decidir coisas como o que tem para o jantar ou o que vestir.
* Diga a eles que o evento traumático não é culpa deles. Incentive as crianças a falar sobre quaisquer sentimentos de culpa, mas não deixe que se culpem pelo que aconteceu.
* Fique em contato com os cuidadores. É importante conversar com professores, babás, e outras pessoas que estão envolvidas na vida do seu filho.
* Não critique o comportamento regressivo (retorno a um nível anterior de desenvolvimento). Se as crianças quiserem dormir com as luzes acesas ou levar um bichinho de pelúcia favorito para cama, pode ajudá-los a superar esse momento difícil. Fale com o médico do seu filho ou terapeuta se você não tiver certeza sobre o que é útil para seu filho ou filha.

## Olhando para a Frente

Certifique-se de também se cuidar. Ajudando seu filho ou adolescente a lidar com PTSD pode ser muito desafiador e exigir muita paciência e apoio. O tempo cura, e obter um bom suporte para sua família pode ajudar todos a seguir em frente.