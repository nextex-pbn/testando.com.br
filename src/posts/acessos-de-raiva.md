---
title: Acessos de raiva
permalink: acessos-de-raiva
layout: post.hbs
---

# Acessos de raiva

Acessos de raiva podem ser frustrantes para qualquer pai. Mas em vez de olhar para eles como desastres, trate os acessos de raiva como oportunidades de educação.

## Por que as crianças têm acessos de raiva?

Os acessos de raiva variam de choramingar e chorar para gritar, chutar, bater e prender a respiração. Eles são igualmente comuns em meninos e meninas e geralmente acontecem entre 1 e 3 anos.

Algumas crianças podem ter acessos de raiva com frequência e outras raramente. Birras são um parte normal do desenvolvimento infantil. Eles são como as crianças mostram que estão chateadas ou frustrado.

As birras podem acontecer quando as crianças estão cansadas, com fome ou desconfortáveis. Eles podem ter um colapso porque eles não conseguem fazer com que algo (como um brinquedo ou um dos pais) faça o que eles quer. Aprender a lidar com a frustração é uma habilidade que as crianças adquirem com o tempo.

As birras são comuns durante o segundo ano de vida, quando as habilidades com o idioma estão começando desenvolver. Porque as crianças ainda não conseguem dizer o que querem, sentem ou precisam, um frustrante a experiência pode causar um acesso de raiva. Conforme as habilidades de linguagem melhoram, os acessos de raiva tendem a diminuir.

As crianças querem independência e controle sobre seu ambiente - mais do que eles realmente podem controlar. Isso pode levar a lutas de poder quando uma criança pensa "eu posso faça eu mesmo "ou" Eu quero, dê para mim. "Quando as crianças descobrem que não podem fazer e não podem ter tudo o que querem, podem ter um acesso de raiva.

## Como podemos evitar acessos de raiva?

Tente evitar a ocorrência de acessos de raiva em primeiro lugar, sempre que possível. Aqui estão algumas idéias que podem ajudar:

* Dê muita atenção positiva. Adquira o hábito de pegar seu filho sendo bom. Recompense seu filho com elogios e atenção pelo positivo comportamento.
* Tente dar às crianças algum controle sobre as pequenas coisas. Oferta menor escolhas como "Você quer suco de laranja ou de maçã?" ou "Você quer escovar seus dentes antes ou depois de tomar banho? "Dessa forma, você não está perguntando" Você quer escovar os dentes agora? "- que inevitavelmente terá uma resposta" não ".
* Mantenha os objetos fora dos limites da vista e do alcance. Isto faz lutas menos prováveis. Obviamente, isso nem sempre é possível, especialmente fora de a casa onde o ambiente não pode ser controlado.
* Distraia seu filho. Aproveite as vantagens do seu pequeno atenção, oferecendo algo mais no lugar do que eles não podem ter. Começar um nova atividade para substituir a frustrante ou proibida. Ou simplesmente mude o ambiente. Leve seu filho para fora ou para dentro ou mude para um cômodo diferente.
* Ajude as crianças a aprender novas habilidades e ter sucesso. Ajude as crianças a aprender a fazer coisas. Elogie-os para ajudá-los a se sentirem orgulhosos do que podem fazer. Além disso, comece com algo simples antes de passar para tarefas mais desafiadoras.
* Considere a solicitação com cuidado quando seu filho quiser algo. Isso é ultrajante? Talvez não seja. Escolha suas batalhas.
* Conheça os limites de seu filho. Se você sabe que seu filho está cansado, não é a melhor hora para fazer compras ou tentar arranjar mais uma tarefa.

## O que devo fazer durante uma birra?

Mantenha a calma ao responder a uma birra. Não complique o problema com sua própria frustração ou raiva. Lembre-se de que seu trabalho está ajudando seu filho aprenda a se acalmar. Então você precisa ser calma também.

As birras devem ser tratadas de forma diferente dependendo do motivo pelo qual seu filho está chateado. As vezes, você pode precisar fornecer conforto. Se seu filho está cansado ou com fome, é hora de um cochilo ou um lanche. Outras vezes, é melhor ignorar uma explosão ou distrair seu filho com uma nova atividade.

Se uma birra está acontecendo para chamar a atenção dos pais, uma das melhores maneiras de reduzir esse comportamento é ignorá-lo. Se uma birra acontecer depois que seu filho for recusado algo, fique calmo e não dê muitas explicações de porque seu filho não pode tem o que ele quer. Passe para outra atividade com seu filho.

Se tiver um ataque de raiva depois que seu filho ouvir algo que ele não quer para fazer, é melhor ignorar a birra. Mas certifique-se de seguir em frente em ter seu filho conclui a tarefa depois que está calmo.

Crianças que correm o risco de machucar a si mesmas ou outras pessoas durante uma birra devem ser levado para um local tranquilo e seguro para se acalmar. Isso também se aplica a acessos de raiva em público lugares.

Se houver um problema de segurança envolvido e uma criança repetir o comportamento proibido depois ser instruído a parar, dar um tempo ou segurar a criança firmemente por vários minutos. Estar consistente. Não ceda em questões de segurança.

Crianças em idade pré-escolar e mais velhas são mais propensas a ter acessos de raiva para conseguir o que querem se eles aprenderam que esse comportamento funciona. Para crianças em idade escolar, é apropriado mande-os para os quartos para se refrescarem, prestando pouca atenção ao comportamento.

Em vez de definir um limite de tempo específico, diga a seu filho para ficar na sala até que ele ou ela recupere o controle. Isso é fortalecedor - as crianças podem afetar o resultado por suas próprias ações, e assim ganhar um senso de controle que foi perdido durante a birra. Mas se o castigo for para uma birra mais comportamento negativo (como como bater), defina um limite de tempo.

Não recompense a birra de seu filho cedendo. Isso vai apenas prove ao seu filho que a birra foi eficaz.

## O que devo fazer depois de uma birra?

Elogie seu filho por recuperar o controle; por exemplo, "Gosto de como você se acalmou".

As crianças podem ficar especialmente vulneráveis ​​depois de um acesso de raiva, quando sabem que foram menos do que adorável. Agora (quando seu filho está calmo) é a hora de um abraço e de uma garantia que seu filho é amado, não importa o quê.

Verifique se seu filho está dormindo o suficiente. Com muito pouco sono, as crianças podem se tornar hiper, desagradável e tem extremos de comportamento. Dormir o suficiente pode dramaticamente reduzir acessos de raiva. Descubra quanto sono é necessário para a idade de seu filho. A maioria as necessidades de sono das crianças estão dentro de um intervalo definido de horas com base na idade, mas cada criança tem suas próprias necessidades de sono.

## Quando devo chamar o médico?

Fale com o seu médico se:

* Você costuma ficar com raiva ou perder o controle quando responde a acessos de raiva.
* Você continua cedendo.
* As birras causam muitos sentimentos ruins entre você e seu filho.
* Você tem dúvidas sobre o que está fazendo ou o que seu filho está fazendo.
* Os acessos de raiva tornam-se mais frequentes, intensos ou duram mais tempo.
* Seu filho costuma se machucar ou aos outros.
* Seu filho parece muito desagradável, briga muito e quase nunca coopera.

O seu médico também pode verificar se há problemas de saúde que aumentem as birras, embora isso não seja comum. Às vezes, problemas de audição ou visão, uma doença crônica, atrasos na linguagem ou dificuldades de aprendizagem podem tornar as crianças mais propensas a ter acessos de raiva.

Lembre-se de que as birras geralmente não são motivo de preocupação e geralmente param em seus próprio. Conforme as crianças amadurecem, elas ganham autocontrole. Eles aprendem a cooperar, se comunicar, e lidar com a frustração. Menos frustração e mais controle significarão menos acessos de raiva - e pais mais felizes.