---
title: Ensine seu filho a ter autocontrole
permalink: ensine-seu-filho-a-ter-autocontrole
layout: post.hbs
---

# Ensine seu filho a ter autocontrole

Quando as crianças derretem no meio de uma loja lotada, em um jantar de feriado com família extensa, ou em casa, pode ser extremamente frustrante. Mas os pais podem ajudar as crianças aprendem o autocontrole e as ensinam a reagir sem apenas agir por impulso.

Ensinar autocontrole é uma das coisas mais importantes que os pais podem fazer para seus filhos, porque essas habilidades são algumas das mais importantes para o sucesso no futuro vida.

## Ajudando as crianças a aprenderem autocontrole

Com o aprendizado de autocontrole, as crianças podem tomar decisões apropriadas e responder a situações estressantes Situações que podem gerar resultados positivos.

Por exemplo, se você disser que só servirá sorvete depois do jantar, seu a criança pode chorar, implorar ou até gritar na esperança de que você ceda. Mas com autocontrole, seu filho pode entender que um acesso de raiva significa que você vai tirar o sorvete para sempre e que é mais sensato esperar com paciência.

Aqui estão algumas sugestões sobre como ajudar as crianças a aprender a controlar seu comportamento:

## Até 2 anos

Bebês e crianças ficam frustrados pela grande lacuna entre as coisas que desejam fazer e o que são capazes de fazer. Eles frequentemente respondem com acessos de raiva. Tente evitar explosões distraindo seu pequeno um com brinquedos ou outras atividades.

Para crianças que atingem a marca de 2 anos, tente um breve intervalo em uma área designada - como uma cadeira de cozinha ou escada inferior - para mostrar as consequências para explosões e ensina que é melhor passar algum tempo sozinho em vez de jogar um birra.

## De 3 a 5 anos

Você pode continuar a usar os tempos limite, mas em vez de definir um limite de tempo específico, termine os tempos limite quando seu filho se acalmar. Isso ajuda as crianças a melhorar seu senso de autocontrole. E é tão importante elogiar seu filho por não perder o controle ao frustrar ou situações difíceis, dizendo coisas como: "Gosto de como você ficou calmo" ou "Bom trabalho para manter a calma. "

## De 6 a 9 anos

Conforme as crianças entram na escola, elas conseguem entender melhor a ideia das consequências e que eles podem escolher um bom ou mau comportamento. Pode ajudar seu filho a imaginar um sinal de parada que deve ser obedecido e pense sobre uma situação antes de responder. Incentive seu filho saia de uma situação frustrante por alguns minutos para se acalmar em vez de ter uma explosão. Elogie as crianças quando elas se afastarem e se refrescarem - eles estarão mais propensos a usar essas habilidades no futuro.

## De 10 a 12 anos

Crianças mais velhas geralmente entendem melhor seus sentimentos. Incentive-os a pensar sobre o que está fazendo com que eles percam o controle e então o analisem. Explique que às vezes situações que são perturbadores no início não acabam sendo tão horríveis. Incentive as crianças a ter tempo para pense antes de responder a uma situação. Ajude-os a entender que não é o situação que os preocupa - é o que eles pensam sobre a situação que os deixa com raiva. Elogie-os conforme eles usam suas habilidades de autocontrole.

## De 13 a 17 anos

A esta altura, as crianças devem ser capazes de controlar a maioria de suas ações. Mas lembre os adolescentes de pense nas consequências a longo prazo. Incentive-os a fazer uma pausa para avaliar situações perturbadoras antes de responder e falar sobre os problemas, em vez de perder o controle, bater portas, ou gritando. Se necessário, discipline seu filho retirando certos privilégios de reforce a mensagem de que o autocontrole é uma habilidade importante. Permita que ele ou ela ganhe os privilégios de volta demonstrando autocontrole.

## Quando as crianças estão fora de controle

Por mais difícil que seja, resista à vontade de gritar quando estiver disciplinando seu crianças. Em vez disso, seja firme e objetivo. Durante o colapso de uma criança, fique calmo e explique que gritar, fazer birra e bater portas são comportamentos inaceitáveis que têm consequências - e diga quais são essas consequências.

Suas ações mostrarão que as birras não farão com que as crianças levem vantagem. Por exemplo, se seu filho fica chateado no supermercado depois que você explica por que você não compre doces, não desista - demonstrando assim que a birra era inaceitável e ineficaz.

Além disso, considere falar com os professores do seu filho sobre as configurações da sala de aula e expectativas de comportamento. Pergunte se a resolução de problemas é ensinada ou demonstrada na escola.

E demonstre bom autocontrole. Se você está em uma situação irritante na frente de seus filhos, diga a eles por que você está frustrado e discuta as possíveis soluções para o problema. Por exemplo, se você perdeu suas chaves, em vez de ficar chateado, diga a seus filhos que as chaves estão faltando e depois procurem por elas juntos. Se eles não aparecer, dar o próximo passo construtivo (como refazer seus passos da última tinha as chaves em mãos). Mostre que um bom controle emocional e resolução de problemas são os maneiras de lidar com uma situação difícil.

Se continuar a ter dificuldades, pergunte ao seu médico se há sessões de aconselhamento familiar pode ajudar.