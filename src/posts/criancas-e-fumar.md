---
title: Crianças e Fumar
permalink: criancas-e-fumar
layout: post.hbs
---

# Crianças e Fumar

## Quais são os riscos do tabagismo?

Os riscos à saúde de fumar, vaporizar e mascar tabaco são graves. Entre eles:

* Os cigarros de tabaco são a principal causa de mortes evitáveis ​​nos Estados Unidos, e pode causar muitas doenças.
* E-cigarros, canetas vaporizadoras e narguilés (cachimbos de água) estão cheios de tabaco, nicotina e outros produtos químicos prejudiciais. Saúde especialistas relatam sérios danos aos pulmões em pessoas que vomitam, incluindo algumas mortes.
* Mascar tabaco (sem fumaça ou cuspido) pode levar ao vício da nicotina, câncer, doenças da gengiva e doenças cardíacas.

Mas crianças e adolescentes continuam fumando e usando produtos de tabaco. Muitas pessoas jovens adquira esses hábitos todos os anos. Na verdade, 90% dos fumantes adultos começaram quando eram crianças. Portanto, é importante que os pais aprendam tudo o que puderem para ajudar os filhos a se manterem distantes.

## Quais os problemas que o fumo e o tabaco podem causar?

Uma das razões pelas quais fumar e mascar tabaco são importantes riscos para a saúde é porque eles contêm a nicotina química. Muitos cigarros eletrônicos também contêm nicotina. Alguém pode ficar viciado em nicotina dentro de dias do primeiro uso. Na verdade, a nicotina do tabaco pode ser tão viciante quanto a cocaína ou heroína. Quando uma pessoa começa a fumar, é difícil parar.

O fumo e o uso do tabaco podem prejudicar todos os sistemas do corpo e levar à saúde a longo prazo problemas como:

* doenças cardíacas
* doença pulmonar
* traço
* muitos tipos de câncer - incluindo câncer de pulmão, garganta, estômago e bexiga

Pessoas que fumam também podem obter:

* mais infecções (como pneumonia)
* úlceras
* doenças gengivais
* doenças oculares

Fumar está relacionado a diabetes, problemas nas articulações (artrite) e problemas de pele (como psoríase). Para as mulheres, fumar pode fazer isso mais difícil de engravidar e pode afetar a saúde de um bebê saúde. Também pode enfraquecer os ossos das mulheres e torná-los mais fáceis de quebrar.

O tabaco e outros produtos químicos também podem afetar o corpo rapidamente. Seus efeitos sobre o coração e pulmões tornar mais difícil ter um bom desempenho nos esportes. Eles também irritam a garganta, causam mau hálito, e danificam as vias respiratórias, levando à conhecida "tosse do fumante".

Finalmente, muitos estudos mostram que os jovens fumantes têm mais probabilidade de experimentar a maconha, cocaína, heroína, ou outras drogas.

## Por que algumas crianças fumam?

As crianças podem sentir-se atraídas por fumar, vaporizar e mascar tabaco por vários motivos - ter uma aparência descolada, parecer mais velho, perder peso, parecer durão ou se sentir independente.

Mas os pais podem lutar contra esses empates e impedir as crianças de tentarem essas coisas - e ficar viciado neles. Fale abertamente sobre o uso de tabaco e vaporização com seus filhos desde o início para facilitar o trabalho com esses problemas complicados.

## Como os pais podem falar com os filhos?

Para ajudar a evitar que seus filhos fumem, vaporizem e masquem tabaco, mantenha estas dicas em mente:

* Fale sobre isso de uma forma que não faça as crianças temerem punição ou julgamento.
* Continue falando com as crianças ao longo dos anos sobre os perigos do uso do tabaco e da fumaça. Até a criança mais nova pode entender que esses hábitos são ruins para o corpo.
* Pergunte o que as crianças acham atraente - ou desagradável - sobre fumar. Estar um ouvinte paciente.
* Incentive as crianças a se envolverem em atividades que não permitem fumar, como esportes.
* Mostre que valoriza as opiniões e ideias dos seus filhos.
* Discuta maneiras de responder à pressão dos colegas para fumar. Seu filho pode se sentir confiante apenas dizendo "não". Mas ofereça outras respostas também, como "Isso fará com que minhas roupas e o hálito cheira mal "ou" Eu odeio a maneira como isso me faz parecer ".
* Concentre-se no que as crianças fazem de certo em vez de errado. A autoconfiança é o melhor para uma criança proteção contra a pressão dos colegas.
* Incentive as crianças a se afastarem de amigos que não respeitam seus motivos de não fumar.
* Explique o quanto o fumo afeta a vida diária das crianças que começam a fumar. Como eles compram os cigarros? Como eles têm dinheiro para pagar outras coisas eles querem? Como isso afeta suas amizades?
* Estabeleça regras firmes que excluam fumar, vaporizar e mascar tabaco de sua casa e explicar por quê: fumantes cheiram mal, parecem mal e se sentem mal e faz mal para todos saúde.

## E se meu filho fumar?

Mesmo quando as crianças estão bem cientes dos riscos para a saúde, algumas ainda tentam fumar. Se isso acontecer, tente não ficar com raiva. É melhor se concentrar na comunicação com seu filho.

Essas dicas podem ajudar:

* Descubra o que atrai seu filho sobre fumar e fale sobre isso com sinceridade.
* Muitas vezes, as crianças não conseguem avaliar como seus comportamentos atuais podem afetar seu futuro saúde. Portanto, fale sobre os problemas que acontecem mais cedo: menos dinheiro para gastar nas coisas eles gostam, falta de ar, mau hálito, dentes amarelos e roupas fedorentas.
* Siga as regras de fumar que você definiu e não deixe seu filho fumar em casa.
* Se você ouvir "Posso parar quando quiser", peça ao seu filho para mostrar a você parando peru frio por uma semana.
* Resista aos sermões e tente não importunar. No final das contas, parar é uma decisão do fumante.
* Quando seu filho estiver pronto, ajude a desenvolver uma desistência plano. Elogie sua decisão de parar.
* Concentre-se nas recompensas que vêm com o abandono: liberdade do vício, melhor condicionamento físico, melhor desempenho esportivo, melhor aparência.
* Incentive uma reunião com seu médico, que pode lhe dar suporte e fazer tratamento planos.

## Quando os pais fumam

As crianças são rápidas em identificar lacunas entre o que os pais dizem e o que eles fazem. E a maioria das crianças diz que o adulto com quem elas mais querem ser quando crescerem é um pai.

Se você é fumante:

* Em primeiro lugar, admita que cometeu um erro ao começar a fumar. Diga isso se você tivesse para fazer tudo de novo, você nunca começaria.
* Em segundo lugar, saia. Não é simples e pode exigir algumas tentativas e a ajuda extra de um programa ou grupo de apoio. Mas seus filhos serão encorajados quando virem você superar seu vício em tabaco. Você pode encontrar informações e suporte online em:

* fumar Quitline
* Sair Tabaco
* Smokefree.gov