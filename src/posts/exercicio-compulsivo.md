---
title: Exercício Compulsivo
permalink: exercicio-compulsivo
layout: post.hbs
---

# Exercício Compulsivo

## O que é exercício compulsivo?

O exercício compulsivo (às vezes chamado de dependência de exercício) acontece quando uma pessoa é levada a se exercitar demais. Lesões, doenças, sair com amigos, ou o mau tempo não impedirá aqueles que se exercitam compulsivamente.

## Por que as crianças se exercitam demais?

O exercício regular é uma parte importante do um estilo de vida saudável. Mas os atletas podem ser levados a se exercitar mais e mais para melhorar seu desempenho esportivo. Metas pessoais, treinadores, companheiros de equipe ou pais podem pressionar atletas irem longe demais.

Exercício compulsivo e transtornos alimentares muitas vezes andam de mãos dadas. Além de fazer dieta extrema, alguém com transtorno alimentar pode treinar excessivamente para perder peso. Alguém com bulimia pode usar exercícios como um maneira de compensar a compulsão alimentar.

Algumas pessoas acreditam que podem alcançar um corpo ideal impossível digite se eles continuarem se exercitando.

## Quais os problemas que o exercício compulsivo pode causar?

O exercício compulsivo pode levar a:

* Lesões, incluindo lesões por uso excessivo e estresse fraturas.
* Em algumas meninas, a tríade de atletas femininas Isso significa que elas perdem muito peso, tem períodos irregulares ou nenhum (chamado amenorréia), e perder densidade óssea (osteoporose).
* Comportamentos não saudáveis ​​para perder peso, como pular refeições ou reduzir drasticamente calorias, vômitos e uso de pílulas dietéticas ou laxantes.
* Isolamento social, porque malhar está sempre em primeiro lugar. Exercícios compulsivos pode pular o dever de casa ou o tempo com amigos e família para fazer exercícios.
* Ansiedade e depressão. Pressão de desempenho, baixa autoestima e falta de outros interesses para contribuir com o emocional problemas.

## O que os pais podem notar?

Os pais podem notar que seus filhos:

* não pulará um treino, mesmo se estiver cansado, doente ou ferido
* não pode tirar uma folga e parece ansioso ou culpado quando perde pelo menos um treino
* está preocupado com seu peso e rotina de exercícios
* perdeu uma quantidade significativa de peso
* faz mais exercícios depois de comer muito ou perder um treino
* come muito menos se não consegue fazer exercícios
* Deixa de ver os amigos, desiste de outras atividades e abandona responsabilidades para ter mais tempo para fazer exercícios
* parece basear a autoestima no número de treinos concluídos e no esforço despendido em treinamento
* nunca está satisfeito com suas próprias realizações físicas
* tem períodos irregulares ou fraturas por estresse

## Como o exercício compulsivo é diagnosticado?

Pode ser difícil diagnosticar exercícios compulsivos. Não há acordo sobre quanto exercício é demais. Uma pessoa que continua a se exercitar apesar das lesões, saúde problemas ou relacionamentos ruins podem ter um vício em exercícios.

## Como o exercício compulsivo é tratado?

Um terapeuta pode ajudar alguém com um vício em exercícios muda comportamentos não saudáveis, trabalha na moderação de exercícios e encontre estratégias de enfrentamento.

O tratamento se concentrará em:

* tratamento de ferimentos
* descanso ou redução do exercício
* planos alternativos de exercícios
* aconselhamento nutricional
* mantendo um peso saudável
* tratamento de doenças, como distúrbios alimentares, depressão ou obsessividade transtorno compulsivo (TOC)

## O que os pais podem fazer?

Os pais podem fazer muito para ajudar uma criança que se exercita demais. Eles podem:

* envolva as crianças na preparação de refeições nutritivas
* divirta-se sendo uma família ativa em família
* ser bons modelos de imagem corporal e não se fixar em suas próprias falhas físicas
* não critique o peso ou a forma corporal de outras pessoas
* pergunte se o filho está sob muita pressão
* ajude as crianças a encontrar novas maneiras de lidar com os problemas

Se você acha que seu filho está se exercitando muito, converse com seu médico.