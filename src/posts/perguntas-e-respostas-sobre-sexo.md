---
title: Perguntas e respostas sobre sexo
permalink: perguntas-e-respostas-sobre-sexo
layout: post.hbs
---

# Perguntas e respostas sobre sexo

Responder às perguntas dos filhos sobre sexo é uma responsabilidade de muitos pais pavor. As mães e pais confiantes costumam se sentir calados e desajeitados quando trata da puberdade e de onde vêm os bebês.

Mas o assunto não deve ser evitado. Os pais podem ajudar a promover sentimentos saudáveis sobre sexo, se eles responderem às perguntas das crianças de uma forma adequada à idade.

## Quando as crianças começam a ficar curiosas sobre seus corpos?

Desde a infância, as crianças estão interessadas em aprender sobre seus próprios corpos. Eles percebem as diferenças entre meninos e meninas e são naturalmente curiosos.

As crianças costumam tocar nos próprios órgãos genitais quando estão nuas, como no banheira ou enquanto estiver sendo fralda. Nesse estágio de desenvolvimento, eles não têm modéstia. Esses comportamentos são sinais de curiosidade normal, não de atividades sexuais, diz o americano Academy of Pediatrics (AAP), e não deve trazer repreensão ou punição.

Então, o que você deve fazer quando seu filho começar a se tocar? Cada a família abordará isso à sua maneira, com base em seus valores, nível de conforto, e estilo. Mas tenha em mente que sua reação à curiosidade de seu filho transmitirá se essas ações são "aceitáveis" ou "vergonhosas". Crianças que são repreendidas e feito para se sentir mal com sua curiosidade natural pode desenvolver um foco maior em seus partes íntimas ou sentir vergonha.

Alguns pais optam por ignorar casualmente o toque pessoal ou redirecionar a atenção de uma criança em direção a outra coisa. Outros podem querer reconhecer que, embora saibam que sente bom para explorar, é um assunto privado e não pode ser feito em público.

## Posso usar apelidos para partes privadas?

Quando a criança chega aos 3 anos, os pais podem escolher usar o modelo anatômico correto palavras. Eles podem parecer médicos, mas não há razão para que o rótulo adequado não deva ser usado quando a criança é capaz de dizê-lo. Estas palavras - pênis, vagina, etc. - deve ser afirmado com naturalidade, sem tolices implícitas. Dessa maneira, a criança aprende a usá-los de maneira direta, sem constrangimento.

Na verdade, é isso que a maioria dos pais faz. Uma pesquisa Gallup mostrou que 67% dos pais use nomes reais para se referir às partes do corpo masculino e feminino.

## O que você diria a uma criança muito pequena que pergunta de onde vêm os bebês?

Dependendo da idade da criança, você pode dizer que o bebê cresce a partir de um ovo na útero da mamãe, apontando para o seu estômago, e sai de um lugar especial, chamado de vagina. Não há necessidade de explicar o ato de fazer amor porque crianças muito pequenas não entenderá o conceito.

No entanto, você pode dizer que quando um homem e uma mulher se amam, eles gostam de estar perto um do outro. Diga a eles que o esperma do homem se junta ao óvulo da mulher e então o bebê começa a crescer. A maioria das crianças com menos de 6 anos aceita esta resposta. Livros apropriados para a idade sobre o assunto também são úteis. Responda a pergunta de forma direta maneira, e você provavelmente descobrirá que seu filho está satisfeito com apenas um pouco informações de cada vez.

## O que você deve fazer se pegar crianças brincando doutor "(mostrando as partes íntimas um ao outro)?

Crianças de 3 a 6 anos têm maior probabilidade de "brincar de médico". Muitos pais exageram quando testemunham ou ouvem falar de tal comportamento. Repreensão pesada não é a maneira de lidar com isso. Nem os pais devem sentir que isso é ou levará a uma promiscuidade comportamento. Muitas vezes, a presença de um dos pais é suficiente para interromper a brincadeira.

Você pode querer direcionar a atenção do seu filho para outra atividade sem fazer muito barulho. Mais tarde, sente-se com seu filho para uma conversa. Explique que embora você entender o interesse pelo corpo de seu amigo, geralmente se espera que as pessoas para manter seus corpos cobertos em público. Desta forma, você estabeleceu limites sem ter fez seu filho se sentir culpado.

Esta também é uma idade apropriada para começar a falar sobre toque bom e mau toque. Contar crianças que seus corpos são seus e que têm direito à privacidade. Ninguém, nem mesmo um amigo ou membro da família tem o direito de tocar nas áreas privadas de uma criança. No entanto, o AAP observa, uma exceção a esta regra é quando um pai está tentando encontrar a fonte de dor ou desconforto na área genital, ou quando um médico ou enfermeiro está realizando um exame físico.

As crianças devem saber que se alguém tocá-las de uma forma que pareça estranha ou ruim, eles deveriam dizer a essa pessoa para parar e então contar a você sobre isso. Explique isso você quer saber sobre qualquer coisa que faça seus filhos se sentirem mal ou desconfortáveis.

## Quando os pais devem sentar-se com os filhos para esses tão importantes "pássaros e abelhas "falam?

A "conversa fiada" é coisa do passado. Aprender sobre sexo não deve ocorrer em um sessão tudo ou nada. Deve ser mais um processo de desdobramento, em que as crianças aprender, com o tempo, o que eles precisam saber. As perguntas devem ser respondidas à medida que surgem para que a curiosidade natural das crianças seja satisfeita à medida que amadurecem.

Se o seu filho não faz perguntas sobre sexo, não ignore o assunto. Quando seu filho tem cerca de 5 anos, você pode começar a apresentar livros que abordem a sexualidade em um nível de desenvolvimento apropriado. Os pais costumam ter dificuldade em encontrar o certo palavras, mas muitos livros excelentes estão disponíveis para ajudar.

## Em que idade as meninas devem ser informadas sobre a menstruação?

Meninas (e meninos!) devem receber informações sobre a menstruação por volta dos 8 anos de idade. é uma área de intenso interesse para as meninas. Informações sobre os períodos podem ser fornecidas na escola - e os livros de instrução podem ser muito úteis.

Muitas mães compartilham suas experiências pessoais com as filhas, incluindo quando a menstruação começou e como foi, e como, como acontece com muitas coisas, não era grande coisa depois de um tempo.

## Com que idade a nudez em casa deve ser reduzida?

As famílias definem seus próprios padrões de nudez, recato e privacidade - e esses os padrões variam muito de família para família e em diferentes partes do mundo. Embora os valores de cada família sejam diferentes, a privacidade é um conceito importante para todas as crianças aprendam.

Os pais devem explicar os limites relativos à privacidade da mesma forma que as outras regras da casa são explicados - com naturalidade - para que as crianças não venham a associar privacidade com culpa ou sigilo. Geralmente, eles aprenderão com os limites que você estabelece para eles - e por seu próprio comportamento.

## Até que ponto os pais podem depender das escolas para ensinar educação sexual?

Os pais devem começar o processo de educação sexual muito antes de começar na escola. A introdução da educação sexual formal na sala de aula varia; muitas escolas comece na quinta ou sexta série - e alguns nem mesmo oferecem.

Os tópicos abordados nas aulas de educação sexual podem incluir anatomia, doenças sexualmente transmissíveis (DSTs) e gravidez. O que os professores cobrem e quando varia muito de escola para escola escola. Você pode querer fazer perguntas sobre o currículo da sua escola para que possa avaliar você mesmo.

As crianças, quando aprendem sobre questões sexuais na escola ou fora dela, são provavelmente terá muitas perguntas. O assunto certamente pode ser confuso. Pais deveriam esteja aberto para continuar o diálogo e responder a perguntas em casa. Isto é especialmente verdadeiro se você quiser que seus filhos entendam a sexualidade dentro do contexto de sua família valores.

Mudanças corporais e questões sexuais são uma parte importante do desenvolvimento humano. Se vocês tiver dúvidas sobre como falar com seu filho sobre eles, peça sugestões ao seu médico.