---
title: Pica
permalink: pica
layout: post.hbs
---

# Pica

## O que é Pica?

Pica é um distúrbio alimentar em que uma pessoa come coisas que normalmente não são consideradas Comida. Crianças pequenas costumam colocar itens não alimentares (como grama ou brinquedos) na boca porque eles estão curiosos sobre o mundo ao seu redor. Mas crianças com pica (PIE-kuh) vão além aquele. Às vezes, comem coisas que podem causar problemas de saúde.

## Quais são os sinais e sintomas da Pica?

Pessoas com paralisia cerebral anseiam e comem itens não alimentares, como:

* sujeira
* argila
* pedras
* papel
* gelo
* lápis de cor
* cabelo
* lascas de tinta
* giz
* fezes (cocô)

Problemas de saúde podem acontecer em crianças com paralisia cerebral, dependendo do que comem. Estes pode incluir:

* anemia por deficiência de ferro
* envenenamento por chumbo, por comer sujeira ou lascas de tinta com chumbo
* constipação ou diarreia, de comer coisas que o corpo não consegue digerir (como cabelo)
* infecções intestinais, por ingestão de sujeira ou cocô com parasitas ou vermes
* obstrução intestinal, por comer coisas que bloqueiam os intestinos
* Lesões na boca ou dentes

## O que causa a paralisia cerebral?

Os médicos não sabem exatamente o que causa a paralisia. Mas é mais comum em pessoas com:

* problemas de desenvolvimento, como autismo ou deficiência intelectual
* problemas de saúde mental, como obsessivo-compulsivo transtorno (TOC) ou esquizofrenia
* desnutrição ou fome. Itens não alimentares podem ajudar a dar uma sensação de saciedade. Baixos níveis de nutrientes como ferro ou zinco podem desencadear desejos específicos.
* estresse. Pica é frequentemente vista em crianças que vivem na pobreza, ou naqueles que foram abusados ou negligenciado.

A maioria dos casos de pica ocorre em crianças pequenas e mulheres grávidas. É normal para crianças até 2 anos para colocar coisas na boca. Portanto, o comportamento geralmente não é considerado um transtorno, a menos que a criança tenha mais de 2 anos.

Pica geralmente melhora à medida que as crianças crescem. Mas para pessoas com problemas de desenvolvimento ou mentais preocupações de saúde, ainda pode ser um problema mais tarde na vida.

## Como a Pica é diagnosticada?

Os médicos podem pensar que é irritante se uma criança comer produtos não alimentares e:

* faz isso há pelo menos 1 mês
* o comportamento não é normal para a idade ou estágio de desenvolvimento da criança
* a criança tem fatores de risco para pica, como deficiência de desenvolvimento

Os médicos também podem:

* verifique se há anemia ou outros problemas nutricionais
* testar os níveis de chumbo no sangue
* faça testes de fezes para verificar se há parasitas
* solicitar radiografias ou outros exames de imagem para descobrir o que a criança comeu ou para procurar problemas intestinais, como obstrução

## Como a pica é tratada?

Os médicos podem ajudar os pais a controlar e interromper os comportamentos relacionados à pica. Por exemplo, eles pode trabalhar com os pais sobre maneiras de evitar que as crianças recebam as coisas não alimentares que comer. Eles podem recomendar fechaduras de segurança para crianças e prateleiras altas para manter os itens fora de alcance.

Algumas crianças com paralisia cerebral precisam da ajuda de um psicólogo ou outro psicólogo profissional de saúde. Se esses tratamentos não funcionarem, os médicos também podem prescrever medicamentos.

## O que mais devo saber?

* Se seu filho está em risco de pica, ou você vê sinais que o preocupam, fale com seu doutor.
* Se seu filho pode ter comido algo prejudicial, procure atendimento médico imediatamente ou ligue para o controle de intoxicações em (800) 222-1222.