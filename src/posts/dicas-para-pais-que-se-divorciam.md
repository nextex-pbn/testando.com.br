---
title: Dicas para pais que se divorciam
permalink: dicas-para-pais-que-se-divorciam
layout: post.hbs
---

# Dicas para pais que se divorciam

Qual é a melhor maneira de ajudar sua família a superar o divórcio? Cada situação - e cada família - é diferente. Mas essas sugestões pode tornar o processo menos doloroso para crianças, adolescentes e famílias.

## Ajudando as crianças a administrar seus sentimentos### Incentive as crianças a compartilharem seus sentimentos - positivos ou negativos - sobre o que está acontecendo.

É importante para os pais divorciados - e já divorciados - sentar-se para baixo com seus filhos e incentivá-los a dizer o que estão pensando e sentindo. Mas mantenha isso separado de seus próprios sentimentos. Assegure a seus filhos que seus sentimentos são importantes, válidos e normais. Diga a eles que você pode lidar com uma conversa sobre até mesmo sentimentos difíceis ou dolorosos.

Durante essas conversas, evite resolver problemas e tente mudar a maneira uma criança sente. Em vez disso, concentre-se em ouvir e agradecer às crianças por sua honestidade. A maioria muitas vezes, as crianças sentem a perda da família e podem culpar você ou o outro pai - ou ambos - pelo que está acontecendo em suas vidas. Então, você precisa estar pronto para responder perguntas que seus filhos podem levantar ou abordar suas preocupações.

Transforme a conversa sobre o divórcio e como ele está afetando seus filhos em um processo contínuo. À medida que envelhecem e se tornam mais maduros, as crianças podem ter dúvidas ou preocupações que eles não tinham pensado antes. Mesmo que pareça que você abordou os mesmos tópicos antes, mantenha o diálogo aberto. Se possível, sente-se com o outro pai e planeje como você vai falar sobre o que está acontecendo.

Se achar que pode ficar muito chateado, peça a outra pessoa (um parente, talvez) para converse com seus filhos. É normal e saudável para as crianças ver seus pais tristes ou chateados, mas ficar muito emocionado pode fazer com que se sintam responsáveis ​​pelos sentimentos dos pais.

Se seus filhos virem que você luta contra uma emoção difícil, seja um exemplo de enfrentamento saudável tanto quanto possível. Tente:

* Identifique sua emoção para eles ("Estou me sentindo triste agora.">).
* Afirme que sabe que é normal sentir-se assim às vezes (é normal e normal para me sentir triste ").
* Fale sobre como você lidará com seus sentimentos difíceis ("Algo que sempre ajuda me sinto melhor quando estou triste é assar biscoitos com você ou brincar lá fora. Vamos lá faça isso! ").

É natural que as crianças tenham muitas emoções sobre o divórcio. Eles podem se sentir culpados e imagine que eles "causaram" o problema. Isso é especialmente verdadeiro se as crianças ouviram seus pais discutindo sobre eles. Crianças e adolescentes podem sentir raiva ou medo, ou preocupados com seu futuro. Se eles expressarem essas emoções, assegure-os de que isso não era o caso, enquanto os lembrava de que é um sentimento normal.

Embora as crianças possam lutar com o divórcio por um bom tempo, o verdadeiro impacto é geralmente parece por um período de 2 a 3 anos. Durante este tempo, alguns podem expressar seus sentimentos. Mas, dependendo de sua idade e desenvolvimento, outras crianças simplesmente não terão as palavras. Em vez disso, eles podem agir ou ficar deprimidos. Para crianças em idade escolar, isso pode significa que suas notas caem ou eles perdem o interesse nas atividades. Para crianças mais novas, esses sentimentos são freqüentemente expressos durante a brincadeira também. Esteja ciente de um "efeito dormente" com crianças pequenas: eles podem ter grandes mudanças no passo no início, mas perturbadoras comportamentos ou emoções desafiadoras podem surgir anos depois. Comunicando-se abertamente com crianças e modelagem de enfrentamento saudável, mesmo que pareçam estar bem com as grandes mudanças, podem reduzir problemas no futuro.

Pode ser tentador dizer a uma criança que não se sinta de determinada maneira, mas crianças (e adultos, quanto a isso) têm direito aos seus sentimentos. E se você tentar forçar um "feliz cara, "seus filhos podem ter menos probabilidade de compartilhar seus verdadeiros sentimentos com você.

Programas de grupo para crianças do divórcio, administrados por escolas ou organizações religiosas são um excelente recurso para crianças e famílias que precisam de ajuda para passar esses estágios iniciais.

## Pegando o caminho certo### Mantenha os conflitos e discussões adultos longe das crianças.

Esta é uma das coisas mais difíceis de fazer. Mas é importante nunca dizer coisas ruins sobre seu ex na frente de seus filhos, ou ao alcance da voz. As crianças aprendem essas coisas. A pesquisa mostra que o maior fator no ajuste de longo prazo para crianças de divórcio é o nível de conflito parental que eles vêem. Isso coloca as crianças em uma situação difícil se eles têm que tomar partido ou ouvir coisas negativas ditas sobre um de seus pais.

É tão importante reconhecer eventos reais. Se, por exemplo, um cônjuge muda-se ou abandona a família, reconhece o que aconteceu. Não é sua responsabilidade para explicar o comportamento do seu ex. Mas se seus filhos lhe fizerem perguntas, é importante para responder da forma mais neutra e verdadeira possível.

### Tente não usar crianças como mensageiros ou intermediários, especialmente quando estiver brigando.

Mesmo que seja tentador, não use seus filhos como mensageiros. Há muitos de outras maneiras de se comunicar com seu ex-parceiro. Além disso, resista a questionar seu filho sobre o que está acontecendo na outra casa. As crianças se ressentem quando sentem que eles estão sendo solicitados a "espionar" o outro pai. Sempre que possível, comunique-se diretamente com o outro pai sobre coisas como agendamento, visitação, problemas de saúde ou problemas escolares.

### Espere solavancos conforme as crianças se adaptam a um novo companheiro ou aos filhos do companheiro.

Novos relacionamentos, famílias unidas e novos casamentos estão entre as partes mais difíceis do processo de divórcio. Uma nova família mesclada pode adicionar mais estresse por um tempo, e levar a outro período de ajuste. Mantenha as linhas de comunicação abertas, permita um-a-um tempo para pais e filhos e observe os sinais de estresse para ajudar a prevenir problemas.

## Obtendo ajuda### Descubra como reduzir o estresse em sua vida para ajudar sua família.

Apoio de amigos, parentes, igrejas e grupos religiosos e organizações como os pais sem parceiros podem ajudar os pais e seus filhos a se ajustarem à separação e divórcio. As crianças podem conhecer outras pessoas que desenvolveram relacionamentos de sucesso com pessoas separadas pais e podem confiar um no outro. Obter apoio pode ajudar os pais a encontrar soluções a todos os tipos de desafios práticos e emocionais.

Sempre que possível, as crianças devem ser incentivadas a ter uma visão tão positiva sobre ambos os pais como eles podem. Mesmo nas melhores circunstâncias, separação e divórcio pode ser doloroso e decepcionante para as crianças.

Os pais também precisam se lembrar de cuidar de si mesmos. Reduza o estresse encontrando amigos que apoiam e pedindo ajuda quando você precisa. Tente manter alguma família antiga tradições enquanto constrói novas memórias para compartilhar. Mostrando a seus filhos como levar o bem cuidar da mente e do corpo durante os tempos difíceis pode ajudá-los a se tornarem mais resistentes em seus próprias vidas.

Lembre-se de que honestidade, sensibilidade, autocontrole e o próprio tempo ajudam o processo de cura.