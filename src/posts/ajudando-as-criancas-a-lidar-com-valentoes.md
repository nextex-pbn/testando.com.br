---
title: Ajudando as crianças a lidar com valentões
permalink: ajudando-as-criancas-a-lidar-com-valentoes
layout: post.hbs
---

# Ajudando as crianças a lidar com valentões

A cada dia, Seth, de 10 anos, pedia à mãe cada vez mais dinheiro para o lanche. Ainda ele parecia mais magro do que nunca e voltou da escola com fome. Descobriu-se que Seth estava entregando o dinheiro do almoço para um aluno da quinta série, que ameaçava espancá-lo se ele não pagou.

Kayla, 13, achou que as coisas estavam indo bem em sua nova escola, já que todos as meninas estavam sendo tão legais com ela. Mas então ela descobriu que um deles havia postado significa rumores sobre ela. Kayla chorou até dormir naquela noite e começou a ir a enfermaria reclamando de dor de estômago para evitar as meninas na sala de estudos.

Infelizmente, o tipo de bullying que Seth e Kayla experimentaram é generalizado. Em pesquisas nacionais, a maioria das crianças e adolescentes afirma que o bullying acontece na escola.

Um agressor pode transformar algo como ir ao ponto de ônibus ou recuar em um pesadelo para crianças. O bullying pode deixar cicatrizes emocionais profundas. E em situações extremas, pode envolvem ameaças violentas, danos à propriedade ou alguém se ferindo gravemente.

Se seu filho está sofrendo bullying, você deve agir para ajudar a impedir isso, se possível. No Além disso, existem maneiras de ajudar seu filho a lidar com provocações, bullying ou fofoca maldosa, e diminuir seu impacto duradouro. E mesmo que o bullying não seja um problema, certo em sua casa agora, é importante discutir isso para que seus filhos estejam preparados se isso acontecer.

## Identificação de bullying

A maioria das crianças já foi provocada por um irmão ou amigo em algum momento. E não é geralmente prejudicial quando feito de forma lúdica, amigável e mútua, e ambas as crianças acham é engraçado. Mas quando a provocação se torna dolorosa, rude e constante, ele cruza a linha para o bullying e precisa parar.

O bullying é um tormento intencional de formas físicas, verbais ou psicológicas. Pode variar de bater, empurrar, xingar, ameaçar e zombar até extorquir dinheiro e posses. Algumas crianças intimidam evitando outras pessoas e espalhando boatos sobre eles. Outros usam mídias sociais ou eletrônicos mensagens para insultar outras pessoas ou ferir seus sentimentos.

É importante levar o bullying a sério e não apenas descartá-lo como algo que as crianças têm que "resistir". Os efeitos podem ser graves e afetar o senso de segurança e auto-estima. Em casos graves, o bullying contribuiu para tragédias, como como suicídios e escola tiroteios.

## Por que as crianças intimidam

As crianças fazem bullying por vários motivos. Às vezes, eles importunam as crianças porque precisam uma vítima - alguém que parece emocionalmente ou fisicamente mais fraco, ou apenas age ou parece diferente de alguma forma - para se sentir mais importante, popular ou no controle. Embora alguns agressores sejam maiores ou mais fortes do que suas vítimas, isso nem sempre o caso.

Às vezes, as crianças atormentam os outros porque é assim que são tratadas. Eles podem pensar que seu comportamento é normal porque vêm de famílias ou outras configurações onde todos regularmente ficam com raiva e gritam ou ligam nomes uns dos outros. Alguns programas de TV populares até parecem promover maldade - pessoas são "eliminados", rejeitados ou ridicularizados por sua aparência ou falta de talento.

## Sinais de bullying

A menos que seu filho fale sobre bullying - ou tenha hematomas ou ferimentos visíveis - pode ser difícil descobrir se isso está acontecendo.

Mas existem alguns sinais de alerta. Os pais podem notar as crianças agindo de maneira diferente ou parecer ansioso, ou não comer, dormir bem ou fazer as coisas que normalmente desfrutar. Quando as crianças parecem mais mal-humoradas ou mais facilmente chateadas do que o normal, ou quando começam evitar certas situações (como pegar o ônibus para a escola), pode ser por causa de um valentão.

Se você suspeita de bullying, mas seu filho está relutante em se abrir, encontre oportunidades para abordar a questão de uma forma mais indireta. Por exemplo, você pode ver uma situação em um programa de TV e use-o para iniciar uma conversa perguntando: "O que você acha de isso? "ou" O que você acha que essa pessoa deveria ter feito? "Isso pode levar a perguntas como: "Você já viu isso acontecer?" ou "Você já experimentou isso?" Você pode querer falar sobre quaisquer experiências que você ou outro membro da família teve nessa idade.

Deixe seus filhos saberem que se eles estiverem sendo intimidados ou assediados - ou ver isso acontecendo com outra pessoa - é importante conversar com alguém sobre seja você, outro adulto (um professor, conselheiro escolar ou amigo da família), ou um irmão.

## Ajudando as crianças

Se seu filho lhe contar sobre ser intimidado, ouça com calma e ofereça conforto e Apoio, suporte. As crianças muitas vezes relutam em contar aos adultos sobre o bullying porque sentem envergonhado e envergonhado por isso estar acontecendo, ou preocupado que seus pais fiquem desapontados, chateado, com raiva ou reativo.

Às vezes, as crianças acham que é sua própria culpa, que se pareciam ou agiam de maneira diferente isso não estaria acontecendo. Às vezes eles têm medo de que se o agressor descobrir que disseram, vai piorar. Outros estão preocupados que seus pais não vão acreditar eles ou fazer qualquer coisa sobre isso. Ou os filhos temem que seus pais os exortem a lutar de volta quando eles estão com medo.

Elogie seu filho por fazer a coisa certa ao conversar com você sobre isso. Lembrar seu filho que ele ou ela não está sozinho - muitas pessoas sofrem bullying em alguns ponto. Enfatize que é o agressor que está se comportando mal - não seu filho. Tranquilize seu filho que vocês vão descobrir o que fazer juntos.

Deixe alguém na escola (o diretor, a enfermeira da escola ou um conselheiro ou professor) saber sobre a situação. Eles geralmente estão em posição de monitorar e tomar medidas para evitar mais problemas.

Como o termo "bullying" pode ser usado para descrever uma ampla gama de situações, não existe uma abordagem única para todos. O que é aconselhável em uma situação pode não ser apropriado em outro. Muitos fatores - como a idade das crianças envolvidas, a gravidade da situação e o tipo específico de comportamentos de bullying - ajudará a determinar o melhor curso de ação.

Leve isso a sério se ouvir que o bullying vai piorar se descobrir que seu filho contou ou se ameaças de dano físico estão envolvidas. Às vezes é útil para abordar os pais do agressor. Mas na maioria dos casos, professores ou conselheiros são os melhores para entrar em contato primeiro. Se você tiver Tentei esses métodos e ainda quero falar com os pais da criança agressora, é melhor fazê-lo em um contexto onde um funcionário da escola, como um conselheiro, possa mediar.

A maioria das escolas tem políticas e programas anti-bullying. Além disso, muitos estados têm leis e políticas de bullying. Descubra mais sobre as leis em sua comunidade. Em certos casos, se você tiver sérias preocupações sobre a segurança de seu filho, você pode precisa entrar em contato com autoridades legais.

## Conselhos para crianças

Os pais podem ajudar as crianças a aprender como lidar com o bullying, se isso acontecer. Para alguns pais, pode ser tentador dizer a uma criança para lutar. Afinal, você está com raiva que seu criança está sofrendo e talvez lhe tenham dito para "se defender" quando você era jovem. Ou você pode se preocupar se seu filho continuará a sofrer nas mãos do valentão, e acho que revidar é a única maneira de colocar o valentão em seu lugar.

Mas é importante aconselhar as crianças a não reagir ao bullying brigando ou fazendo bullying costas. Pode rapidamente se transformar em violência, problemas e alguém se ferindo. Em vez disso, é melhor se afastar da situação, sair com outras pessoas e contar um adulto.

Aqui estão algumas outras estratégias para discutir com as crianças que podem ajudar a melhorar a situação e fazer com que se sintam melhor:

* Evite o agressor e use o sistema de camaradagem. Use um banheiro diferente se um agressor estiver por perto e não vá ao seu armário quando não houver ninguém por perto. Faço certifique-se de ter alguém com você para não ficar sozinho com o agressor. Amigo com um amigo no ônibus, nos corredores ou no recreio - onde quer que o agressor esteja. Ofereça-se para fazer o mesmo por um amigo.
* Contenha a raiva. É natural ficar chateado com o agressor, mas isso em que os valentões prosperam. Isso os faz sentir mais poderosos. Pratique não reagir por chorando ou parecendo vermelho ou chateado. Requer muita prática, mas é uma habilidade útil para se manter fora do radar de um agressor. Às vezes, as crianças acham útil praticar "legal para baixo "estratégias, como contar até 10, escrever suas palavras raivosas, aprofundar respirações, ou indo embora. Às vezes, a melhor coisa a fazer é ensinar as crianças a usar um "cara de pau" até que estejam longe de qualquer perigo (sorrir ou rir pode provocar o valentão).
* Aja com coragem, afaste-se e ignore o agressor. Com firmeza e clareza diga ao agressor para parar e depois vá embora. Pratique maneiras de ignorar os comentários prejudiciais, como agir desinteressado ou enviar mensagens de texto para alguém no seu celular. Ao ignorar o agressor, você está mostrando que não se importa. Eventualmente, o agressor provavelmente ficará entediado tentando incomodá-lo.
* Conte a um adulto. Professores, diretores, pais e funcionários do refeitório na escola podem ajudar a acabar com o bullying.
* Fale sobre isso. Fale com alguém em quem você confia, como uma orientação conselheiro, professor, irmão ou amigo. Eles podem oferecer algumas sugestões úteis, e mesmo que eles não consigam resolver a situação, isso pode ajudá-lo a se sentir um pouco menos sozinho.

## Restaurando a confiança

Lidar com o bullying pode minar a confiança de uma criança. Para ajudar a restaurá-lo, incentive seus filhos para passarem tempo com amigos que têm uma influência positiva. Participação em clubes, esportes ou outras atividades agradáveis ​​constrói força e amizades.

Dê ouvidos a situações difíceis, mas incentive seus filhos a também falar sobre os momentos bons do dia e ouvir com igual atenção. Faço certifique-se de que eles sabem que você acredita neles e que você fará o que puder para lidar com qualquer bullying que ocorre.