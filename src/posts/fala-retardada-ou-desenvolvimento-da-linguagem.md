---
title: Fala retardada ou desenvolvimento da linguagem
permalink: fala-retardada-ou-desenvolvimento-da-linguagem
layout: post.hbs
---

# Fala retardada ou desenvolvimento da linguagem

Tal como acontece com outras habilidades e marcos, a idade em que as crianças aprendem a língua e começam falar pode variar. Saber um pouco sobre o desenvolvimento da fala e da linguagem pode ajudar os pais descubra se há motivo para preocupação.

## Como a fala e o idioma diferem?

* Fala é a expressão verbal da linguagem e inclui articulação (a maneira como formamos sons e palavras).
* Linguagem é dar e receber informações. É compreensão e ser compreendido por meio da comunicação - verbal, não verbal e escrita.

## O que são atrasos de fala ou linguagem?

Os problemas de fala e linguagem são diferentes, mas costumam se sobrepor. Por exemplo:

* Uma criança com atraso de linguagem pode falar bem as palavras, mas apenas ser capaz de juntar duas palavras.
* Uma criança com atraso na fala pode usar palavras e frases para se expressar ideias, mas sejam difíceis de entender.

## Quais são os sinais de atraso na fala ou linguagem?

Um bebê que não responde a sons ou vocaliza deve ser examinado por um médico, certo longe. Mas, muitas vezes, é difícil para os pais saberem se seu filho está demorando um pouco mais para alcançar um marco de fala ou idioma, ou se houver um problema.

Aqui estão algumas coisas a serem observadas. Ligue para seu médico se seu filho:

* por 12 meses: não usa gestos, como como apontando ou acenando tchau
* por 18 meses: prefere gestos a vocalizações comunicar
* por 18 meses: tem problemas para imitar sons
* tem dificuldade para entender solicitações verbais simples
* por 2 anos: só pode imitar a fala ou ações e não produz palavras ou frases espontaneamente
* por 2 anos: diz apenas alguns sons ou palavras repetidamente e não pode usar a linguagem oral para comunicar mais do que suas necessidades imediatas
* por 2 anos: não consigo seguir instruções simples
* por 2 anos: tem um tom de voz incomum (como um som áspero ou nasal)

Também chame o médico se a fala do seu filho for mais difícil de entender do que esperado para sua idade:

* Os pais e cuidadores regulares devem entender cerca de 50% da fala de uma criança aos 2 anos e 75% disso aos 3 anos.
* Aos 4 anos, uma criança deve ter principalmente compreendida, mesmo por pessoas que não conhecem a criança.

## O que causa atrasos na fala ou linguagem?

Um atraso na fala pode ser devido a:

* uma deficiência oral, como problemas com a língua ou palato (céu da boca)
* um frênulo curto (a dobra abaixo da língua), que pode limitar o movimento da língua

Muitas crianças com atraso na fala têm problemas motores orais. Isso acontece quando há um problema nas áreas do cérebro responsáveis ​​pela fala. Isso torna difícil coordenar os lábios, língua e mandíbula para fazer os sons da fala. Essas crianças também pode ter outros problemas motores orais, como problemas de alimentação.

Problemas de audição também podem afetar a fala. Portanto, um audiologista deve teste a audição de uma criança sempre que houver um discurso preocupação. Crianças com problemas de audição podem ter dificuldade em dizer, compreender, imitar, e usando a linguagem.

Infecções de ouvido, especialmente infecções crônicas podem afetar a audição. Mas, enquanto houver audição normal em um ouvido, a fala e a linguagem se desenvolverão normalmente.

## Como os atrasos de fala ou linguagem são diagnosticados?

Se seu filho tiver um problema, é importante consultar um fonoaudiólogo (SLP) imediatamente. Você pode encontrar um fonoaudiólogo sozinho ou perguntar ao seu profissional de saúde para encaminhá-lo a um.

O SLP (ou fonoaudiólogo) verificará as habilidades de fala e linguagem de seu filho. O patologista fará testes padronizados e buscará marcos na fala e na linguagem desenvolvimento.

O SLP também verificará:

* o que seu filho entende (chamado de linguagem receptiva)
* o que seu filho pode dizer (chamado de linguagem expressiva)
* desenvolvimento de som e clareza de fala
* oral do seu filho - estado motor (como a boca, língua, palato, etc., funcionam juntos para falar, bem como comer e engolir)

Com base nos resultados do teste, o fonoaudiólogo pode recomendar a fala terapia para o seu filho.

## Como a terapia da fala ajuda?

O fonoaudiólogo trabalhará com seu filho para melhorar as habilidades de fala e linguagem, e mostrar o que fazer em casa para ajudar seu filho.

## Como os pais podem ajudar?

Os pais são uma parte importante para ajudar as crianças que têm problemas de fala ou linguagem.

Aqui estão algumas maneiras de incentivar o desenvolvimento da fala em casa:

* Foco na comunicação. Converse com seu bebê, cante e incentive imitação de sons e gestos.
* Leia para seu filho. Comece a ler quando seu filho é um bebê. Procure por livros ou gravuras apropriados para a idade livros que incentivam as crianças a olhar enquanto você nomeia as fotos.
* Use situações cotidianas. Para desenvolver a fala de seu filho e linguagem, converse durante o dia. Nomeie os alimentos no supermercado, explique o que você está fazendo ao cozinhar uma refeição ou limpar uma sala e apontar objetos ao redor a casa. Mantenha as coisas simples, mas evite "conversa de bebê".

Reconhecer e tratar atrasos na fala e na linguagem desde o início é a melhor abordagem. Ligue para seu médico se tiver alguma dúvida sobre a fala ou linguagem de seu filho desenvolvimento.