---
title: Engasgando
permalink: engasgando
layout: post.hbs
---

# Engasgando

## O que é gagueira?

Muitas crianças passam por um estágio entre as idades de 2 e 5 anos quando gaguejam. Isso pode torná-los:

* repita certas sílabas, palavras ou frases
* prolongue-os
* pare, sem emitir nenhum som para certos sons e sílabas

A gagueira é uma forma de disfluência (dis-FLOO-en-see), uma interrupção no fluxo da fala.

Em muitos casos, a gagueira desaparece por conta própria aos 5 anos de idade. Em algumas crianças, ela desaparece por mais tempo. Tratamentos eficazes estão disponíveis para ajudar a criança a superá-lo.

## O que causa a gagueira?

Médicos e cientistas não sabem ao certo por que algumas crianças gaguejam. Mas a maioria acredita que algumas coisas contribuem para isso, como um problema com a forma como as mensagens do cérebro interagir com os músculos e partes do corpo necessários para falar.

Muitos acreditam que a gagueira pode ser genética. Crianças que gaguejam são três vezes mais é provável que tenha um parente próximo que também gagueje ou tenha.

## Quais são os sinais de gagueira?

Os primeiros sinais de gagueira tendem a aparecer quando uma criança tem entre 18 e 24 anos meses de idade. Nessa idade, há uma explosão no vocabulário e as crianças estão começando a colocar palavras juntas para formar frases. Para os pais, a gagueira pode ser perturbadora e frustrante, mas é natural que as crianças gaguejem neste estágio. Seja como paciente com seu filho quanto possível.

Uma criança pode gaguejar por algumas semanas ou vários meses, e a gagueira pode vir e ir. A maioria das crianças que começam a gaguejar antes dos 5 anos param sem qualquer necessidade de ajuda, como terapia da fala ou linguagem.

Mas se a gagueira do seu filho acontece muito, piora ou acontece junto com movimentos corporais ou faciais, vendo uma linguagem de fala ter um terapeuta por volta dos 3 anos é uma boa ideia.

Normalmente, a gagueira cessa quando as crianças entram na escola primária e começam a afiar suas habilidades de comunicação. É provável que uma criança em idade escolar que continue a gaguejar ciente do problema e pode ficar envergonhado por ele. Colegas e amigos podem desenhar atenção a ele ou até mesmo provocar a criança.

Se isso acontecer com seu filho, fale com o professor, quem pode resolver isso na sala de aula com as crianças. O professor também pode diminuir o número de situações estressantes de falar para seu filho até o início da terapia da fala.

## Quando obter ajuda

Se o seu filho tem 5 anos e ainda gagueja, converse com seu médico ou com um fonoaudiólogo terapeuta. Verifique com um fonoaudiólogo se seu filho:

* tenta evitar situações que exijam conversa
* muda uma palavra por medo de gaguejar
* tem movimentos faciais ou corporais junto com a gagueira
* repete palavras e frases inteiras com frequência e consistência
* repete sons e sílabas com mais frequência
* tem uma fala que soa muito tensa

Fale também com o terapeuta se:

* você percebe um aumento da tensão facial ou rigidez nos músculos da fala de seu filho
* você percebe uma tensão vocal que causa aumento do tom ou volume
* você tem outras preocupações sobre a fala de seu filho

A maioria das escolas oferecerá testes e terapia apropriada se a gagueira durar 6 meses ou mais.

## Como os pais podem ajudar?

Experimente estes passos para ajudar o seu filho:

* Não exija que seu filho fale com precisão ou corretamente o tempo todo. Permitir falar para ser divertido e agradável.
* Use as refeições em família como um momento de conversa. Evite distrações, como rádio ou TV.
* Evite correções ou críticas como "desacelere", "leve o seu tempo" ou "leve respire fundo. "Esses comentários, embora bem-intencionados, só farão com que seu filho sinta-se mais constrangido.
* Evite que seu filho fale ou leia em voz alta quando estiver desconfortável ou gaguejando aumenta. Em vez disso, durante esses momentos, incentive atividades que não exijam um muita conversa.
* Não interrompa seu filho ou diga a ele para começar de novo.
* Não diga a seu filho para pensar antes de falar.
* Proporcione um ambiente calmo em casa. Tente diminuir o ritmo da vida familiar.
* Fale devagar e claramente ao falar com seu filho ou outras pessoas na presença dele.
* Mantenha contato visual com seu filho. Tente não desviar o olhar ou mostrar sinais de ser chateado.
* Deixe seu filho falar por si mesmo e terminar pensamentos e frases. Faça uma pausa antes de responder às perguntas ou comentários do seu filho.
* Fale devagar com seu filho. Isso requer prática! Modelando uma velocidade de fala lenta ajudará com a fluência do seu filho.