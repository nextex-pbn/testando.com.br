---
title: Sexting: o que os pais precisam saber
permalink: sexting-o-que-os-pais-precisam-saber
layout: post.hbs
---

# Sexting: o que os pais precisam saber

## O que é sexting?

Sexting (ou "mensagens de texto sexuais") é enviar ou se tornar sexualmente explícito ou sugestivo imagens, mensagens ou vídeo em um smartphone ou pela Internet.

Sexting inclui envio:

* fotos ou selfies com nudez ou quase nudez
* vídeos que mostram nudez, atos sexuais ou simulação de sexo
* mensagens de texto que propõem sexo ou se referem a atos sexuais

## Por que os adolescentes fazem sexo oral?

A maioria dos adolescentes tem várias maneiras de se conectar à Internet: smartphones, tablets e laptops, todos pode ser usado em privado. É muito fácil para os adolescentes criar e compartilhar fotos pessoais e vídeos de si mesmos sem que os pais saibam.

As meninas podem fazer sexo como uma piada, como uma forma de chamar a atenção ou por causa da pressão dos colegas ou pressão de caras. Os rapazes às vezes culpam a "pressão dos amigos". Para alguns, porém, quase se tornou um comportamento normal, uma forma de flertar, parecer legal ou se tornar popular.

E os adolescentes têm apoio para isso quando fotos e vídeos obscenos de celebridades se tornam populares. Em vez de carreiras arruinadas ou humilhação, as consequências costumam ser mais fama e reality shows.

## Quais problemas podem acontecer com o sexting?

Os adolescentes devem entender que mensagens, fotos ou vídeos enviados pela Internet ou smartphones nunca são verdadeiramente privados ou anônimos. Em segundos, eles podem estar lá fora para todo o mundo ver.

Mesmo que a imagem, vídeo ou texto fosse destinado a apenas uma pessoa, depois de enviado ou postado, está fora do controle do seu filho. Muitas pessoas podem ver e pode seja impossível de apagar da Internet, mesmo que seu filho pense que ela sumiu.

Se uma imagem comprometedora se tornar pública ou enviada a outras pessoas, seu filho adolescente pode estar em risco de humilhação, constrangimento e ridículo público. Pior ainda, pode danificar a autoimagem do seu filho adolescente e até mesmo levar à depressão e outros problemas de saúde mental.

E pode haver consequências legais. Em alguns estados, um adolescente pode enfrentar acusações criminais para enviar mensagens de texto com fotos explícitas ou mesmo ter que se registrar como agressor sexual.

Comportamentos de risco on-line podem perseguir um candidato a uma faculdade ou em busca de emprego anos depois. Muitas faculdades e empregadores verificam os perfis online em busca de sinais de maturidade - ou bandeiras vermelhas gigantes sobre mau julgamento.

## Como posso ajudar meu filho adolescente?

Pode ser difícil para os adolescentes compreender os resultados a longo prazo dos comportamentos impulsivos. Eles podem não entender como compartilhar tudo agora arrisca sua reputação mais tarde.

Converse com seus filhos sobre como fotos, vídeos, e-mails e textos que parecem temporários pode existir para sempre no ciberespaço. Uma imagem atrevida enviada para o telefone de uma paixão pode facilmente ser encaminhado para amigos, postado online ou impresso e distribuído. Uma imagem enviada para um namorado ou namorada pode levar a problemas se outra pessoa ver ou for distribuído após uma separação.

Então, como você pode chegar até seus filhos? Fale abertamente sobre responsabilidade pessoal, limites pessoais e como resistir à pressão dos colegas. Conversas como esta deveriam acontecem com frequência, não apenas quando surgem problemas.

Explique, com antecedência e com frequência, que uma imagem ou mensagem enviada não pode ser devolvida. isto pode, e provavelmente irá, se espalhar para outras pessoas que não deveriam ver. Ensine as crianças a siga a regra "WWGT" ("O que a vovó pensaria?"). Se a vovó não deveria ver, eles não deveriam enviá-lo.

E deixe claro que haverá consequências se seus filhos forem pegos sexting. Esteja pronto para remover dispositivos ou definir limites para quando e como eles podem usá-los.