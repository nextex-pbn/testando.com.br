---
title: Sanduíches De Creme De Pepino
permalink: sanduiches-de-creme-de-pepino
layout: post.hbs
---

# Sanduíches De Creme De Pepino

Observação: você deve manter esses sanduíches refrigerados. Eles vão durar 8 horas, firmemente embrulhado.

Tempo de preparação: 5–10 minutos

## O que você precisa:

* 3/4 xícara de requeijão light, ligeiramente amolecido
* meio pepino grande em fatias finas
* 8 fatias de pão integral

Equipamentos e suprimentos:

* Faca grande
* Tábua de corte
* Filme plástico (opcional)

## O que fazer:

1. Passe queijo cremoso em cada fatia de pão (cerca de 3 colheres de sopa por sanduíche).
2. Coloque cerca de 4 fatias de pepino em 4 das fatias de pão e cubra com o restante fatias de pão.
3. Corte em quartos e sirva imediatamente ou embrulhe sanduíches e guarde para mais tarde.

## Análise nutricional (por porção):

* 250 calorias
* 10g de proteína
* 10g de gordura
* 6g sat. gordura
* 30g de carboidrato
* fibra 4g
* 20 mg de colesterol
* 520mg de sódio
* 6g de açúcar

Serve: 4

Tamanho da porção: 1 sanduíche (142g)

Observação: a análise nutricional pode variar dependendo das marcas de ingredientes usadas.