---
title: Especialistas em saúde mental
permalink: especialistas-em-saude-mental
layout: post.hbs
---

# Especialistas em saúde mental

## Quais são os tipos de especialistas em saúde mental?

Conselheiros, terapeutas, assistentes sociais clínicos, psicólogos e psiquiatras são todos especialistas em saúde mental. Mas eles têm diferentes níveis de educação e alguns oferecem diferentes serviços de saúde mental.

Ao procurar atendimento de saúde mental, você também pode ouvir o termo "saúde comportamental". Refere-se a tudo o que você considera como saúde mental, como ajuda para as lutas em torno de condições como depressão e ansiedade, e desafios como luto e vício.

## Que tipo de especialista em saúde mental é o melhor para meu filho?

A primeira etapa para obter ajuda para seu filho é encontrar um especialista com o treinamento e experiência adequados às suas necessidades. Mas com tantas opções para escolher, esta tarefa simples pode parecer complicada.

Comece com o seu médico de família. Eles estão chamados de prestadores de cuidados "primários" por uma razão. A menos que seja uma emergência, vá para o seu pediatra da criança ou seu médico de família primeiro para preocupações mentais e físicas.

Alguns problemas de saúde mental têm uma causa física. Quando não o fazem, seu médico de família ou o pediatra pode ajudá-lo a encontrar o especialista certo em saúde mental.

Aqui está quem seu médico pode recomendar:

## Terapeutas e conselheiros

Na maioria das vezes, essas duas palavras significam a mesma coisa. Ambos são especialistas em usar técnicas de psicoterapia para ajudar seus clientes. Isso inclui resolver o problema crenças, mudança de comportamentos, melhoria de perspectivas emocionais e muito mais.

Quando tratam crianças, os terapeutas e os conselheiros podem trabalhar com toda a família ou apenas com a criança. Eles usam uma variedade de maneiras de ajudar as crianças a se expressarem, como desenhar, brincar e falar. Mais velho crianças e adolescentes podem estar prontos para atividades que os ajudem a aprender as habilidades necessárias.

Às vezes, pode ajudar os pais a consultar um terapeuta sozinhos para conseguirem seus próprios suporte.

A maioria dos estados exige que terapeutas e conselheiros licenciados tenham um mestrado. Portanto, além de um diploma de 4 anos, eles têm formação em sua área específica.

Uma maneira de aprender mais sobre um terapeuta ou conselheiro clínico é examinar o letras que vêm depois de seus nomes. Estas são as abreviações de uma licença ou acreditação que se referem à educação e treinamento de um provedor. Diferentes tipos de conselheiros e os terapeutas incluem:

* Conselheiro profissional licenciado (LPC): alguém que é treinado para tratar distúrbios mentais, comportamentais e emocionais.
* Casamento licenciado e terapeuta familiar (LMFT): Alguém que trata uma ampla gama de transtornos de saúde mental. Eles também ajudam famílias e casais a trabalhar através de seus próprios desafios.
* Conselheiro clínico licenciado de álcool e drogas (LCADAC): Alguém que se especializou em trabalhar com famílias que lutam contra as drogas e abuso de álcool.

Pergunte se o conselheiro ou terapeuta de saúde mental do seu filho é certificado e descubra sobre sua educação e experiência com crianças.

## Assistentes sociais clínicos

Os assistentes sociais podem ajudar as pessoas a terem uma vida mais saudável e produtiva. Clínico assistentes sociais se concentram em diagnosticar e tratar problemas de saúde mental.

Como terapeutas e conselheiros, assistentes sociais clínicos podem ajudar famílias a trabalhar através de experiências desafiadoras como o divórcio. Eles também podem tratar problemas de saúde mental, como depressão.

Assistentes sociais clínicos trabalham em muitos lugares, incluindo médicos de família, em hospitais e em organizações sem fins lucrativos.

Eles têm pelo menos o título de mestre. Aqui estão alguns dos tipos de clínica assistentes sociais:

* Assistente social clínico licenciado (LCSW): Eles podem ajudar os clientes trabalhe através de uma ampla variedade de desafios mentais e emocionais. Eles são obrigados a atender aos requisitos de educação e experiência, embora variem por estado.
* Academia de assistente social certificado (ACSW): são clínicas assistente social com mestrado e 2 anos de experiência organizacional. Estes assistentes sociais são credenciados pela Academy of Certified Social Workers.

## Enfermeiras

Enfermeiros com educação avançada fornecem uma ampla gama de serviços de saúde mental, incluindo terapia e, às vezes, gerenciamento de medicamentos. Eles normalmente ganharam um mestrado ou doutorado, como um doutorado ou doutorado em prática de enfermagem (DNP). Eles são chamados de enfermeiros de saúde mental ou psiquiátricos enfermeiros.

## Psicólogos e psiquiatras

Esses títulos parecem semelhantes, mas têm uma diferença importante.

### Psicólogos

Como um conselheiro ou terapeuta, os psicólogos podem fazer um diagnóstico de saúde mental e desenvolver um plano de tratamento. Eles têm doutorado (como PhD ou PsyD), mas não são médicos e não podem prescrever medicamentos.

### Psiquiatras

Um psiquiatra é um médico (MD) que pode fazer tudo o que um psicólogo pode, além de prescrever remédios.

Para crianças e adolescentes que se beneficiam apenas da psicoterapia, seja um psicólogo ou um psiquiatra é uma ótima escolha. Quem precisa de medicação deve consultar um psiquiatra. Muitas vezes, também é útil fazer terapia de conversa junto com medicamentos.