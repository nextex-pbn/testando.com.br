---
title: Ansiedade de separação
permalink: ansiedade-de-separacao
layout: post.hbs
---

# Ansiedade de separação

Despedidas cheias de lágrimas e birra são comuns durante os primeiros anos de uma criança. Por aí no primeiro aniversário, muitas crianças desenvolvem ansiedade de separação, ficando chateadas quando um dos pais tenta deixá-los com outra pessoa.

Embora a ansiedade de separação seja uma parte perfeitamente normal do desenvolvimento infantil, pode ser perturbador.

Compreender o que seu filho está passando e ter algumas estratégias de enfrentamento pronto pode ajudar vocês dois a superar isso.

## Sobre ansiedade de separação

Os bebês se adaptam muito bem a outros cuidadores. Os pais provavelmente sentem mais ansiedade sobre estar separada do que crianças! Desde que suas necessidades sejam atendidas, a maioria Bebês com menos de 6 meses se adaptam facilmente a outras pessoas.

Entre 4 e 7 meses de idade, os bebês desenvolvem uma sensação de "permanência do objeto". Eles estão perceber que as coisas e pessoas existem mesmo quando estão fora de vista. Os bebês aprendem que quando não podem ver a mãe ou o pai, isso significa eles foram embora. Eles não entendem o conceito de tempo, então eles não conhecem a mãe vai voltar e pode ficar chateado com sua ausência. Esteja a mãe na cozinha, no quarto ao lado ou no escritório, é tudo o mesmo para o bebê, que pode chorar até que a mãe esteja por perto novamente.

Crianças entre 8 meses e 1 ano estão se tornando crianças mais independentes, no entanto, estão ainda mais incertos sobre estar separados de um dos pais. É quando a separação a ansiedade se desenvolve e as crianças podem ficar agitadas e chateadas quando um pai tenta sair.

Se você precisar ir para a próxima sala por apenas alguns segundos, deixe seu filho com uma babá à noite, ou deixar seu filho na creche, seu filho pode agora reaja chorando, agarrando-se a você e resistindo à atenção dos outros.

O momento da ansiedade de separação pode variar. Algumas crianças podem passar por isso mais tarde, entre 18 meses e 2 anos e meio de idade. Alguns nunca experimentam isso. E para outros, certos estresses da vida podem desencadear sentimentos de ansiedade por estar separado de um pai: uma nova situação de cuidado infantil ou cuidador, um novo irmão, mudando-se para um novo lugar ou tensão em casa.

## Quanto tempo dura?

A duração da ansiedade da separação pode variar, dependendo da criança e de como os pais responde. Em alguns casos, dependendo do temperamento da criança, a ansiedade da separação pode duram desde a infância até os anos do ensino fundamental.

A ansiedade da separação que afeta as atividades normais de uma criança mais velha pode ser um sinal de um transtorno de ansiedade mais profundo. E se A ansiedade da separação aparece do nada em uma criança mais velha, pode haver outro problema, como bullying ou abuso.

A ansiedade da separação é diferente dos sentimentos normais que as crianças mais velhas têm quando não quero que um dos pais saia (o que geralmente pode ser superado se uma criança estiver distraída o suficiente). E as crianças entendem o efeito disso tem nos pais. Se você correr de volta para o quarto toda vez que seu filho chorar ou cancelar seus planos, seu filho continuará a usar essa tática para evitar a separação.

## O que você pode sentir

A ansiedade da separação pode fazer você sentir uma variedade de emoções. Pode ser bom sentir que seu filho está finalmente tão apegado a você como você é para ele ou ela. Mas também é provável que você se sinta culpado por demorar por si mesma, deixando seu filho com um cuidador ou indo para o trabalho. E você pode comece a se sentir oprimido pela quantidade de atenção que seu filho parece precisar de você.

Lembre-se de que a relutância do seu filho em deixá-lo é um bom sinal de que apegos saudáveis ​​se desenvolveram entre vocês dois. Eventualmente, seu filho será capaz de lembrar que você sempre volta depois de sair, e isso será conforto o suficiente enquanto você estiver fora. Isso também dá crianças a chance de desenvolver habilidades de enfrentamento e um pouco de independência.

## Facilitando as despedidas

Essas dicas podem ajudar a aliviar as crianças e os pais neste período difícil:

* O tempo é tudo. Tente não iniciar uma creche ou creche com uma pessoa desconhecida quando seu filho tem entre 8 meses e 1 ano de idade, quando é provável que apareça primeiro a ansiedade de separação. Além disso, tente não sair quando seu filho está cansado, com fome ou inquieto. Se possível, programe suas saídas para depois cochilos e refeições.
* Pratique. Pratique estar longe um do outro e apresente novas pessoas e lugares lentamente. Se você planeja deixar seu filho com um parente ou um nova babá, convide essa pessoa com antecedência para que possam passar um tempo juntos enquanto você está na sala. Se seu filho está começando em uma nova creche ou pré-escola, façam algumas visitas juntos antes de começar uma programação em tempo integral. Pratique sair seu filho com um cuidador por curtos períodos para que ele ou ela possa se acostumar a ser longe de você.
* Seja calmo e consistente. Crie um ritual de saída durante o qual você diga um adeus agradável, amoroso e firme. Fique calmo e mostre confiança em seu filho. Tranquilize-o de que você voltará - e explique quando voltará usando conceitos que as crianças vão entender (como depois do almoço). Dê toda a sua atenção quando você diz adeus, e quando você diz que está indo embora, seja sincero; voltar só vai fazer coisas piores.
* Cumpra as promessas. É importante ter certeza de que você retorna quando você prometeu. Isso é fundamental - é assim que seu filho desenvolverá a confiança de que ele ou ela pode superar o tempo separados.

Por mais difícil que seja deixar uma criança gritando e chorando por você, é importante ter a confiança de que o cuidador pode lidar com isso. No momento em que você conseguir para o seu carro, é provável que seu filho tenha se acalmado e esteja brincando com outras coisas.

Se você está cuidando do filho de outra pessoa que sofre de ansiedade de separação, tente para distrair a criança com uma atividade ou brinquedo, ou com músicas, jogos ou qualquer outra coisa Isto é engraçado. Você pode ter que continuar tentando até que algo clique na criança.

Além disso, tente não mencionar a mãe ou o pai da criança, mas responda ao perguntas de uma forma simples e direta. Você pode dizer: "Mamãe e papai são vai estar de volta assim que terminar o jantar. Vamos brincar com alguns brinquedos! "

## É apenas temporário

Lembre-se, esta fase vai passar. Se seu filho nunca foi cuidado por ninguém mas você é naturalmente tímido ou tem outros estresses, a ansiedade de separação pode ser pior do que para outras crianças.

Além disso, confie nos seus instintos. Se seu filho se recusa a ir a uma certa babá ou creche ou mostra outros sinais de tensões, como dificuldade para dormir ou perda de apetite, pode haver um problema com a situação do acolhimento de crianças.

Se a ansiedade de separação intensa persistir na pré-escola, no ensino fundamental ou além e interfere nas atividades diárias, converse com seu médico. Pode ser um sinal de uma condição rara, porém mais séria, conhecida como transtorno de ansiedade de separação. Crianças com esse transtorno temem perder-se da família membros e muitas vezes estão convencidos de que algo ruim vai acontecer. Converse com seu medico se seu filho tem sinais disso, incluindo:

* sintomas de pânico (como náuseas, vômitos ou falta de ar) ou ataques de pânico antes que um dos pais saia
* pesadelos sobre separação
* medo de dormir sozinho (embora também seja comum em crianças que não têm separação ansiedade)
* preocupação excessiva em ser perdido ou sequestrado ou em ir a lugares sem os pais

Para a maioria das crianças, a ansiedade de estar longe dos pais passa sem qualquer necessidade para atenção médica. Mas se você tiver dúvidas, converse com seu médico.