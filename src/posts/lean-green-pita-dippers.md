---
title: Lean Green Pita Dippers
permalink: lean-green-pita-dippers
layout: post.hbs
---

# Lean Green Pita Dippers

Complete este almoço leve adicionando seus vegetais picados favoritos ou uma salada lateral.

Tempo de preparação: 10 minutos

## O que você precisa:

* 4 pitas de trigo integral
* 1 xícara de iogurte grego natural sem gordura
* 1/2 xícara de espinafre fresco ou congelado
* 1/4 de pepino fatiado
* pitada de sal
* 1/2 colher de chá de pimenta preta

Equipamento necessário:

* Processador de alimentos ou liquidificador
* faca
* Assadeiras

## O que fazer:

1. Pré-aqueça o forno a 400 ° F (204 ° C).
2. Coloque iogurte, espinafre, pepino, sal e pimenta no processador de alimentos ou liquidificador. Processe até ficar homogêneo, cerca de 1 minuto.
3. Coloque o molho em uma tigela e leve à geladeira até que os chips estejam prontos.
4. Corte os pitas em 8 fatias.
5. Organize as fatias de fita em uma assadeira em uma única camada.
6. Pulverize com spray de cozinha e polvilhe levemente com alho em pó.
7. Asse por 5 minutos ou até dourar claro e crocante.
8. Use as fatias de pão árabe para recolher o molho de iogurte!

## Análise nutricional (por porção):

* 210 calorias
* 12g de proteína
* 1,5g de gordura
* 0g sat. gordura
* 38g de carboidrato
* fibra 5g
* 0mg de colesterol
* 410mg de sódio
* 4g de açúcar

Serve: 4

Tamanho da porção: 8 chips e molho de 1/2 xícara

Observação: a análise nutricional pode variar dependendo das marcas de ingredientes usadas.