---
title: Levando seu filho a um terapeuta
permalink: levando-seu-filho-a-um-terapeuta
layout: post.hbs
---

# Levando seu filho a um terapeuta

## O que é terapia?

Muitas crianças e adolescentes têm problemas que afetam a forma como se sentem, agem ou aprendem. A terapia é um tipo de tratamento para esses problemas. É uma forma de obter ajuda para o seu criança.

Na terapia, as crianças falam e aprendem como resolver seus problemas. Indo para a terapia os ajuda a lidar melhor, se comunicar melhor e fazer melhor.

## Com quais problemas os terapeutas ajudam?

Os terapeutas são treinados para ajudar em todos os tipos de problemas. Por exemplo, eles ajudam crianças e adolescentes passando por momentos difíceis como:

* problemas familiares
* problemas escolares
* bullying
* problemas de saúde

Eles ajudam com sentimentos como:

* tristeza
* raiva
* estresse e preocupação
* baixa autoestima
* luto

Eles ajudam crianças e adolescentes com doenças como:

* TDAH
* depressão
* TOC e ansiedade
* transtornos alimentares
* automutilação
* transtornos de comportamento perturbadores
* transtornos relacionados a traumas

## Por que crianças e adolescentes precisam de terapia?

Crianças e adolescentes precisam de terapia quando têm problemas que não conseguem resolver sozinhos. Ou eles precisam de ajuda quando os problemas afetam como eles se comportam, sentem ou agem. Se as coisas não melhorar por conta própria, as crianças podem precisar de terapia para que as coisas possam melhorar. Às vezes, inteiro As famílias precisam de apoio enquanto tentam se comunicar, aprender e criar limites.

## Como funciona a terapia?

Na terapia, as crianças aprendem fazendo. Com crianças mais novas, isso significa trabalhar com o família inteira, desenhando, brincando e conversando. Para crianças mais velhas e adolescentes, terapeutas compartilhe atividades e ideias que enfoquem o aprendizado das habilidades de que precisam. Eles falam através dos sentimentos e resolver problemas.

Os terapeutas elogiam e apoiam enquanto as crianças aprendem. Eles ajudam as crianças a acreditar em si mesmas e encontrar seus pontos fortes. A terapia cria padrões de pensamento úteis e comportamentos saudáveis hábitos.

Um terapeuta pode se encontrar com a criança e os pais juntos ou se encontrar com a criança sozinho. Depende da idade da criança. Um terapeuta também pode se encontrar com um pai para dê dicas e ideias de como ajudar seus filhos em casa.

## O que acontece na terapia?

No início, o terapeuta se encontrará com você e seu filho para conversar. Eles vão perguntar perguntas e ouvir. Isso os ajuda a aprender mais sobre seu filho e sobre o problema. O terapeuta dirá como ele pode ajudar.

Depois disso, seu filho fará mais consultas para terapia. Nessas visitas, seu filho pode:

* Fale. Falar é uma forma saudável de expressar sentimentos. Quando crianças Coloque os sentimentos em palavras em vez de ações, eles podem agir da melhor maneira. Quando alguém ouve e sabe como se sentem, as crianças estão mais prontas para aprender.
* Faça atividades. Os terapeutas usam atividades para ensinar sobre sentimentos e habilidades de enfrentamento. Eles podem fazer com que as crianças desenhem ou brinquem como uma forma de aprender. Eles podem ensinar atenção plena e respiração calma como forma de diminuir o estresse.
* Pratique novas habilidades. Os terapeutas ajudam as crianças a praticar o que eles aprender. Eles podem jogar jogos em que as crianças precisam esperar sua vez, usar o autocontrole, seja paciente, siga as instruções, ouça, compartilhe, tente novamente ou lide com as perdas.
* Resolva problemas. Com crianças mais velhas e adolescentes, os terapeutas perguntam como os problemas os afetam em casa, na escola. Eles falam sobre como resolver esses problemas.

## Por quanto tempo as crianças fazem terapia?

A duração da terapia depende dos objetivos que você e o terapeuta do seu filho têm. Na maioria das vezes, um terapeuta vai querer se encontrar com seu filho uma vez por semana durante um alguns meses.

## Como os pais podem ajudar?

Você pode fazer coisas para ajudar seu filho a aproveitar ao máximo a terapia. Aqui estão alguns de eles:

* Encontre um terapeuta com quem você e seu filho se sintam confortáveis. Seu a equipe de saúde da criança pode ajudá-lo a encontrar alguém.
* Leve seu filho a todos os compromissos. A mudança leva tempo. isto requer muitas consultas de terapia para seu filho aprender novas habilidades e mantê-las atualizadas.
* Encontre-se com o terapeuta do seu filho. Pergunte o que fazer quando seu filho mostra problemas em casa. Pergunte como ajudar seu filho a se sair bem.
* Passe algum tempo com seu filho. Brinque, cozinhe, leia ou ria juntos. Faça isso todos os dias, mesmo que seja apenas por alguns minutos.
* Pais com paciência e cordialidade. Use palavras gentis, mesmo quando você precisa corrigir seu filho. Mostre amor. Elogie quando seu filho estiver bem ou tentando muito.