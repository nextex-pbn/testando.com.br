---
title: Sobre o suicídio de adolescentes
permalink: sobre-o-suicidio-de-adolescentes
layout: post.hbs
---

# Sobre o suicídio de adolescentes

A tragédia de um jovem morrendo por causa de uma enorme desesperança ou frustração é devastador para a família, amigos e comunidade. Pais, irmãos, colegas de classe, treinadores e vizinhos podem ficar se perguntando se eles poderiam ter feito algo para impedir que o jovem se suicide.

Saber mais sobre o que pode levar um adolescente ao suicídio pode ajudar a prevenir ainda mais tragédias. Mesmo que nem sempre seja evitável, é sempre uma boa ideia ser informado e tome medidas para ajudar um adolescente problemático.

## Sobre o suicídio de adolescentes

As razões por trás do suicídio ou da tentativa de suicídio de um adolescente podem ser complexas. Apesar suicídio é relativamente raro entre crianças, a taxa de suicídios e tentativas de suicídio aumenta muito durante a adolescência.

O suicídio é a terceira causa de morte de jovens de 15 a 24 anos, de acordo com aos Centros de Controle e Prevenção de Doenças (CDC), após acidentes e homicídios. Também se acredita que pelo menos 25 tentativas são feitas para cada suicídio de adolescente concluído.

O risco de suicídio aumenta drasticamente quando crianças e adolescentes têm acesso a armas de fogo em casa, e quase 60% de todos suicídios nos Estados Unidos são cometidos com arma de fogo. É por isso que qualquer arma em seu casa deve ser descarregada, trancada e mantida fora do alcance de crianças e adolescentes.

A sobredosagem com medicamentos de venda livre, com ou sem receita é também um método muito comum para tentar e completar o suicídio. É importante monitorar cuidadosamente todos os medicamentos em sua casa. Esteja ciente de que os adolescentes irão "trocar" diferentes medicamentos prescritos na escola e carregá-los (ou armazená-los) em seus armário ou mochila.

As taxas de suicídio variam entre meninos e meninas. As meninas pensam e tentam o suicídio cerca de duas vezes mais que os meninos, e tendem a tentar o suicídio por overdose de drogas ou cortando-se. No entanto, meninos morrem por suicídio cerca de quatro vezes mais meninas, talvez porque eles tendem a usar métodos mais letais, como armas de fogo, enforcamento ou salto das alturas.

## Quais adolescentes correm risco de suicídio?

Pode ser difícil lembrar como era ser adolescente, preso naquela área cinzenta entre infância e idade adulta. Claro, é um momento de enormes possibilidades, mas também pode ser um período de estresse e preocupação. Há pressão para se encaixar socialmente, para realizar academicamente e agir com responsabilidade.

A adolescência também é uma época sexual identidade e relacionamentos e uma necessidade de independência que muitas vezes entra em conflito com as regras e expectativas estabelecidas por outros.

Jovens com problemas de saúde mental - como ansiedade, depressão, bipolar distúrbio ou insônia - apresentam maior risco de pensamentos suicidas. Adolescentes passando por grandes mudanças na vida (divórcio dos pais, mudança, pai saindo de casa devido ao serviço militar ou separação dos pais, mudanças financeiras) e aqueles que são vítimas de bullying e têm maior risco de pensamentos suicidas.

Fatores que aumentam o risco de suicídio entre adolescentes incluem:

* um distúrbio psicológico, especialmente depressão, transtorno bipolar e álcool e uso de drogas (na verdade, cerca de 95% dos pessoas que morrem por suicídio têm um distúrbio psicológico no momento da morte)
* sentimentos de angústia, irritabilidade ou agitação
* sentimentos de desesperança e inutilidade que muitas vezes acompanham a depressão
* uma tentativa anterior de suicídio
* um histórico familiar de depressão ou suicídio
* abuso emocional, físico ou sexual
* falta de uma rede de apoio, relacionamento ruim com pais ou colegas e sentimentos de isolamento social
* lidar com bissexualidade ou homossexualidade em uma família ou comunidade que não o apóia ou ambiente escolar hostil

## Sinais de alerta

O suicídio entre adolescentes costuma acontecer após um evento estressante, como problemas na escola, um rompimento com um namorado ou namorada, a morte de um ente querido, um divórcio ou um grande conflito familiar.

Adolescentes que estão pensando em suicídio podem:

* falar sobre suicídio ou morte em geral
* dar dicas de que eles podem não estar mais por perto
* fale sobre sentir-se sem esperança ou culpado
* afaste-se de amigos ou familiares
* escreva canções, poemas ou cartas sobre morte, separação e perda
* comece a distribuir bens valiosos para irmãos ou amigos
* perder o desejo de participar de atividades ou atividades favoritas
* têm problemas para se concentrar ou pensar com clareza
* experimentar mudanças nos hábitos de comer ou dormir
* se envolva em comportamentos de risco
* perder o interesse pela escola ou esportes

## O que os pais podem fazer?

Muitos adolescentes que cometem ou tentam suicídio têm dado algum tipo de aviso a seus entes queridos alguns antes do tempo. Portanto, é importante que os pais conheçam os sinais de alerta para que os adolescentes quem pode ser suicida pode obter a ajuda de que precisa.

Alguns adultos acham que as crianças que dizem que vão se machucar ou se matar estão "apenas fazendo isso para chamar a atenção." É importante perceber que se os adolescentes forem ignorados ao buscar atenção, pode aumentar a chance de eles se machucarem (ou pior).

Chamando a atenção na forma de visitas ao pronto-socorro, consultas médicas e residências tratamento geralmente não é algo que os adolescentes desejam - a menos que estejam gravemente deprimidos e pensando em suicídio ou pelo menos desejando estar mortos. É importante ver sinais de alerta como sérios, não como "busca de atenção" a serem ignorados.

## Veja e ouça

Fique de olho em um adolescente que está deprimido e retraído. Compreendendo a depressão em adolescentes é muito importante, pois pode ser diferente das crenças comuns sobre depressão. Por exemplo, pode assumir a forma de problemas com amigos, notas, dormir ou ficar mal-humorado e irritado, em vez de tristeza crônica ou choro.

É importante tentar manter as linhas de comunicação abertas e expressar seu preocupação, apoio e amor. Se seu filho adolescente confia em você, mostre que você aceita aqueles preocupações a sério. Uma briga com um amigo pode não parecer grande coisa para você em o esquema mais amplo das coisas, mas para um adolescente pode parecer imenso e desgastante. Está importante não minimizar ou descontar o que seu filho está passando, pois isso pode aumentar sua sensação de desesperança.

Se o seu filho adolescente não se sente confortável para falar com você, sugira um método mais neutro pessoa, como outro parente, um membro do clero, um treinador, um conselheiro escolar ou o médico do seu filho.

## Faça perguntas

Alguns pais relutam em perguntar aos adolescentes se eles estão pensando em suicídio ou se machucando. Alguns temem que, ao perguntar, plantem a ideia do suicídio na cabeça do adolescente.

É sempre uma boa ideia perguntar, embora possa ser difícil. As vezes ajuda a explicar por que você está perguntando. Por exemplo, você pode dizer: "Percebi que você tem falado muito sobre querer estar morto. Você tem pensado sobre tentar se matar? "

## Obtenha ajuda

Se você descobrir que seu filho está pensando em suicídio, procure ajuda imediatamente. Seu médico pode encaminhá-lo a um psicólogo ou psiquiatra, ou o departamento de psiquiatria do seu hospital local pode fornecer uma lista de médicos em sua área. Sua associação local de saúde mental ou médico do condado a sociedade também pode fornecer referências. Em caso de emergência, você pode ligar para (800) SUICIDE.

Se o seu filho adolescente está em uma situação de crise, o pronto-socorro local pode realizar um avaliação psiquiátrica abrangente e encaminhá-lo para os recursos apropriados. E se Você não tem certeza se deve levar seu filho ao pronto-socorro, entre em contato seu médico ou ligue para (800) SUICIDE para obter ajuda.

Se você marcou uma consulta com um profissional de saúde mental, certifique-se para manter a consulta, mesmo se seu filho adolescente diz que está se sentindo melhor ou não querer ir. Os pensamentos suicidas tendem a ir e vir; no entanto, é importante que seu filho adolescente recebe ajuda para desenvolver as habilidades necessárias para diminuir a probabilidade de suicídio pensamentos e comportamentos emergirão novamente se surgir uma crise.

Se o seu filho se recusa a ir à consulta, discuta o assunto com o especialista em saúde mental profissional - e considere participar da sessão e trabalhar com o médico para garantir que seu filho tenha acesso à ajuda necessária. O clínico também pode ser capaz de ajudá-lo a planejar estratégias para que seu filho deseje ajuda.

Lembre-se de que conflitos contínuos entre pais e filhos podem alimentar o fogo por um adolescente que está se sentindo isolado, incompreendido, desvalorizado ou suicida. Obtenha ajuda para levantar os problemas da família e resolvê-los de forma construtiva. Deixe também a saúde mental profissional saber se há histórico de depressão, abuso de substâncias, violência familiar, ou outros estresses em casa, como um ambiente contínuo de críticas.

## Ajudando Adolescentes lidam com perdas

O que você deve fazer se alguém que seu filho conhece, talvez um membro da família, amigo, ou um colega, tentou ou cometeu suicídio? Primeiro, reconheça seu filho muitas emoções. Alguns adolescentes dizem que se sentem culpados - especialmente aqueles que sentem que poderia ter interpretado melhor as ações e palavras do amigo.

Outros dizem que sentem raiva da pessoa que cometeu ou tentou suicídio por tendo feito algo tão egoísta. Ainda outros dizem que não sentem emoções fortes ou não sei como expressar o que sentem. Tranquilize seu filho que não há direito ou a maneira errada de se sentir, e não há problema em falar sobre isso quando ele ou ela se sentir pronto.

Quando alguém tenta o suicídio e sobrevive, as pessoas podem sentir medo ou se sentir desconfortáveis falar com ele ou ela sobre isso. Diga a seu filho para resistir a esse impulso; esta é uma hora quando uma pessoa absolutamente precisa se sentir conectada a outras pessoas.

Muitas escolas abordam o suicídio de um aluno chamando conselheiros especiais para falar com os alunos e ajudá-los a enfrentar. Se o seu filho adolescente está lidando com um amigo ou colega de classe suicídio, incentive-o a usar esses recursos ou a falar com você ou outro adulto de confiança.

## Se você perdeu uma criança devido ao suicídio

Para os pais, a morte de um filho é a perda mais dolorosa que se possa imaginar. Para os pais quem perdeu um filho para o suicídio, a dor e a tristeza podem ser intensificadas. Embora estes sentimentos podem nunca desaparecer completamente, sobreviventes de suicídio podem tomar medidas para começar o processo de cura:

* Mantenha contato com outras pessoas. O suicídio pode ser uma experiência muito isoladora para a sobrevivência membros da família porque muitas vezes os amigos não sabem o que dizer ou como ajudar. Procurar pessoas que o apóiam para conversar sobre seu filho e seus sentimentos. Se aqueles ao redor você parece desconfortável em entrar em contato, inicie a conversa e peça para eles ajuda.
* Lembre-se de que seus outros membros da família também estão sofrendo e que todos expressam luto à sua maneira. Seus outros filhos, em particular, podem tentar lidar com seus dor sozinha, para não sobrecarregá-lo com preocupações adicionais. Esteja lá um pelo outro através das lágrimas, raiva e silêncios - e, se necessário, procure ajuda e apoio juntos.
* Espere que aniversários, aniversários e feriados possam ser difíceis. Importante dias e feriados muitas vezes despertam uma sensação de perda e ansiedade. Naqueles dias, faça o que melhor para as suas necessidades emocionais, quer isso signifique cercar-se de família e amigos ou planejando um dia tranquilo de reflexão.
* Entenda que é normal se sentir culpado e questionar como isso poderia ter aconteceu, mas também é importante perceber que talvez você nunca obtenha as respostas você procura. A cura que ocorre ao longo do tempo vem de chegar a um ponto de perdão - para seu filho e para você.
* Grupos de aconselhamento e apoio podem desempenhar um papel importante em ajudá-lo a perceber Você não está sozinho. Alguns familiares enlutados passam a fazer parte da prevenção do suicídio rede que ajuda pais, adolescentes e escolas a aprender como ajudar a prevenir o futuro tragédias.