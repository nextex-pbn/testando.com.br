---
title: Medicamentos para TDAH
permalink: medicamentos-para-tdah
layout: post.hbs
---

# Medicamentos para TDAH

## O que é medicamento para TDAH?

Depois que uma criança é diagnosticada com TDAH, os médicos pode prescrever medicamentos para tratá-lo. A medicina não cura o TDAH. Mas ajuda a impulsionar a capacidade de prestar atenção, diminuir o ritmo e ter mais autocontrole.

## Por que as crianças precisam de remédios para TDAH?

Nem todas as crianças com TDAH precisam de remédios. Mas a medicina pode ajudar a maioria das crianças com O TDAH mantém o foco por mais tempo, ouve melhor e se inquieta menos.

As crianças também se beneficiam da terapia comportamental para aprender e praticar habilidades como se organizar ou esperar a vez sem interromper. A medicina não é um atalho - as crianças ainda precisam trabalhar para dominar essas habilidades. O benefício da medicina é que ajuda as crianças a manterem o foco enquanto aprendem eles.

## Como funciona o medicamento para TDAH?

Os medicamentos para o TDAH melhoram a atenção, ajudando os produtos químicos normais do cérebro a funcionarem melhor.

Os medicamentos têm como alvo dois produtos químicos cerebrais, dopamina e norepinefrina. Esses produtos químicos afetam a atenção e concentração de uma pessoa.

## Como as pessoas tomam remédios para TDAH?

Crianças e adolescentes com TDAH podem tomar remédios diferentes. Todos os medicamentos para TDAH precisam uma prescrição.

Crianças e adolescentes geralmente tomam remédios para TDAH uma ou duas vezes ao dia, dependendo do remédio.

### Estimulantes

Esses medicamentos incluem metilfenidato (as marcas incluem Ritalina, Concerta, Daytrana, Focalin) e anfetaminas (por exemplo, Adderall, Dexedrine, Vyvanse).

Os estimulantes funcionam assim que uma criança os toma. Quanto tempo eles duram depende do remédio:

* As fórmulas de ação curta duram cerca de 4 horas.
* As fórmulas de ação prolongada permanecem no corpo por cerca de 10 ou 12 horas. Estimulantes de longa ação podem ser úteis para crianças mais velhas e adolescentes que têm uma longa dia de aula e precisa do remédio para se manter concentrado nos deveres de casa ou nas atividades após as aulas.

### Não estimulantes

Esses medicamentos incluem atomoxetina (Strattera), clonidina (Kapvay) e guanfacina (Intuniv). Os não estimulantes podem levar algumas semanas para começar a fazer efeito. Eles trabalham para 24 horas.

Antes de prescrever medicamentos, a equipe de saúde irá perguntar se o seu filho está tomando algum outro medicamento. Isso inclui over-the-counter medicamentos e suplementos (como vitaminas ou fitoterápicos). A equipe de atendimento vai também deseja saber sobre o histórico médico de sua família, especialmente se algum membro da família tem (ou teve) doença cardíaca.

Os médicos geralmente começam prescrevendo uma dose baixa de um medicamento estimulante. Se seu criança está tomando um novo medicamento ou dose de TDAH, o médico vai querer que você observe e veja se o remédio ajuda. O médico mudará a dose e a frequência com que seu filho toma o medicamento com base no quanto ajuda e se seu filho está tendo efeitos colaterais.

As crianças responderam de forma diferente aos medicamentos. Se o primeiro remédio parece não funcionar, mesmo na dose mais alta, então o médico pode tentar um diferente medicamento. Algumas crianças precisam tomar mais de um medicamento para TDAH para obter o melhor resultado.

## Como os pais podem ajudar?

Informe o médico do seu filho se você notar algum efeitos colaterais do medicamento.

Você pode precisar ir a várias consultas com o médico. Pode levar semanas ou meses para encontrar o medicamento e a dose certa para o seu filho. Depois disso, a equipe de atendimento irá quero ver seu filho a cada 3 a 6 meses.

Leve seu filho a todas as visitas de acompanhamento. É importante que a equipe de atendimento verifique a altura, o peso e a pressão arterial do seu filho. O cuidado equipe também procurará efeitos colaterais. Eles podem ajustar a dose do medicamento, especialmente conforme seu filho cresce.

Para ajudar seu filho e prevenir problemas, sempre faça essas coisas ao dar remédio para crianças com TDAH:

* Dê a dose recomendada.
* Dê cada medicamento na hora certa.
* Converse com um médico antes de interromper o medicamento ou alterar a dose.
* Guarde todos os medicamentos em um lugar seguro, onde outras pessoas não possam pegá-los.

A medicina é uma parte do tratamento para o TDAH. O tratamento também inclui terapia, treinamento de pais e escola Apoio, suporte. A medicina funciona melhor quando pais, professores e terapeutas ajudam as crianças aprenda quaisquer habilidades sociais, emocionais e comportamentais que estejam atrasadas por causa do TDAH.

## Existe algum risco?

Como qualquer medicamento, os medicamentos para o TDAH podem ter efeitos colaterais. Nem todo mundo fica do lado efeitos, no entanto.

Os efeitos colaterais mais comuns são perda de apetite e dificuldade para dormir. Outro TDAH os efeitos colaterais dos medicamentos incluem nervosismo, irritabilidade, mau humor, dores de cabeça, dores de estômago, frequência cardíaca acelerada e pressão alta.

Os efeitos colaterais geralmente acontecem nos primeiros dias após o início de um novo medicamento ou tomar uma dose mais elevada. Eles costumam ir embora por conta própria após alguns dias ou semanas, como o corpo se ajusta ao medicamento.

Se um efeito colateral não passar, o médico pode decidir diminuir a dose ou interromper aquele remédio e tente outro. Remédios para TDAH permanecem no corpo apenas por algumas horas, então, os efeitos colaterais desaparecem à medida que o remédio sai do corpo.

A equipe de saúde do seu filho fornecerá mais informações sobre o possível lado efeitos para o medicamento específico que prescrevem. Se você notar algo que preocupa você, fale com o médico do seu filho imediatamente.

Alguns pais não gostam da ideia de dar a seus filhos remédios para TDAH. Mas o o remédio certo pode fazer uma grande diferença para a maioria das crianças. Fale com o médico do seu filho sobre suas preocupações. Pergunte. A equipe de saúde do seu filho pode ajudá-lo a decidir se tentar um medicamento é certo para seu filho.