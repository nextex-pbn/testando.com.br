---
title: Panquecas de Blueberry
permalink: panquecas-de-blueberry
layout: post.hbs
---

# Panquecas de Blueberry

Tempo de preparação: 20 minutos

## O que você precisa:

* 3/4 c. farinha
* 1 colher de sopa. açúcar
* 1 colher de chá fermento em pó
* 1/2 colher de chá sal
* 1 colher de sopa. margarina
* 1 ovo
* 3/4 c. leite
* 1/2 c. mirtilos, lavados e escorridos
* margarina extra para a frigideira

## O que fazer:

1. Em uma tigela grande, peneire a farinha, o açúcar, o fermento e o sal. Conjunto a tigela de lado.
2. Derreta a margarina em uma panela pequena.
3. Quebre o ovo em uma tigela média e adicione o leite e a margarina derretida.
4. Bata a mistura de ovos até que esteja bem misturada.
5. Adicione a mistura de farinha à mistura de ovo. Bata novamente até que ambas as misturas estejam misturados.
6. Coloque o excesso de margarina na panela e leve ao fogão em fogo médio. Está quente o suficiente quando a margarina começa a borbulhar.
7. Use um copo medidor ou uma concha pequena para colocar a massa na assadeira para fazer 4 panquecas. Coloque alguns mirtilos em cima de cada panqueca.
8. Cozinhe suas panquecas em fogo médio até que pequenas bolhas apareçam no topo.
9. Use uma espátula para levantar a borda das panquecas para ver se elas são marrons claras no fundo. Quando estiverem, vire-os com a espátula.
10. Cozinhe por mais alguns minutos até que as panquecas estejam marrom claro do outro lado.
11. Sirva e divirta-se!

## Análise nutricional (por porção):

* 333 calorias
* 11g de proteína
* 9g de gordura
* 52g de carboidratos
* fibra 3g
* 108 mg de colesterol
* 915mg de sódio
* 270 mg de cálcio
* 2,8 mg de ferro

Serve: 2

Tamanho da porção: 2 panquecas

Observação: a análise nutricional pode variar dependendo das marcas de ingredientes usadas.

Sugestão: Coma suas panquecas com açúcar de confeiteiro, canela, xarope de bordo, geléia ou fruta por cima.