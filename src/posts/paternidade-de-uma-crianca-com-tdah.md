---
title: Paternidade de uma criança com TDAH
permalink: paternidade-de-uma-crianca-com-tdah
layout: post.hbs
---

# Paternidade de uma criança com TDAH

## Como o TDAH afeta as crianças

O TDAH faz com que as crianças sejam mais distraídas, hiperativas, e impulsivo do que é normal para sua idade. TDAH torna mais difícil para as crianças se desenvolverem as habilidades que controlam a atenção, o comportamento, as emoções e a atividade. Como um resultado, muitas vezes agem de maneiras difíceis para os pais administrarem.

Por exemplo, como se distraem, crianças com TDAH podem:

* parecem não ouvir
* têm problemas para prestar atenção
* não siga bem as instruções
* precisa de muitos lembretes para fazer as coisas
* mostram pouco esforço nos trabalhos escolares

Por serem hiperativos, crianças com TDAH podem:

* escalar, pular ou praticar violência na hora de brincar calmamente
* ser desorganizado ou bagunçado
* inquietos e parecem incapazes de ficar parados
* apresse-se em vez de perder tempo
* cometer erros descuidados

Por serem impulsivos, crianças com TDAH podem:

* interromper muito
* faça coisas sem pensar
* fazem coisas que não deveriam, embora saibam mais
* dificuldade para esperar, revezar ou compartilhar
* têm explosões emocionais, perdem a paciência ou não têm autocontrole

No início, os pais podem não perceber que esses comportamentos fazem parte do TDAH. Pode parece que uma criança está apenas se comportando mal. O TDAH pode deixar os pais estressados, frustrados, ou desrespeitado.

Os pais podem ficar constrangidos com o que os outros pensam sobre o comportamento de seus filhos. Eles podem se perguntar se eles fizeram algo para causar isso. Mas para crianças com TDAH, as habilidades que controlam a atenção, o comportamento e a atividade não vêm naturalmente.

Quando os pais aprendem sobre o TDAH e quais abordagens parentais funcionam melhor, eles podem ajude as crianças a melhorar e se sair bem.

## O que os pais podem fazer

A paternidade é tão importante quanto qualquer outra parte do tratamento do TDAH. A maneira como os pais responder pode tornar o TDAH melhor - ou pior.

Se seu filho foi diagnosticado com TDAH:

Esteja envolvido. Aprenda tudo o que puder sobre o TDAH. Siga o tratamento o provedor de seu filho recomenda. Guarde todos os compromissos de terapia recomendados. Se seu criança toma remédios para TDAH, dê-lhes no horário recomendado. Não altere a dose sem consultar o seu médico. Guarde os medicamentos do seu filho em um lugar seguro, onde outras pessoas não possam pegá-los.

Saiba como o TDAH afeta seu filho. Cada criança é diferente. Identificar as dificuldades que seu filho tem por causa do TDAH. Algumas crianças precisam melhorar no pagamento atenção e escuta. Outros precisam melhorar a desaceleração. Pergunte ao seu filho terapeuta para dicas e maneiras de ajudar seu filho pratica e melhora.

Concentre-se em ensinar ao seu filho uma coisa de cada vez. Não tente trabalhar em tudo de uma vez. Comece pequeno. Escolha uma coisa para se concentrar. Elogie seu filho esforço.

Disciplina com propósito e calor. Aprenda quais abordagens de disciplina são os melhores para uma criança com TDAH e podem piorar o TDAH. Obtenha treinamento de seu terapeuta da criança sobre as maneiras de responder ao comportamento de seu filho. Crianças com TDAH podem seja sensível às críticas. Corrigir seu comportamento é melhor feito de uma forma que seja encorajando e apoiando em vez de punir.

Defina expectativas claras. Antes de ir a algum lugar, converse com seu criança para explicar como você quer que ele se comporte. Concentre mais energia em ensinar seu filho o que fazer, em vez de reagir ao que não fazer.

Fale sobre isso. Não hesite em falar com seu filho sobre ADHD. Ajude as crianças a entender que ter TDAH não é culpa delas e que podem aprenda maneiras de melhorar os problemas que ele causa.

Passe um tempo especial juntos todos os dias. Arranje tempo para conversar e desfrutar atividades relaxantes e divertidas com seu filho - mesmo que seja apenas por alguns minutos. Dê total atenção ao seu filho. Elogie o comportamento positivo. Não elogie demais, mas comente quando seu filho fizer algo bom. Por exemplo, quando seu filho espera a vez dela, diga: "Você está se revezando tão bem."

Seu relacionamento com seu filho é o mais importante. Crianças com TDAH muitas vezes sentem que estão decepcionando os outros, fazendo coisas erradas ou não sendo "boas". Proteger a autoestima do seu filho sendo paciente, compreensão e aceitação. Deixe seu filho saber que você acredita nele e veja todos os coisas boas sobre ele. Desenvolva resiliência mantendo seu relacionamento com seu filho positivo e amoroso.

Trabalhe com a escola de seu filho. Converse com o professor do seu filho para saber se ele deve fazer um IEP. Reúna-se frequentemente com o professor de seu filho para saber como ele está. Trabalhar com o professor para ajudar seu filho a se sair bem.

Conecte-se com outras pessoas para obter suporte e conscientização. Junte-se a um suporte organização para ADHD como CHADD para obter atualizações sobre tratamento e informações, etc.

Descubra se você tem TDAH. O TDAH geralmente é herdado. Pais (ou outros parentes) de crianças com TDAH podem não saber que também têm. Quando os pais com O TDAH é diagnosticado e tratado, isso os ajuda a dar o melhor de si como pais.