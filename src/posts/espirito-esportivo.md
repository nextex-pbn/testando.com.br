---
title: Espírito esportivo
permalink: espirito-esportivo
layout: post.hbs
---

# Espírito esportivo

## O que é bom espírito esportivo?

Bom espírito esportivo é quando as pessoas que estão jogando ou assistindo a um esporte tratam cada um outro com respeito. Isso inclui jogadores, pais, técnicos e oficiais.

## Como as crianças podem aprender a praticar esportes?

As crianças podem não saber como mostrar que são bons desportistas. Ensine-lhes bons esportes:

* Tenha uma atitude positiva.
* Dê o seu melhor.
* Aperte a mão do outro time antes e depois do jogo.
* Apoie os colegas de equipe dizendo "bom tiro" ou "boa tentativa". Nunca critique um colega de equipe por tentar.
* Aceite ligações e não discuta com funcionários.
* Trate a outra equipe com respeito e nunca provoque ou intimide.
* Siga as regras do jogo.
* Ajude outro jogador que caiu.
* Tenha orgulho de vencer, mas não esfregue isso.
* Aceite uma perda sem reclamar ou dar desculpas.

## Qual é o papel dos pais no bom espírito esportivo?

As crianças aprendem a ser um bom esporte com os adultos em suas vidas, especialmente com seus pais e seus treinadores. Assim que seu filho começa a competir em esportes, é importante ser um bom modelo. E qualquer time em que seu filho joga deve ter treinadores que incentivam o bom espírito esportivo.

Mostre seu espírito esportivo:

* Mantenha os comentários secundários positivos e encorajadores.
* Não fale mal de treinadores, jogadores ou dirigentes. Se você tem uma preocupação séria, fale em particular com o treinador ou oficial.
* Depois de uma competição, não se concentre em quem ganhou ou perdeu. Em vez disso, tente perguntar: "O que você se saiu bem durante o jogo? "" havia algo que você gostaria de ter feito melhor? "Se seu filho acha que havia algo que poderia ter sido melhor, oferecem para trabalhar juntos antes do próximo jogo.
* Aplauda boas jogadas, não importa quem as faz.
* Parabenize os vencedores, mesmo que sejam do outro time.
* Procure exemplos de bom espírito esportivo em atletas profissionais e indique-os para seus filhos. Fale sobre os exemplos ruins também e por que eles o aborrecem.

Se você é um treinador da equipe de seu filho, seja justo e positivo e incentive o jogadores para fazer o mesmo.

Pais e treinadores que enfatizam o bom espírito esportivo ajudam jovens atletas a aprender Respeito pelos outros e autocontrole. Essas habilidades podem ajudá-los a gerenciar outras partes de suas vidas e ajudá-los a se tornarem jovens maduros, respeitosos e bem-sucedidos adultos.