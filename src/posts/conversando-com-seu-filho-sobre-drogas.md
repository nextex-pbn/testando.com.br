---
title: Conversando com seu filho sobre drogas
permalink: conversando-com-seu-filho-sobre-drogas
layout: post.hbs
---

# Conversando com seu filho sobre drogas

Assim como você protege seus filhos contra doenças como o sarampo, você pode ajudar a "imunizar" contra o uso de drogas, fornecendo-lhes os fatos antes que se encontrem em uma situação de risco.

Quando os filhos não se sentem à vontade para conversar com os pais, procuram respostas em outro lugar, mesmo que suas fontes não sejam confiáveis. E as crianças que não estão devidamente informadas estão maior risco de se envolver em comportamentos inseguros e experimentar drogas.

Os pais instruídos sobre os efeitos do uso de drogas e aprender os fatos podem dê a seus filhos informações corretas e esclareça quaisquer equívocos. Você é um papel modelos para seus filhos e suas opiniões sobre álcool, tabaco e drogas podem influenciar fortemente como eles pensam sobre eles. Portanto, faça da conversa sobre drogas uma parte de sua conversas sobre saúde e segurança.

## Da pré-escola aos 7 anos

Antes de ficar nervoso ao falar com crianças pequenas, anime-se. Você provavelmente já lançou as bases para uma discussão. Por exemplo, sempre que você dá febre medicamento ou antibiótico para o seu filho, você pode discutir por que e quando esses medicamentos Deveria ser dado. Este também é um momento em que seu filho provavelmente prestará atenção a seu comportamento e orientação.

Aproveite os "momentos ensináveis" agora. Se você ver um personagem em um filme ou na tv com um cigarro, falar sobre fumo, nicotina vício, e o que fumar faz ao corpo de uma pessoa. Isso pode levar a uma discussão sobre outras drogas e como elas podem causar danos.

Mantenha o tom dessas discussões calmo e use termos que seu filho possa entender. Seja específico sobre os efeitos das drogas: como elas fazem uma pessoa se sentir, o risco de overdose e os outros danos a longo prazo que podem causar. Para dar aos seus filhos estes fatos, talvez você precise fazer uma pequena pesquisa.

## De 8 a 12 anos

Conforme seus filhos crescem, você pode começar a conversar com eles perguntando o que eles pense sobre drogas. Ao fazer as perguntas de uma forma não crítica e aberta, você está mais probabilidade de obter uma resposta honesta.

Lembre-se de mostrar aos seus filhos que você está ouvindo e prestando muita atenção suas preocupações e perguntas.

Crianças dessa idade geralmente ainda desejam falar abertamente com os pais sobre situações delicadas assuntos. Iniciar um diálogo agora ajuda a manter a porta aberta conforme as crianças ficam mais velhas e menos inclinado a compartilhar seus pensamentos e sentimentos.

Mesmo que suas perguntas não resultem em uma discussão imediatamente, você receberá crianças pensando sobre o assunto. Mostre a eles que você está disposto a discutir o assunto e ouvir o que eles têm a dizer. Então, eles podem estar mais dispostos a vir até você para ajuda no futuro.

Notícias, como o uso de esteróides em esportes profissionais, podem ser trampolins para o casual conversas sobre eventos atuais. Use essas discussões para dar informações aos seus filhos sobre os riscos das drogas.

## De 13 a 17 anos

Crianças dessa idade provavelmente conhecerão outras crianças que usam álcool ou drogas e terão amigos que dirigem. Muitos ainda estão dispostos a expressar seus pensamentos ou preocupações com pais sobre isso. Eles podem fazer perguntas mais específicas sobre drogas.

Use essas conversas não apenas para entender os pensamentos e sentimentos de seu filho, mas também para falar sobre os perigos de dirigir sob a influência de drogas ou álcool. Fale sobre as questões legais - pena de prisão e multas - e a possibilidade que eles ou outra pessoa pode ser morto ou gravemente ferido.

Considere fazer um contrato escrito ou verbal sobre as regras sobre sair ou usar o carro. Você pode prometer buscar seus filhos a qualquer hora (mesmo às 2 da manhã!), Sem perguntas perguntado, se eles ligam para você quando o responsável pela direção estiver bebendo ou usando drogas.

O contrato também pode detalhar outras situações: por exemplo, se você descobrir que alguém bebeu ou usou drogas em seu carro enquanto seu filho ou filha estava atrás do roda, você pode suspender os privilégios de direção por 6 meses. Discutindo tudo disso com seus filhos desde o início, você elimina surpresas e faz suas expectativas claro.

## Estabelecendo uma base sólida

Nenhum pai, filho ou família está imune aos efeitos das drogas. Qualquer criança pode acabar em apuros, mesmo aqueles que fizeram um esforço para evitá-los e mesmo quando eles receberam a orientação adequada de seus pais.

No entanto, certos grupos de crianças podem ser mais propensos a usar drogas do que outros. Crianças quem tem amigos que usam drogas provavelmente também experimentará drogas. Aqueles que se sentem socialmente isolado por qualquer motivo pode recorrer às drogas.

Portanto, é importante conhecer os amigos do seu filho - e os pais deles. Estar envolvido na vida de seus filhos. Se a escola de seu filho tem um programa antidrogas, envolva-se. Você pode aprender algo! Preste atenção em como seus filhos estão se sentindo e deixe-os saiba que você está disponível e disposto a ouvir sem fazer julgamentos. Reconhecer quando seus filhos estão passando por momentos difíceis para que você possa fornecer o apoio eles precisam ou procuram cuidados adicionais, se necessário.

A encenação pode ajudar seu filho a desenvolver estratégias para recusar drogas, se ele são oferecidos. Elabore cenários possíveis que eles possam encontrar. Ajudando-os a construir frases e respostas para dizer não os prepara para saber como responder antes de mesmo nessa situação.

Um ambiente familiar acolhedor e aberto - onde as crianças podem falar sobre seus sentimentos, onde suas realizações são elogiadas e onde sua auto-estima é aumentada - incentiva as crianças a apresentarem suas perguntas e preocupações. Quando censurado em em suas próprias casas, as crianças vão a outros lugares para encontrar apoio e respostas para seus mais importantes perguntas.

Faça com que conversar e conversar com seus filhos faça parte do seu dia. Encontrar tempo para fazer coisas que você gosta em família ajuda todos a ficarem conectados e manter uma comunicação aberta.

Se você está procurando mais recursos para você ou seu filho, certifique-se também converse com seu médico.