---
title: Guia dos pais para sobreviver na adolescência
permalink: guia-dos-pais-para-sobreviver-na-adolescencia
layout: post.hbs
---

# Guia dos pais para sobreviver na adolescência

Você sobreviveu às 2 da manhã alimentações, acessos de raiva de crianças e a volta às aulas Os Azuis. Então, por que a palavra "adolescente" está causando tanta preocupação?

Quando você considera que a adolescência é um período de intenso crescimento, não só fisicamente, mas emocionalmente e intelectualmente, é compreensível que seja um momento de confusão e agitação para muitas famílias.

Apesar das percepções negativas de alguns adultos sobre os adolescentes, eles costumam ser enérgicos, pensativo e idealista, com um profundo interesse no que é justo e certo. Então, embora pode ser um período de conflito entre pais e filhos, a adolescência também é um hora de ajudar as crianças a se tornarem indivíduos distintos em que se tornarão.

## Compreendendo os anos da adolescência

Então, quando começa a adolescência? Todo mundo é diferente - existem aqueles que começam cedo, chegadores atrasados, desenvolvedores rápidos e produtores lentos, mas estáveis. Em outras palavras, há uma ampla gama do que é considerado normal.

Mas é importante fazer uma distinção (um tanto artificial) entre puberdade e adolescência. A maioria de nós pensa na puberdade como o desenvolvimento das características sexuais adultas: seios, períodos menstruais, pelos pubianos e pelos faciais. Estes são certamente os sinais mais visíveis da puberdade e a idade adulta iminente, mas crianças que estão mostrando mudanças físicas (entre as idades de 8 e 14 ou mais) também pode estar passando por um monte de mudanças que não são prontamente visto de fora. Essas são as mudanças da adolescência.

Muitas crianças anunciam o início da adolescência com uma mudança dramática de comportamento em torno de seus pais. Eles estão começando a se separar da mãe e do pai e se tornarem mais independente. Ao mesmo tempo, as crianças desta idade estão cada vez mais conscientes de como os outros, especialmente seus colegas, vê-los e estão tentando desesperadamente se encaixar. Seus pares muitas vezes se tornam muito mais importantes do que os pais na tomada de decisões.

As crianças muitas vezes começam a "experimentar" aparências e identidades diferentes e ficam muito ciente de como eles diferem de seus pares, o que pode resultar em episódios de angústia e conflito com os pais.

## Batendo cabeças

Um dos estereótipos comuns da adolescência é o adolescente rebelde e selvagem continuamente em desacordo com a mãe e o pai. Embora possa ser o caso de algumas crianças e este seja uma época de altos e baixos emocionais, esse estereótipo certamente não é representativo da maioria dos adolescentes.

Mas o objetivo principal da adolescência é alcançar a independência. Para fazer isso, adolescentes devem começar a se afastar de seus pais - especialmente o pai que eles são os mais próximos de. Pode parecer que os adolescentes estão sempre em conflito com os pais ou não quero ficar perto deles como costumavam ser.

Conforme os adolescentes amadurecem, eles começam a pensar de forma mais abstrata e racional. Eles estão formando seu código moral. E os pais de adolescentes podem descobrir que as crianças que já foram dispostos a se conformar para agradá-los, de repente começarão a se afirmar - e suas opiniões - fortemente e se rebelando contra o controle dos pais.

Você pode precisar avaliar com atenção quanto espaço você dá a seu filho para ser um indivíduo e pergunte a si mesmo: "Sou um pai controlador?", "Eu ouço meu filho? "e" Eu permito que as opiniões e gostos do meu filho sejam diferentes dos meus? "

## Dicas para pais durante a adolescência

Procurando um roteiro para encontrar seu caminho ao longo desses anos? Aqui estão algumas dicas:

## Eduque-se

Leia livros sobre adolescentes. Pense em sua própria adolescência. Lembre-se de suas lutas com acne ou seu constrangimento em desenvolver precocemente - ou tarde. Espere algumas mudanças de humor em seu filho normalmente ensolarado e esteja preparado para mais conflito conforme ele ou ela amadurece como indivíduo. Pais que sabem o que está por vir pode lidar melhor com isso. E quanto mais você sabe, melhor se prepara.

## Converse com crianças desde cedo e com frequência

Começar a falar sobre menstruação ou sonhos molhados depois que eles já começaram é começando tarde demais. Responda às primeiras perguntas que as crianças têm sobre corpos, como as diferenças entre meninos e meninas e de onde vêm os bebês. Mas não sobrecarregue com informações - basta responder às suas perguntas. Se você não sabe as respostas, obtenha-os de alguém que os faça, como um amigo de confiança ou seu pediatra.

Você conhece seus filhos. Você pode ouvir quando seu filho começa a contar piadas sobre sexo ou quando atenção à aparência pessoal está aumentando. Este é um bom momento para começar com suas próprias perguntas, como:

* Você está notando alguma mudança em seu corpo?
* Você está tendo algum sentimento estranho?
* Você fica triste às vezes e não sabe por quê?

Um exame físico anual é uma ótima hora para falar sobre isso. Um médico pode dizer ao seu pré-adolescente - e você - o que esperar nos próximos anos. Um exame pode ser um ponto de partida para uma boa discussão entre pais e filhos. Quanto mais tarde você espera ter essas conversas, é mais provável que seu filho tenha ideias erradas ou ficar envergonhado ou com medo de mudanças físicas e emocionais.

E quanto mais cedo você abrir as linhas de comunicação, melhores serão suas chances de mantendo-os abertos durante a adolescência. Dê ao seu filho livros escritos sobre puberdade para crianças passando por isso. Compartilhe memórias de sua própria adolescência. Não há nada como saber que a mãe ou o pai também passaram por isso, para deixar as crianças mais à vontade.

## Coloque-se no lugar do seu filho

Pratique a empatia ajudando seu filho a entender que é normal ser um pouco preocupado ou constrangido, e que é normal se sentir adulto por um minuto e gostar uma criança no próximo.

## Escolha suas batalhas

Se os adolescentes quiserem pintar o cabelo, pinte as unhas de preto ou use roupas descoladas roupas, pense duas vezes antes de se opor. Os adolescentes querem chocar seus pais e é muito melhor deixá-los fazer algo temporário e inofensivo; salve suas objeções para coisas que realmente importam, como tabaco, Drogas e álcool, ou mudanças permanentes em sua aparência.

Pergunte por que seu filho adolescente quer se vestir ou ter uma determinada aparência e tente entender como seu filho adolescente está se sentindo. Você também pode querer discutir como os outros podem percebê-los se forem diferentes, ajude seu filho a entender como ele pode ser visto.

## Defina as expectativas

Os adolescentes podem parecer infelizes com as expectativas que seus pais depositam sobre eles. Ainda assim, eles geralmente entendem e precisam saber que seus pais se preocupam o suficiente com eles devem esperar certas coisas, como boas notas, comportamento aceitável e persistência às regras da casa. Se os pais tiverem expectativas adequadas, os adolescentes provavelmente tentarão para conhecê-los. Sem expectativas razoáveis, seu filho pode sentir que você não se importa com ele ou ela.

## Informe seu filho adolescente - e mantenha-se informado

A adolescência costuma ser uma época de experimentação e, às vezes, de experimentação inclui comportamentos de risco. Não evite os assuntos de sexo e drogas, álcool ou tabaco usar. Discutir tópicos difíceis abertamente com as crianças antes de serem expostos para eles, na verdade, torna-se mais provável que ajam com responsabilidade quando chegar o momento vem. Compartilhe seus valores familiares com seu filho adolescente e fale sobre o que você acredita ser certo e errado, e por quê.

Conheça os amigos do seu filho - e conheça os pais dos amigos deles. Comunicação regular entre os pais pode ajudar muito na criação de um ambiente seguro para todos os adolescentes em um grupo de pares. Os pais podem ajudar uns aos outros a acompanhar as atividades dos filhos sem fazendo com que as crianças sintam que estão sendo observadas.

## Conheça os sinais de alerta

Certas mudanças são normais durante a adolescência. Mas muito drástico ou uma mudança duradoura na personalidade ou comportamento pode sinalizar um problema real - o tipo que precisa de ajuda profissional. Fique atento a estes sinais de alerta:

* ganho ou perda extrema de peso
* problemas de sono
* mudanças rápidas e drásticas na personalidade
* mudança repentina de amigos
* faltar às aulas com frequência
* notas decrescentes
* falar ou mesmo fazer piadas sobre suicídio
* sinais de uso de tabaco, álcool ou drogas
* desentendimentos com a lei

Qualquer outro comportamento impróprio que dure mais de 6 semanas pode ser um sinal de problemas subjacentes também. Você pode esperar uma ou duas falhas no comportamento de seu filho ou notas durante este tempo, mas seu aluno A / B não deveria ser reprovado de repente, e seu filho normalmente extrovertido não deveria de repente tornar-se constantemente retraído. Seu medico ou um conselheiro, psicólogo ou psiquiatra local pode ajudá-lo a encontrar aconselhamento adequado.

## Respeite a privacidade das crianças

Alguns pais, compreensivelmente, têm muita dificuldade com este. Eles podem sentir que tudo o que seus filhos fazem é problema deles. Mas para ajudar seu filho a se tornar um jovem adulto, você precisará garantir alguma privacidade. Se você notar sinais de alerta de problemas, então você pode invadir a privacidade de seu filho até chegar ao cerne do problema. Mas, caso contrário, é uma boa ideia recuar.

Em outras palavras, o quarto, as mensagens de texto, os e-mails e as ligações do seu adolescente devem seja privado. Você também não deve esperar que seu filho compartilhe todos os pensamentos ou atividades com você o tempo todo. Claro, por razões de segurança, você deve sempre saber onde os adolescentes vão, quando vão voltar, o que estão fazendo e com quem, mas você não precisa saber todos os detalhes. E você definitivamente não deve esperar ser convidado junto!

Comece com confiança. Diga a seu filho que você confia nele, mas se a confiança quebrado, ele ou ela terá menos liberdade até que seja reconstruído.

## Monitore o que as crianças veem e leem

Programas de TV, revistas e livros, a Internet - as crianças têm acesso a toneladas de informações. Esteja ciente do que o seu assiste e lê. Não tenha medo de definir limites na quantidade de tempo gasto na frente do computador ou da TV. Saiba o que são aprendendo com a mídia e com quem eles podem se comunicar online.

Os adolescentes não devem ter acesso ilimitado à TV ou à Internet em particular - estas deveriam ser atividades públicas. O acesso à tecnologia também deve ser limitado após certas horas (por exemplo, 22 horas ou mais) para estimular um sono adequado. Está não é absurdo ter celulares e computadores fora dos limites após um certo tempo.

## Faça regras apropriadas

A hora de dormir para um adolescente deve ser apropriada à idade, assim como era quando seu filho era um bebê. Os adolescentes ainda precisam de cerca de 8 a 9 horas de dormir. Incentive seu filho a seguir um horário de sono que atenda a esses necessidades.

Recompense seu filho adolescente por ser confiável. Ele ou ela manteve o horário de 22 horas regredir nos fins de semana? Mova-o para 22h30. E um adolescente sempre tem que ir junto com a família passeios? Incentive um período razoável de tempo com a família, mas seja flexível. Não fique insultado quando seu filho em crescimento nem sempre quer estar com você. Pense bem: Você provavelmente se sentiu da mesma forma em relação à sua mãe e ao seu pai.

## Isso nunca vai acabar?

À medida que as crianças progridem na adolescência, você notará uma desaceleração dos agudos e baixos da adolescência. E, eventualmente, eles se tornarão independentes, responsáveis, jovens adultos comunicativos.

Portanto, lembre-se do lema de muitos pais com adolescentes: vamos passar por isso juntos, e sairemos disso - juntos!