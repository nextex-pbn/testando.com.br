---
title: Coronavírus (COVID-19): Cuidando de Crianças com Necessidades Especiais de Saúde
permalink: coronavirus-covid-19-cuidando-de-criancas-com-necessidades-especiais-de-saude
layout: post.hbs
---

# Coronavírus (COVID-19): Cuidando de Crianças com Necessidades Especiais de Saúde

Durante a pandemia de coronavírus (COVID-19), crianças com necessidades especiais de saúde ainda precisam de rotina Cuidado. Mas como eles conseguirão isso pode mudar. Algumas visitas médicas estão sendo feitas pelo telefone ou pela telessaúde. Aqui está mais sobre o que esperar durante este tempo desafiador.

## Crianças com necessidades especiais de saúde correm mais risco com o COVID-19?

Descobriu-se que poucas crianças têm a doença. Portanto, é difícil para os especialistas ainda saberem como COVID-19 pode afetar crianças com problemas de saúde contínuos. Para ficar seguro e evitar infecção, siga o conselho do Centros para Controle e Prevenção de Doenças (CDC) e outros especialistas em saúde. Estar certifique-se de:

* Mantenha-se a 2 metros de distância das pessoas com quem você não mora (chamadas de pessoas físicas ou sociais distanciamento).
* Use uma cobertura facial ou máscara quando sair e não é capaz de se distanciar dos outros.
* Lave bem as mãos e com frequência.

## Devo levar meu filho a consultas planejadas de assistência médica?

Fale com o seu médico. Muitos consultórios médicos estão agendando pessoalmente visitas. Algumas marcações podem ser visitas de vídeo (telessaúde). Se o seu filho tem um presencial visite, converse com o escritório sobre como eles estão mantendo crianças e famílias seguras. O médico a equipe usará máscaras. Você e seu filho também devem usá-los. Tranquilizar seu filho que eles são seguros e não há razão para ter medo.

## Devo continuar a dar medicamentos diários regulares ao meu filho?

Sim. Continue dando todos os medicamentos diários regulares, a menos que a equipe de atendimento lhe diga para Pare. A melhor maneira de manter as crianças saudáveis ​​é cuidar de sua condição de saúde. Estar em sua melhor saúde tornará mais fácil para as crianças melhorarem com o coronavírus se eles pegarem a infecção.

Tenha pelo menos 2–3 semanas de medicamentos e outros suprimentos necessários à mão em todas as vezes. Trabalhe com seu seguro e empresas de suprimentos médicos domésticos para solicitar recargas bem antes que acabem.

## Meu filho deve ainda receber quimioterapia ou outros medicamentos que afetam o sistema imunológico Sistema?

Sim. Continue a quimioterapia ou qualquer outro medicamentos que podem enfraquecer o sistema imunológico até você falar com a equipe de atendimento. Só interrompa os medicamentos se a equipe de atendimento assim o indicar.

Siga estas diretrizes para prevenir a infecção:

* Mantenha 6 pés de distância das pessoas com quem você não mora (chamado de distanciamento físico ou social)
* Use uma cobertura facial ou máscara quando estiver fora e não puder se distanciar dos outros.
* Lave bem as mãos e com frequência.

## Os prestadores de cuidados de saúde ao domicílio ainda devem vir à sua casa? Como posso manter todos Seguro?

Os prestadores de cuidados de saúde ao domicílio desempenham um papel importante no cuidado de algumas crianças com necessidades especiais de saúde. Converse com sua equipe de atendimento sobre os prestadores de cuidados domiciliares do seu filho. Pergunte se eles são todos necessários agora. Qualquer provedor de cuidados que esteja doente não deve ajudar a cuidar de seu filho.

Peça aos profissionais de saúde que usem uma cobertura facial ou máscara enquanto estiver em sua casa e quando cuidando de seu filho. Eles devem lavar bem as mãos por pelo menos 20 segundos quando eles chegam, e muitas vezes enquanto prestam cuidados. Configure estações de lavagem de mãos com muito sabonete e toalhas de papel. Mantenha desinfetante para as mãos por perto também. Poste sinais como lembretes para limpar as mãos.

## E se meu filho ficar doente? Poderia ser COVID-19?

Primeiro, ligue para a equipe de saúde. Eles conhecem o histórico de saúde do seu filho e irão saiba se seu filho corre algum risco especial. O médico vai perguntar como seu filho está e se estiveram perto de alguém com coronavírus conhecido ou suspeito. Do seu médico o escritório dirá o que fazer a seguir e se você precisa de uma visita pessoal.

Se o seu filho tem cateter central, está em quimioterapia, ou tem um sistema imunológico fraco, siga as instruções usuais para o que fazer para a febre. Normalmente, isso significa ligar a equipe de atendimento imediatamente.

## Como podemos lidar com o estresse enquanto cuidamos de nosso filho com necessidades especiais?

* Controle o que você pode. Estruture seu dia com uma rotina.
* Tente se concentrar em estar no momento com atenção plena, imagens guiadas (procure vídeos online) ou ioga.
* Dê um passeio lá fora. Use uma máscara se não conseguir manter uma distância segura dos outros. Ou faça coisas ativas lá dentro.
* Mantenha contato com a equipe de cuidados do seu filho para não se sentir sozinho.
* Vá com calma. Faça o seu melhor para gerenciar o que puder. As coisas vão melhorar.
* Se você se sentir estressado, peça apoio da família. Uma chamada telefônica ou uma visita de vídeo podem percorrer um longo caminho. Se você está se sentindo oprimido, ligue para o 911. Você também pode ligar para o Disaster Socorro Helpline em 1-800-985-5990 ou vá ao site deles.