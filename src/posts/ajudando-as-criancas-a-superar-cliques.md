---
title: Ajudando as Crianças a Superar Cliques
permalink: ajudando-as-criancas-a-superar-cliques
layout: post.hbs
---

# Ajudando as Crianças a Superar Cliques

## O que é uma Clique?

A amizade é uma parte importante do desenvolvimento das crianças. Ter amigos os ajuda seja independente além da família e os prepare para relacionamentos mútuos e de confiança esperamos que se estabeleçam como adultos.

Os grupos de amigos são diferentes dos cliques em alguns aspectos importantes.

* Grupos de amigos se formam com base em interesses, esportes, atividades comuns, classes, vizinhanças ou mesmo conexões familiares. Em grupos de amigos, membros são livres para se socializar e conviver com outras pessoas fora do grupo sem se preocupar sobre ser expulso. Eles podem não fazer tudo juntos - e tudo bem.
* Os grupos às vezes se formam em torno de interesses comuns, mas o social a dinâmica é muito diferente. Cliques geralmente são rigidamente controlados por líderes que decida quem está "dentro" e quem está "fora". As crianças do grupo fazem muitas coisas juntas. Alguém que tem um amigo fora do grupo pode ser rejeitado ou ridicularizado.

Os membros do grupo geralmente seguem as regras do líder, seja usando roupas específicas roupas ou fazer certas atividades. Cliques geralmente envolvem muitas regras - implícito ou claramente declarado - e intensa pressão para segui-los.

Crianças em panelinhas geralmente se preocupam se continuarão sendo populares ou se farão ser descartado por fazer ou dizer a coisa errada ou por não se vestir de determinada maneira. Isso pode criar muita pressão. As crianças podem ser pressionadas a assumir riscos como roubar, pregar peças ou intimidar outras crianças na ordem para ficar na camarilha. As crianças também podem ser pressionadas a comprar roupas caras ou envolver-se em fofocas e provocações online.

As aulas costumam ser mais intensas no ensino fundamental e no ensino médio, mas têm problemas com cliques podem começar já na 4ª e 5ª séries.

## Quando os cliques causam problemas

Para a maioria das crianças, a pré-adolescência e a adolescência são uma época para descobrir como desejam para se encaixar e como eles querem se destacar. É natural que as crianças ocasionalmente sintam inseguro; anseia por ser aceito; e sair com as crianças que parecem mais atraentes, legal ou popular.

Mas cliques podem causar problemas duradouros quando:

* as crianças se comportam de uma forma que sentem que estão em conflito ou sabem que é errado para agradar um líder e permaneça no grupo
* um grupo se torna um clique anti-social ou uma gangue que tem regras prejudiciais, como como perda de peso ou intimidação com base na aparência, deficiência, raça ou etnia
* uma criança é rejeitada por um grupo e se sente condenada ao ostracismo e sozinha

## Como os pais podem ajudar?

À medida que as crianças navegam por amizades e panelinhas, os pais podem fazer muitas coisas Apoio, suporte. Se o seu filho parece chateado, ou de repente passa um tempo sozinho quando geralmente muito social, pergunte sobre isso.

Aqui estão algumas dicas:

* Fale sobre suas próprias experiências. Compartilhe suas próprias experiências de escola - as panelinhas existem há muito tempo!
* Ajude a colocar a rejeição em perspectiva. Lembre seu filho dos tempos ele ou ela ficou zangado com os pais, amigos ou irmãos - e com que rapidez as coisas podem mudar.
* Lance alguma luz sobre a dinâmica social. Reconheça que as pessoas são muitas vezes julgado pela forma como uma pessoa se parece, age ou se veste, mas que muitas vezes as pessoas agem significa e rebaixa os outros porque não têm autoconfiança e tentam encobrir isso mantendo o controle.
* Encontre histórias com as quais possam se identificar. Muitos livros, programas de TV e filmes retratar estranhos triunfando diante da rejeição e enviar mensagens fortes sobre a importância de ser fiel à sua própria natureza e o valor de ser um bom amigo, mesmo em face de situações sociais difíceis. Para crianças em idade escolar, livros como "Blubber" por Judy Blume ilustram como os cliques podem mudar rapidamente. Crianças mais velhas e adolescentes podem relacionam-se a filmes como "Meninas malvadas", "Angus", "Clube do café da manhã" e "Sem noção".
* Promova amizades fora da escola. Envolva as crianças em atividades extracurriculares atividades (se ainda não o forem) - aulas de arte, esportes, artes marciais, cavalo equitação, estudo da língua - qualquer atividade que lhes dê a oportunidade de criar outro grupo social e aprender novas habilidades.

Se seu filho faz parte de um grupo e um dos filhos está provocando ou rejeitando os outros, é importante resolver isso imediatamente. Com programas de TV populares de concursos de talentos a reality shows que glorificam o comportamento rude, é uma batalha difícil para as famílias promova gentileza, respeito e compaixão.

Discuta o papel do poder e do controle nas amizades e tente chegar ao coração de por que seu filho se sente compelido a estar nessa posição. Discuta quem está dentro e quem está fora, e o que acontece quando as crianças estão fora (elas são ignoradas, rejeitadas, intimidadas?). Desafie as crianças a pensar e falar se têm orgulho da maneira como agem escola.

Peça a opinião de professores, orientadores ou outros funcionários da escola sobre o que está acontecendo dentro e fora da classe. Eles podem ser capazes de falar sobre quaisquer programas que a escola tenha para lidar com panelinhas e ajudar crianças com diferenças se dar bem.

## Incentivo a amizades saudáveis ​​

Aqui estão algumas maneiras de incentivar as crianças a ter amizades saudáveis ​​e não apanhados em cliques:

* Encontre o ajuste certo - não se encaixe apenas nele. Incentive as crianças a pensar sobre o que valorizam e em que estão interessadas, e como essas as coisas se encaixam no grupo. Faça perguntas como: Qual é o principal motivo pelo qual você deseja fazer parte do grupo? Que compromissos você terá que fazer? Vale a pena? o que você faria se o líder do grupo insistisse que você agisse mal com as outras crianças ou fizesse algo você não quer fazer? Quando isso muda de diversão e brincadeira para provocação e intimidação?
* Atenha-se ao que gosta. Se o seu filho sempre gostou de brincar piano, mas de repente quer abandoná-lo porque não é considerado "legal", discutir maneiras de ajudar a resolver isso. Incentive as crianças a participarem de atividades de que gostam e isso aumenta sua confiança.
* Mantenha os círculos sociais abertos e diversificados. Incentive as crianças a serem amigas com pessoas de quem gostam e de diferentes contextos, origens, idades e interesses. Modele você mesmo o máximo que puder com diferentes idades e tipos de amigos e conhecidos.
* Fale e levante-se. Se eles estão se sentindo preocupados ou pressionados pelo que está acontecendo nas panelinhas, incentive seus filhos a se defenderem ou outros que estão sendo expulsos ou intimidados. Incentive-os a não participar de qualquer coisa que pareça errada, seja uma piada ou falar sobre pessoas por trás suas costas.
* Assuma a responsabilidade por suas próprias ações. Incentive a sensibilidade para os outros e não apenas acompanhando um grupo. Lembre às crianças que um verdadeiro amigo respeita suas opiniões, interesses e escolhas, não importa o quão diferentes sejam. Reconhecer que pode ser difícil se destacar, mas, em última análise, as crianças são responsáveis pelo que eles dizem e fazem.

Lembre-se de fornecer também a perspectiva geral. Por mais difícil que os cliques possam ser para lidar agora, as coisas podem mudar rapidamente. O que é mais importante é fazer verdadeiros amigos - pessoas em quem podem confiar, rir e confiar. E o real segredo para ser "popular" - no verdadeiro sentido da palavra - é para eles ser o tipo de amigo que gostariam de ter: respeitoso, justo, solidário, atencioso, confiável e gentil.