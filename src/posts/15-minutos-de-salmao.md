---
title: 15 minutos de salmão
permalink: 15-minutos-de-salmao
layout: post.hbs
---

# 15 minutos de salmão

Tempo de preparação: 15 minutos

## O que você precisa:

* 2 6 onças bifes de salmão (uma porção normal de carne e peixe é de 3 onças)
* spray de manteiga ou óleo vegetal ou spray de canola sem gordura

## O que fazer:

1. Pré-aqueça o frango. Cubra a assadeira com papel alumínio e levemente unte o papel alumínio com manteiga ou óleo vegetal em spray.
2. Coloque os bifes na frigideira e grelhe o salmão por 5 minutos. Vire os bifes com cuidado e grelhe o outro lado por 4 a 5 minutos ou até que o peixe esteja firme, mas ainda elástico e um pouco translúcido no centro.
3. Sirva imediatamente coberto com o molho de sua escolha.

## Análise nutricional (por porção):

* 350 calorias
* 34g de proteína
* 20g de gordura
* 114 mg de sódio
* 0mg de cálcio

Serve: 2

Tamanho da porção: 1 bife de salmão

Observação: a análise nutricional pode variar dependendo das marcas de ingredientes usadas.

Variações e sugestões: esta receita pode ser facilmente duplicada para servir 4. O salmão congela bem se preparado de forma adequada. Congele cada peça separadamente, embrulhada primeiro em plástico e depois em folha de alumínio para preservar a frescura e facilitar a descongelação. Sirva com molho de laranja e gengibre.