---
title: Orientação sexual
permalink: orientacao-sexual
layout: post.hbs
---

# Orientação sexual

A adolescência é o início da atração sexual. Acontece devido às mudanças hormonais da puberdade. Essas mudanças envolvem tanto o corpo quanto a mente - então, apenas pensar em alguém atraente pode causar excitação física.

Esses novos sentimentos podem ser intensos, confusos, às vezes até opressores. Adolescentes estão começando a descobrir o que significa ser atraído romanticamente e fisicamente para outros. E reconhecer a orientação sexual de uma pessoa faz parte desse processo.

## O que é orientação sexual?

O termo orientação sexual se refere ao gênero (ou seja, masculino ou feminino) ao qual uma pessoa é atraída. Existem vários tipos de orientação sexual que são comumente descrito:

* Heterossexual (heterossexual). Pessoas que são heterossexuais são românticas e fisicamente atraído por membros do sexo oposto: os homens são atraídos pelas mulheres, e as mulheres são atraídas por homens. Os heterossexuais costumam ser chamados de "heterossexuais".
* Homossexual (gay ou lésbica). Pessoas que são homossexuais são românticas e fisicamente atraído por pessoas do mesmo sexo: as mulheres são atraídas por outros mulheres; os preguiçosos são atraídos por outros machos. Homossexuais (sejam homens ou mulheres) são freqüentemente chamados de "gays". Mulheres gays também são chamadas de lésbicas.
* Bissexual. Pessoas que são bissexuais são romanticamente e fisicamente atraído por membros de ambos os sexos.

## Escolhemos nossa orientação?

Ser heterossexual, gay ou bissexual não é algo que uma pessoa pode escolher ou escolher mudar. Na verdade, as pessoas não escolhem sua orientação sexual não mais do que escolhem a altura ou a cor dos olhos. Estima-se que cerca de 10% de pessoas são gays. Os gays são representados em todas as esferas da vida, em todas as nacionalidades, origens étnicas e em todos os grupos sociais e econômicos.

Ninguém entende exatamente o que determina a orientação sexual de uma pessoa, mas é provavelmente explicado por uma variedade de fatores biológicos e genéticos. Médico especialistas e organizações como a American Academy of Pediatrics (AAP) e a American Psychological Association (APA) vê a orientação sexual como parte da natureza. Ser gay também não é considerado um transtorno mental ou anormalidade.

Apesar dos mitos e equívocos, não há evidências de que ser gay é causado por experiências na primeira infância, estilos parentais ou a maneira como alguém é gerado.

Esforços para transformar gays em heterossexuais (às vezes chamada de "terapia de conversão") provaram ser ineficazes e podem ser prejudiciais. Profissionais de saúde e saúde mental acautele contra quaisquer esforços para mudar a orientação sexual de uma pessoa.

## Em Com que idade as crianças "sabem"?

Saber a orientação sexual de uma pessoa - hetero ou gay - costuma ser algo que as crianças ou adolescentes reconhecem com pouca dúvida desde a mais tenra idade. Alguns adolescentes gays dizem que tiveram paixões pelo mesmo sexo na infância, assim como seus pares heterossexuais teve paixões do sexo oposto.

No ensino médio, quando entram na adolescência, muitos adolescentes gays já reconhecem seus orientação sexual, independentemente de a terem ou não revelado a outras pessoas. Aqueles que não percebiam que eram gays no início, costumam dizer que sempre se sentiram diferentes de seus colegas, mas não sabia exatamente por quê.

Tomar conhecimento - e aceitar - a orientação sexual de alguém pode levar algum tempo. Pensar sexualmente no mesmo sexo e no sexo oposto é bastante comum quando os adolescentes resolvem seus sentimentos sexuais emergentes.

Alguns adolescentes podem experimentar experiências sexuais, incluindo com membros do mesmo sexo, à medida que exploram sua própria sexualidade. Mas essas experiências, por si mesmas, não significa necessariamente que um adolescente seja gay ou hetero. Para muitos adolescentes, essas experiências são simplesmente parte do processo de classificação de sua sexualidade emergente. E apesar estereótipos de gênero, traços masculinos e femininos não necessariamente predizem se alguém é hetero ou gay.

Uma vez cientes, alguns adolescentes gays podem se sentir bem e aceitar sua sexualidade, enquanto outros podem achar isso confuso ou difícil de aceitar.

## Como os adolescentes gays podem se sentir

Assim como seus colegas heterossexuais, os adolescentes gays podem se estressar com a escola, notas, faculdade, esportes, atividades, amigos e adaptação. Além disso, adolescentes gays e lésbicas muitas vezes lidam com uma camada extra de estresse - como se eles têm que esconder quem eles são, se eles serão assediados por serem gays, ou se enfrentarão estereótipos ou julgamentos se forem honestos sobre quem são.

Eles costumam se sentir diferentes de seus amigos quando há pessoas heterossexuais por perto eles começam a falar sobre sentimentos românticos, namoro e sexo. Para eles, pode sentir como se esperasse que todos fossem heterossexuais. Eles podem sentir que precisam fingir sentir coisas que eles não sentem para se encaixar. Eles podem sentir que precisam negar quem eles são ou escondem uma parte importante de si mesmos.

Muitos adolescentes gays se preocupam se serão aceitos ou rejeitados por seus entes queridos ou se as pessoas ficarão chateadas, com raiva ou desapontadas com eles. Esses medos de preconceito, discriminação, rejeição ou violência, pode levar alguns adolescentes que não são direto para manter sua orientação sexual em segredo, mesmo de amigos e familiares que pode ser de suporte.

Pode levar algum tempo para que os adolescentes gays processem como se sentem e aceitem esse aspecto de sua própria identidade antes de revelar sua orientação sexual a outras pessoas. Muitos decidir contar a alguns amigos e familiares que o apoiam e que o aceitam sobre seus orientação sexual. Isso é chamado de sair do armário.

Para a maioria das pessoas, assumir exige coragem. Em algumas situações, os adolescentes que são abertamente os gays podem correr o risco de sofrer mais assédio do que aqueles que não revelaram sua orientação sexual. Mas muitos adolescentes lésbicas, gays e bissexuais que assumem para seus amigos e familiares são totalmente aceitos por eles e suas comunidades. Eles se sentem confortáveis ​​e seguros sobre ser atraído por pessoas do mesmo sexo. Em uma pesquisa recente, adolescentes que relataram que se sentiam mais felizes e menos estressados ​​do que aqueles que não o fizeram.

## Como os pais podem se sentir

A adolescência é um período de transição não apenas para os adolescentes, mas também para seus pais. Muitos pais enfrentam a sexualidade emergente de seu filho com uma mistura de confusão e preocupação. Eles podem se sentir completamente despreparados para o próximo estágio da paternidade. E se o seu criança é gay, isso pode trazer um novo conjunto de perguntas e preocupações.

Alguns ficam surpresos ao saber a verdade, sempre pensando que seu filho era heterossexual. Outros se perguntam se a notícia é realmente verdadeira e se o filho adolescente tem certeza. Eles pode se perguntar se eles fizeram algo para tornar seu filho gay - mas eles não deveriam. Não há evidências de que ser gay é o resultado da maneira que alguém foi criado.

Felizmente, muitos pais de adolescentes gays entendem e aceitam desde o começo. Eles sentem que sempre souberam, mesmo antes de seu filho adolescente eles. Eles muitas vezes se sentem felizes que seus filhos decidiram confiar neles e são orgulhosos de seu filho por ter a coragem de contar a eles.

Outros pais ficam chateados, desapontados ou incapazes de aceitar a sexualidade de seu filho orientação em primeiro lugar. Eles podem estar preocupados ou preocupados se seu filho ou filha será intimidada, maltratada ou marginalizada. E eles podem se sentir protetores, preocupando-se que outros possam julgar ou rejeitar seu filho. Alguns também lutam para se reconciliar orientação sexual de seus filhos adolescentes com suas crenças religiosas ou pessoais. Infelizmente, alguns reaja com raiva, hostilidade ou rejeição.

Mas muitos pais descobrem que só precisam de tempo para se ajustar às notícias. Isso e onde grupos de apoio e outras organizações podem ajudar. Pode ser reconfortante para eles aprenda sobre pessoas assumidamente gays que levam uma vida feliz e bem-sucedida.

Com o tempo, até mesmo os pais que achavam que não podiam aceitar os filhos de seus filhos orientação sexual ficam surpresos ao descobrir que podem chegar a um ponto de compreensão.