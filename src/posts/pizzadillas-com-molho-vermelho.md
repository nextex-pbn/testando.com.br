---
title: Pizzadillas Com Molho Vermelho
permalink: pizzadillas-com-molho-vermelho
layout: post.hbs
---

# Pizzadillas Com Molho Vermelho

Nota: Estes são muito quentes na frigideira ou frio, embalados para o almoço em um recipiente de molho para mergulhar.

Tempo de preparação: 10-15 minutos

## O que você precisa:

* 4 tortilhas de trigo integral (8 polegadas)
* 2/3 xícara de espinafre fresco ou congelado, finamente picado
* 2/3 xícara de queijo mozzarella desnatado, ralado
* 1 xícara de molho marinara (comprado em loja ou caseiro)
* Spray de cozinha

Equipamentos e suprimentos:

* Uma frigideira de 10 a 12 polegadas
* Espátula fina (de preferência de metal)

## O que fazer:

1. Coloque 2 tortilhas em uma superfície plana. Divida o espinafre e o queijo entre as tortilhas.
2. Cubra com as 2 tortilhas restantes.
3. Coloque a frigideira em fogo médio. Cubra levemente a panela com spray de cozinha.
4. Deslize suavemente 1 pizzadilla na panela e cozinhe até dourar claro em uma lado (cerca de 1–2 minutos).
5. Usando uma espátula fina, vire suavemente a pizzadilla e cozinhe por 30-60 segundos mais ou até que o queijo esteja totalmente derretido.
6. Remova a pizzadilla e corte em triângulos. Repita com o restante da pizzadilla.
7. Sirva com molho marinara para mergulhar.
8. Você pode embrulhar e refrigerar as sobras.

## Análise nutricional (por porção):

* 170 calorias
* 10g de proteína
* 5g de gordura
* 2,5 g sat. gordura
* 27g de carboidrato
* fibra 4g
* 10 mg de colesterol
* 550 mg de sódio
* 3g de açúcar

Serve: 4

Tamanho da porção: meia pizzadilla

Observação: a análise nutricional pode variar dependendo das marcas de ingredientes usadas.