---
title: Autoestima do seu filho
permalink: autoestima-do-seu-filho
layout: post.hbs
---

# Autoestima do seu filho

Às vezes, é fácil perceber quando as crianças parecem se sentir bem consigo mesmas - e quando não o fazem. Muitas vezes descrevemos essa ideia de nos sentirmos bem conosco como "auto-estima."

Crianças com autoestima:

* se sinta querido e aceito
* sinta-se confiante
* sinta orgulho do que eles podem fazer
* pensam coisas boas sobre si mesmos
* acreditar em si mesmos

Crianças com baixa autoestima:

* são autocríticos e duros consigo mesmos
* acham que não são tão bons quanto as outras crianças
* pense nas vezes em que eles falham, em vez de quando eles têm sucesso
* falta de confiança
* duvido que eles possam fazer as coisas bem

## Por que a autoestima é importante

Crianças que se sentem bem consigo mesmas têm confiança para experimentar coisas novas. Eles são mais propensos a dar o melhor de si. Eles se sentem orgulhosos do que podem fazer. Auto estima ajuda as crianças a lidar com os erros. Ajuda as crianças a tentarem de novo, mesmo que falhem no início. Como resultado, a autoestima ajuda as crianças a se sairem melhor na escola, em casa e com os amigos.

Crianças com baixa autoestima se sentem inseguras. Se eles pensam que outros não vão aceitá-los, eles não podem participar. Eles podem deixar que outros os tratem mal. Eles podem têm dificuldade em se defender. Eles podem desistir facilmente, ou não tentar tudo. Crianças com baixa auto-estima têm dificuldade em lidar com quando cometem um erro, perdem, ou falhar. Como resultado, eles podem não ter um desempenho tão bom quanto poderiam.

## Como se desenvolve a autoestima

A auto-estima pode começar já na infância. Ele se desenvolve lentamente com o tempo. Pode comece apenas porque uma criança se sente segura, amada e aceita. Pode começar quando um bebê recebe atenção positiva e cuidado amoroso.

Conforme os bebês se tornam bebês e crianças pequenas, eles são capazes de fazer algumas coisas, por si próprios. Eles se sentem bem consigo mesmos quando podem usar suas novas habilidades. A autoestima deles aumenta quando os pais prestam atenção, deixam o filho experimentar, sorriem, e mostre que está orgulhoso.

À medida que as crianças crescem, a auto-estima também pode crescer. Sempre que as crianças tentam coisas, fazem coisas e aprender coisas pode ser uma chance para o crescimento da auto-estima. Isso pode acontecer quando as crianças:

* progredir em direção a uma meta
* aprender coisas na escola
* fazer amigos e se dar bem
* aprender habilidades - música, esportes, arte, culinária, habilidades tecnológicas
* praticar atividades favoritas
* ajude, dê ou seja gentil
* receba elogios por bom comportamento
* tente arduamente em algo
* fazer coisas nas quais eles são bons e desfrutar
* são incluídos por outros
* sinta-se compreendido e aceito
* ganhe um prêmio ou uma boa nota que eles sabem que conquistaram

Quando as crianças têm autoestima, elas se sentem confiantes, capazes e aceitas por quem eles são.

## Como os pais podem desenvolver autoestima

Cada criança é diferente. A auto-estima pode ser mais fácil para algumas crianças do que para outras. E algumas crianças enfrentam coisas que podem diminuir sua auto-estima. Mas mesmo se uma criança a auto-estima está baixa, pode ser aumentada.

Aqui estão algumas coisas que os pais podem fazer para ajudar os filhos a se sentirem bem consigo mesmos:

Ajude seu filho a aprender a fazer coisas. Em cada idade, existem novos coisas para as crianças aprenderem. Mesmo durante a infância, aprender a segurar um copo ou tomar primeiro passos desperta uma sensação de domínio e deleite. Conforme seu filho cresce, coisas como aprender vestir-se, ler ou andar de bicicleta são chances de aumentar a auto-estima.

Ao ensinar as crianças a fazer as coisas, mostre e ajude-as primeiro. Em seguida, deixe-os fazer o que puderem, mesmo que cometam erros. Certifique-se de que seu filho tenha uma chance de aprender, experimentar e sentir orgulho. Não torne os novos desafios muito fáceis - ou muito difícil.

Elogie seu filho, mas faça-o com sabedoria. Claro, é bom elogiar crianças. Seu elogio é uma forma de mostrar que você está orgulhoso. Mas algumas maneiras de elogiar as crianças pode realmente sair pela culatra.

Veja como fazer da maneira certa:

* Não elogie demais. O elogio que não parece merecido não toca verdade. Por exemplo, dizer a uma criança que ela jogou um ótimo jogo quando sabia que não parece vazio e falso. É melhor dizer: "Sei que não foi seu melhor jogo, mas todos nós temos dias de folga. Estou orgulhoso de você por não desistir. "Dê um voto de confiança: "Amanhã, você estará de volta ao seu jogo."
* Elogie o esforço. Evite focar elogios apenas em resultados (como obtendo um A) ou qualidades fixas (como ser inteligente ou atlético). elogie muito o esforço, o progresso e a atitude. Por exemplo: "Você é trabalhando duro nesse projeto, "" Você está ficando cada vez melhor com a ortografia testes, "ou" Estou orgulhoso de você por praticar piano - você realmente ficou com com este tipo de elogio, as crianças se esforçam nas coisas, trabalham para atingir os objetivos e experimentar. Quando as crianças fazem isso, é mais provável que tenham sucesso.

Seja um bom modelo. Quando você se esforça nas tarefas diárias (como varrer as folhas, fazer uma refeição, lavar a louça ou lavar o carro), você está dando um bom exemplo. Seu filho aprende a se esforçar para fazer a lição de casa, limpando brinquedos ou arrumando a cama.

Modelar a atitude certa também conta. Quando você executa tarefas alegremente (ou pelo menos sem resmungar ou reclamar), você ensina seu filho a fazer o mesmo. Quando você evita correndo nas tarefas domésticas e se orgulhando de um trabalho bem feito, você ensina seu filho a faça isso também.

Banir críticas severas. As mensagens que as crianças ouvem sobre si mesmas outros se traduzem facilmente em como se sentem sobre si mesmos. Palavras duras ("Você é tão preguiçoso! ") são prejudiciais, não motivam. Quando as crianças ouvem mensagens negativas sobre si mesmas, isso prejudica sua auto-estima. Corrija as crianças com paciência. Concentre-se no que você deseja para fazer na próxima vez. Quando necessário, mostre a eles como.

Concentre-se nos pontos fortes. Preste atenção ao que seu filho faz bem e goza. Certifique-se de que seu filho tenha chances de desenvolver esses pontos fortes. Foco mais nos pontos fortes do que nos pontos fracos, se você quiser ajudar as crianças a se sentirem bem consigo mesmas. Isso também melhora o comportamento.

Deixe as crianças ajudarem e doarem. A autoestima aumenta quando as crianças veem que o que eles fazem é importante para os outros. As crianças podem ajudar em casa, fazer um projeto de serviço na escola, ou faça um favor para um irmão. Atos amáveis ​​e de ajuda para aumentar a auto-estima e outros bons sentimentos.