---
title: Mordendo
permalink: mordendo
layout: post.hbs
---

# Mordendo

As crianças fazem as coisas mais adoráveis: dar abraços inesperados, gritar de tanto rir, e abraçar você quando estiverem cansados.

Mas, como qualquer pai de uma criança vai lhe dizer, eles também fazem algumas coisas não tão adoráveis coisas, como chutar, gritar ... ou morder.

Morder é bastante comum em crianças dessa idade, mas não é um grande consolo se você mordidas de criança. Afinal, ninguém quer que seu filho seja considerado a ameaça de o grupo de jogo. E pior ainda, as crianças que são rotuladas de "mordedores" podem ser expulsas de creches - um desafio que nenhum pai trabalhador deseja enfrentar.

Você pode pensar que morder é apenas mais uma fase pela qual terá de viver, mas isso é não necessariamente o caso. Existem maneiras de chegar ao fundo das mordidas de seu filho hábito. Veja como ajudar a conter esse tipo de comportamento.

## Por que crianças pequenas mordem

Morder é muito comum na primeira infância. Bebês e crianças pequenas mordem por uma variedade de motivos, como dentição ou explorar um novo brinquedo ou objeto com a boca. NOS eles começam a entender causa e efeito, eles também podem morder uma pessoa para ver se eles podem obter uma reação.

Morder também pode ser uma forma de as crianças chamarem a atenção ou expressarem o que estão sentindo. Frustração, raiva e medo são emoções fortes e as crianças não têm habilidades linguísticas para comunicar como eles estão se sentindo. Então, se eles não conseguem encontrar as palavras que precisam rapidamente o suficiente ou não podem dizer como estão se sentindo, eles podem morder como uma forma de dizer: "Preste atenção para mim! "ou" Não gosto disso! "

Morder é um pouco mais comum em meninos e tende a acontecer com mais frequência entre os primeiro e segundo aniversário. À medida que a linguagem melhora, as mordidas tendem a diminuir.

## Como podemos parar a mordida?

Com morder, é importante lidar com o comportamento imediatamente após ele acontecer. Na próxima vez que seu filho morder, tente estas etapas:

* Passo 1: Fique calmo e firme. Dirija-se ao seu filho com um firme "não morder!" ou "morder dói!" Mantenha-o simples e fácil para uma criança para entender. Deixe claro que morder é errado, mas evite longas explicações até que seu filho tenha idade suficiente para entender. Permanecer o mais calmo possível ajudará resolva a situação mais rapidamente.
* Passo 2: Conforte a vítima. Direcione sua atenção para a pessoa que foi mordida, especialmente se for outra criança. Se houver um ferimentos, limpe a área com água e sabão. Procure atendimento médico se a mordida for profunda ou sangrando.
* Passo 3: Conforte o mordedor, se necessário. Frequentemente, as crianças não percebem que morder dói. Não há problema em confortar uma criança que está sentindo chateado por machucar alguém. As crianças mais velhas podem aprender com a permissão para confortar seu amigo depois de uma mordida. Mas se o mordedor estiver usando o comportamento para chamar a atenção, você não quer reforçar esse comportamento dando conforto e atenção.
* Etapa 4: oferecer alternativas. Quando as coisas se acalmarem, sugira alternativas para morder, como usar as palavras "não", "pare" e "isso é meu" quando querer se comunicar com outras pessoas.
* Etapa 5: redirecionar. A distração faz maravilhas com crianças dessa idade. Se as emoções e os níveis de energia estão altos ou se o tédio se instalou, ajude a redirecionar a atenção de um pequeno para uma atividade mais positiva, como dançar ao som de música, colorir, ou jogando um jogo.

A disciplina geralmente não é necessária, já que a maioria das crianças não percebe que morder machuca. Nunca bata ou morda uma criança que tenha mordido, pois isso ensina a criança que esse comportamento é OK.

Se você tentou as etapas acima e o comportamento não para, os tempos limite podem ser eficaz. Crianças mais velhas podem ser levadas a uma área de tempo limite designada - uma cozinha cadeira ou escada inferior - por um ou dois minutos para se acalmar.

Como regra geral, cerca de 1 minuto por ano de idade é um bom guia para tempos limite. Tempos limites mais longos não trazem nenhum benefício adicional. Eles também podem prejudicar seus esforços se o seu criança se levanta (e se recusa a voltar) antes de você sinalizar que o tempo limite terminou.

## Criando um ambiente 'Bite-Free'

Quer você sinta que fez progresso com o hábito de morder de seu filho ou com ele continua a ser um trabalho em andamento, é importante criar uma cultura de tolerância zero em casa, creche e em outros lugares.

Aqui estão algumas maneiras de colocar seu filho de volta no caminho certo:

* Seja consistente. Reforce a regra "Proibido morder" em todos os momentos.
* Use reforço positivo. Em vez de recompensar ações negativas com atenção, elogie seu filho quando ele se comportar bem. Você pode usar afirmações como "Gosto de como você usou suas palavras" ou "Gosto de como você está brincar suavemente "para reforçar alternativas positivas para morder.
* Planeje com antecedência. As crianças podem ficar mais confortáveis ​​e não sentir o desejo de morder se eles sabem o que esperar em situações novas ou de alta energia. Se morder acontecer na creche, diga ao seu filho o que você deve esperar antes de ir. Se for maior, mais ambiente caótico parece opressor, você pode considerar colocar seu filho em um configuração menor.
* Encontre alternativas. Conforme as habilidades de linguagem se desenvolvem, você pode ajudar seu criança encontre melhores maneiras de expressar emoções negativas. Por exemplo, pedir às crianças que "usem suas palavras "quando estão frustrados ou chateados podem ajudar a acalmá-los. Se precisar de ajuda, um médico, conselheiro ou especialista em comportamento pode discutir maneiras de ensinar seu filho para gerenciar emoções fortes e expressar sentimentos de uma forma saudável.

## Quando devo chamar o médico?

Morder é comum em bebês e crianças pequenas, mas deve parar quando as crianças estão prestes 3 ou 4 anos. Se ultrapassar essa idade, for excessivo, parece estar piorando em vez de melhorar, e acontece com outros comportamentos desagradáveis, converse com seu filho médico. Juntos, vocês podem encontrar suas causas e maneiras de lidar com isso.