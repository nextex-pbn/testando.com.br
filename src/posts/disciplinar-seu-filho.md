---
title: Disciplinar seu filho
permalink: disciplinar-seu-filho
layout: post.hbs
---

# Disciplinar seu filho

Seja qual for a idade do seu filho, é importante ser consistente no que diz respeito à disciplina. Se os pais não seguirem as regras e consequências que estabeleceram, seus filhos não provavelmente também.

Aqui estão algumas idéias sobre como variar sua abordagem disciplinar para melhor se adequar ao seu família.

## Idades de 0 a 2

Bebês e crianças são naturalmente curiosos. Portanto, é aconselhável eliminar tentações e proibições - itens como TVs e equipamentos de vídeo, aparelhos de som, joias, e, especialmente, produtos de limpeza e remédios devem ser mantidos bem fora do alcance.

Quando o seu bebê engatinhando ou criança errante se encaminha para uma situação inaceitável ou perigosa brincar de objeto, diga calmamente "Não" e remova seu filho da área ou distraia ele ou ela com uma atividade apropriada.

Os limites de tempo podem ser uma disciplina eficaz para crianças pequenas. Uma criança que está batendo, mordendo ou jogar comida, por exemplo, deve ser informado por que o comportamento é inaceitável e tomado para uma área de tempo limite designada - uma cadeira de cozinha ou escada inferior - para um um ou dois minutos para se acalmar (tempos limite mais longos não são eficazes para crianças pequenas).

É importante não bater, bater ou esbofetear uma criança de qualquer idade. Bebês e crianças são especialmente improváveis ​​de serem capazes de fazer qualquer conexão entre seu comportamento e Punimento físico. Eles sentirão apenas a dor do golpe.

E não se esqueça de que as crianças aprendem observando os adultos, principalmente os pais. Certifique-se de que seu comportamento seja um modelo de comportamento. Você deixará uma impressão muito mais forte guardando seus próprios pertences, em vez de apenas dar ordens ao seu filho para pegar brinquedos enquanto suas coisas são deixadas espalhadas.

## De 3 a 5 anos

À medida que seu filho cresce e começa a entender a conexão entre as ações e Consequências, certifique-se de começar a comunicar as regras da casa de sua família.

Explique às crianças o que você espera delas antes de puni-las para um comportamento. A primeira vez que seu filho de 3 anos usa giz de cera para decorar a sala parede da sala, discuta por que isso não é permitido e o que acontecerá se seu filho fizer de novo (por exemplo, seu filho terá que ajudar a limpar a parede e não será capaz de usar os lápis pelo resto do dia). Se a parede for decorada novamente alguns dias depois, emita um lembrete de que giz de cera é apenas para papel e, em seguida, reforce as consequências.

Quanto mais cedo os pais estabelecerem esse tipo de "Eu defino as regras e você é esperado ouvir ou aceitar as consequências "padrão, melhor para todos às vezes é mais fácil para os pais ignorar o mau comportamento ocasional ou não seguir através de alguma ameaça de punição, isso abre um precedente ruim. Ameaças vazias minam sua autoridade como pai, e torna mais provável que os filhos testem os limites. Consistência é a chave para uma disciplina eficaz, e é importante que os pais decidam (juntos, se você não for um pai solteiro) quais são as regras e, em seguida, respeite-as.

Enquanto você esclarece quais comportamentos serão punidos, não se esqueça de recompensar bons comportamentos. Não subestime o efeito positivo que seu elogio pode ter - Disciplina não é apenas punição, mas também reconhecimento do bem comportamento. Por exemplo, dizer "Estou orgulhoso de você por compartilhar seus brinquedos no playgroup" geralmente é mais eficaz do que punir uma criança que não compartilhou. E seja específico ao elogiar, em vez de apenas dizer "Bom trabalho!" Você quer deixar claro qual os comportamentos que você gostou. Isso os torna mais prováveis ​​de acontecer no futuro - o quanto mais atenção dermos a um comportamento, maior será a probabilidade de ele continuar.

Se seu filho continuar a ter um comportamento inaceitável, não importa o que você faça, tente um gráfico com uma caixa para cada dia da semana. Decida quantas vezes seu filho pode comportar-se mal antes de uma punição ser aplicada ou por quanto tempo o comportamento adequado deve ser observado antes de ser recompensado. Poste o gráfico na geladeira e acompanhe o comportamentos bons e inaceitáveis ​​todos os dias. Isso dará ao seu filho (e a você) uma visão concreta veja como está indo. Assim que começar a funcionar, elogie seu filho por aprender a controlar o mau comportamento e, especialmente, para superando qualquer problema teimoso.

Os limites de tempo também podem funcionar bem para crianças dessa idade. Escolha um local de tempo limite adequado, como uma cadeira ou degrau, está livre de distrações. Lembre-se, sendo enviado para a sua sala não é eficaz se houver um computador, TV ou jogos. Além disso, um tempo limite está longe de qualquer tipo de reforço. Portanto, seu filho não deve receber nenhuma atenção de você durante um intervalo - incluindo conversa, contato visual, etc.

Certifique-se de considerar o período de tempo que funcionará melhor para seu filho. Especialistas dizer que 1 minuto para cada ano de idade é uma boa regra prática; outros recomendam usar o intervalo até a criança se acalmar (para ensinar autorregulação). Certifique-se de que se o tempo limite acontecer porque seu filho não seguiu as instruções, você segue com a direção após o tempo limite.

É importante dizer às crianças qual é a coisa certa a fazer, não apenas dizer o que a coisa errada é. Por exemplo, em vez de dizer "Não pule no sofá", tente "Por favor, sente-se na mobília e coloque os pés no chão."

Certifique-se de dar comandos claros e diretos. Em vez de "Você poderia por favor colocar seus sapatos está ligado? "diga" Por favor, coloque os sapatos. "Isso não deixa espaço para confusão e não implica que as instruções a seguir são uma escolha.

## De 6 a 8 anos

Limites de tempo e consequências também são estratégias de disciplina eficazes para esta idade grupo.

Mais uma vez, a consistência é crucial, assim como o acompanhamento. Cumpra todas as promessas de disciplina ou então você corre o risco de minar sua autoridade. As crianças têm que acreditar nisso você quer dizer o que você diz. Isso não quer dizer que você não pode dar uma segunda chance ou permitir uma certa margem de erro, mas na maior parte, você deve agir de acordo com o que diz.

Tenha cuidado para não fazer ameaças irreais de punição ("Bata essa porta e você nunca mais assista TV! ") com raiva, já que não seguir até o fim pode enfraquecer todos suas ameaças. Se você ameaçar virar o carro e ir para casa se a briga no banco de trás não para, certifique-se de fazer exatamente isso. A credibilidade que você ganhar com seus filhos é muito mais valioso do que um dia perdido na praia.

Punições pesadas podem tirar seu poder como pai. Se você de castigo seu filho ou filha por um mês, seu filho pode não se sentir motivado a mudar de comportamento porque tudo já foi levado embora. Pode ajudar definir alguns objetivos que as crianças pode se reunir para ganhar de volta privilégios que foram retirados por mau comportamento.

## De 9 a 12 anos

Crianças nessa faixa etária - assim como em todas as idades - podem ser disciplinadas com consequências naturais. À medida que amadurecem e exigem mais independência e responsabilidade, ensiná-los a lidar com as consequências de seu comportamento é um método eficaz e método apropriado de disciplina.

Por exemplo, se o dever de casa do seu quinto ano não for feito antes de dormir, você o faz ficar acordado para fazê-lo ou até mesmo lhe dar uma mão? Provavelmente não - você perderá a oportunidade de ensinar uma lição importante de vida. Se o dever de casa estiver incompleto, seu filho irá para a escola no dia seguinte sem ele e sofrerá o mal resultante nota.

É natural que os pais queiram resgatar os filhos dos erros, mas no longo prazo correm, eles fazem um favor às crianças, deixando-as falhar às vezes. As crianças veem o que está se comportando inadequadamente pode significar e provavelmente não cometerá esses erros novamente. No entanto, se o seu criança não parece estar aprendendo com as consequências naturais, configure alguns de seus própria para ajudar a mudar o comportamento. Remover privilégios como eletrônicos pode ser conseqüência efetiva para esta faixa etária.

## A partir de 13 anos

Você já lançou as bases. Seu filho sabe o que é esperado e que você significa o que você diz sobre as penalidades por mau comportamento. Não baixe a guarda agora - a disciplina é tão importante para os adolescentes quanto para os mais novos. Somente como acontece com a criança de 4 anos que precisa que você defina uma hora de dormir e a aplique, seu filho adolescente precisa limites também.

Estabeleça regras sobre dever de casa, visitas de amigos, toque de recolher e namoro e discuta de antemão com seu filho adolescente para que não haja mal-entendidos. Seu adolescente provavelmente reclamará de vez em quando, mas também perceberá que você está no controle. Acredite ou não, os adolescentes ainda querem e precisam que você estabeleça limites e faça cumprir a ordem suas vidas, mesmo que você conceda a eles maior liberdade e responsabilidade.

Quando seu filho infringe uma regra, tirar privilégios pode parecer o melhor plano de ação. Embora seja bom tirar o carro por uma semana, por exemplo, não se esqueça de também discutir por que voltar para casa uma hora após o toque de recolher é inaceitável e preocupante.

Lembre-se de dar ao adolescente algum controle sobre as coisas. Não será apenas este limite o número de lutas pelo poder que você tem, ajudará seu filho a respeitar as decisões que você precisa fazer. Você pode permitir que um adolescente mais jovem tome decisões sobre roupas da escola, estilos de cabelo ou até mesmo a condição de seu quarto. Como seu adolescente fica mais velho, esse domínio de controle pode ser estendido para incluir um ocasional relaxamento toque de recolher.

Também é importante focar nos aspectos positivos. Por exemplo, faça com que seu filho ganhe um toque de recolher posterior, demonstrando um comportamento positivo em vez de definir um toque de recolher mais cedo como punição por comportamento irresponsável.

## Uma palavra sobre palmadas

Talvez nenhuma forma de disciplina seja mais controversa do que surras. Aqui estão alguns motivos pelos quais os especialistas desencorajam a surra:

* Espancar ensina as crianças que não há problema em bater quando estão com raiva.
* Bater pode prejudicar fisicamente as crianças.
* Em vez de ensinar as crianças a mudar seu comportamento, as palmadas os deixam com medo de seus pais e os ensina a evitar serem pegos.
* Para crianças que buscam atenção atuando, espancar pode "recompensá-las" - negativo atenção é melhor do que nenhuma atenção.