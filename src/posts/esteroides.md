---
title: Esteróides
permalink: esteroides
layout: post.hbs
---

# Esteróides

É importante compreender os fatos sobre esteróides, seus efeitos colaterais e o que pode levar crianças e adolescentes a experimentá-los. Estar ciente dos tipos de pressão que as crianças lidar com esportes pode ajudá-lo a garantir que seu filho não corre risco.

## O que são esteróides?

Os medicamentos comumente chamados de "esteróides" são classificados como corticosteróides ou anabólico (ou anabólico-androgênico) esteróides.

### Corticosteróides

Corticosteroides, como a cortisona, são medicamentos prescritos pelos médicos para ajudar no controle inflamação. Eles são usados ​​para ajudar a controlar condições como a asma e lúpus. Eles não são iguais como esteróides anabolizantes.

### Esteróides anabolizantes

Os esteróides anabolizantes são hormônios sintéticos que podem aumentar a capacidade do corpo de produzir músculo e prevenir o colapso muscular.

Alguns atletas tomam esteróides na esperança de melhorar sua capacidade de corra mais rápido, bata mais longe, levante pesos mais pesados, pule mais alto ou tenha mais resistência. Nos Estados Unidos, é contra a lei usar esteróides anabolizantes sem receita.

Androstenediona, ou "andro", é um tipo de esteróide anabolizante administrado por atletas que quer construir músculos. Agora é uma substância controlada devido a suspeitas de saúde riscos e disponível apenas por prescrição. Há pouca ou nenhuma evidência de que tenha quaisquer efeitos anabólicos significativos.

## Por que as pessoas usam esteróides?

Alguns jogadores profissionais de beisebol, ciclistas e estrelas do atletismo foram acusados de - e em alguns casos admitiu - usar esteróides para dar-lhes uma vantagem competitiva.

O uso de esteróides também atingiu os atletas mais jovens, que enfrentam a pressão para ser mais forte e mais rápido, e chegar às ligas universitárias e profissionais.

Os esteróides prometem resultados ousados, mas há poucas provas de que proporcionem tais benefícios. Mas eles podem prejudicar crianças em desenvolvimento - com alguns destes efeitos nocivos provavelmente só aparecerão anos depois.

## Como funcionam os esteróides anabolizantes?

Os esteróides anabolizantes são drogas que se assemelham à estrutura química do hormônio sexual testosterona, que é produzida naturalmente pelo corpo. A testosterona dirige o corpo para fazer ou melhorar as características masculinas, como aumento da massa muscular, crescimento do cabelo e aprofundamento da voz, e é uma parte importante do desenvolvimento masculino durante a puberdade.

Quando os esteróides anabolizantes aumentam os níveis de testosterona no sangue, eles estimulam tecido muscular no corpo para crescer mais e mais forte. No entanto, os efeitos de também muita testosterona circulando no corpo pode ser prejudicial ao longo do tempo.

## O que São os perigos dos esteróides anabólicos?

Os esteróides são perigosos por dois motivos: eles são ilegais e podem danificar um saúde da pessoa, especialmente se usado em grandes doses ao longo do tempo. Além disso, os problemas de saúde causados ​​por esteróides podem não aparecer até anos após a ingestão dos esteróides.

Embora possam ajudar a construir músculos, os esteróides podem ter efeitos colaterais muito graves. O uso de esteróides por muito tempo pode prejudicar o sistema reprodutivo. Na preguiça, esteróides podem levar à impotência, uma redução na quantidade de espermatozóides produzidos no testículos e até mesmo tamanho reduzido dos testículos.

Mulheres que usam esteróides podem ter problemas com seus ciclos menstruais porque os esteróides podem interromper a maturação e liberação de óvulos dos ovários. Isso pode causar problemas de fertilidade a longo prazo.

Esteróides ingeridos por um longo período de tempo também podem causar:

* crescimento atrofiado em adolescentes (fazendo com que os ossos amadureçam muito rápido e parem de crescer em uma idade precoce)
* tumores de fígado
* aumento anormal dos músculos do coração
* comportamento violento e agressivo e alterações de humor
* anormalidades lipídicas do sangue que contribuem para doenças cardíacas
* acne (ou agravamento da acne)
* aumento do crescimento dos seios em homens, especialmente adolescentes
* estrias irreversíveis
* uma tendência acentuada para queda de cabelo e calvície de padrão masculino
* dores musculares

Meninas e mulheres adolescentes correm o risco desses efeitos colaterais adicionais:

* crescimento de pelos faciais e corporais de tipo masculino e calvície de padrão masculino
* aprofundamento da voz
* aumento do clitóris

## O que mais pode acontecer?

Além dos riscos para a saúde, crianças que usam esteróides sem receita médica estão quebrando a lei. O teste de drogas para todos os atletas se tornou comum, e aqueles que falham em uma droga teste de esteróides pode enfrentar consequências legais, incluindo pena de prisão, multas monetárias, sendo banido de um evento ou equipe, ou perda de troféus ou medalhas.

O uso do Andro foi proibido por muitas organizações esportivas, incluindo a International Comitê Olímpico, Liga Nacional de Futebol, Associação Nacional de Basquete, a National Collegiate Athletic Association, a Association of Tennis Professionals, e a maioria das associações de atletas do ensino médio.

## Conversando com crianças sobre esteróides

Muitas pressões podem levar jovens atletas a experimentarem esteróides. Embora a maioria dos atletas exercite-se muito, alimente-se adequadamente e cuide do corpo para atingir a boa forma e o desempenho objetivos, a pressão para se destacar e o desejo de parecer fisicamente tonificado e em forma podem ser intenso.

Ajude seus filhos a lidar com essas pressões:

* discutindo competição saudável com eles
* falando sobre as atitudes dos treinadores e membros da equipe em relação aos esteróides
* saber em que tipo de ambiente esportivo eles competem
* incentivando-os a se prepararem mental e fisicamente para a competição comendo bem e descansando o suficiente

Fique atento a estes sinais de alerta de abuso de esteróides:

* mudanças de humor exageradas
* agravamento da acne
* pele excepcionalmente oleosa com estrias
* um aumento repentino no tamanho do músculo

Se você notar algum desses sinais em seu filho, converse com seu médico. Esteróides podem dão aos jovens atletas a sensação de que são mais fortes e mais atléticos, mas os riscos são muito perigosos.

Quando o uso de esteróides entre atletas profissionais é notícia, use-o como uma forma de discutir o assunto, certificando-se de que seu filho compreenda a saúde riscos, a possibilidade de problemas legais e o conceito de que o uso de esteróides é uma forma de trapaça.