---
title: Transtorno obsessivo-compulsivo
permalink: transtorno-obsessivo-compulsivo
layout: post.hbs
---

# Transtorno obsessivo-compulsivo

## O que é TOC?

O transtorno obsessivo-compulsivo (TOC) é uma condição que faz com que as crianças tenham pensamentos, sentimentos e medos. Estes são chamados de obsessões, e eles pode fazer as crianças ficarem ansiosas. Para aliviar as obsessões e a ansiedade, o TOC leva as crianças fazer comportamentos chamados de compulsões (também chamados de rituais).

### O que são obsessões?

Obsessões são medos nos quais as crianças com TOC não param de pensar. Eles podem perceber que seus pensamentos não fazem sentido, mas ainda se sentem ansiosos sobre certas coisas.

Esses medos podem incluir:

* eles, ou outra pessoa, ficarão doentes, machucados ou morrerão
* eles disseram um palavrão, tiveram um pensamento ruim ou cometeram um erro
* eles quebraram uma regra, fizeram uma coisa ruim ou pecaram
* algo está limpo, sujo ou com germes
* algo é reto, uniforme ou colocado de maneira exata
* algo é sortudo ou azarado, ruim ou bom, seguro ou prejudicial

### O que são compulsões?

Compulsões (rituais) são comportamentos repetidos por crianças com TOC. O TOC faz com que as crianças sintam que precisam fazer rituais para "ter certeza" de que as coisas estão limpo, seguro, em ordem, ou apenas certo. Para crianças com TOC, os rituais parecem ter o poder de evitar que coisas ruins aconteçam.

Os rituais incluem coisas como:

* lavar e limpar
* frequentemente apagando coisas, reescrevendo, refazendo ou relendo
* repetir uma palavra, frase ou pergunta muito mais do que o necessário
* entrando e saindo de portas várias vezes seguidas
* verificar se a luz está apagada, se a porta está trancada ou verificar e verificar novamente lição de casa
* tocar ou tocar um determinado número de vezes ou de uma forma definida
* ter as coisas em uma ordem específica
* contar até um certo número 'bom', evitando números "azarados"

## Por que as pessoas têm TOC?

Os cientistas ainda não sabem por que as pessoas têm TOC, mas conhecem fatores biológicos desempenhar um papel. As crianças podem ter TOC porque está em seus genes ou eles tiveram uma infecção. Pode haver diferenças nas estruturas cerebrais e na atividade cerebral em pessoas com TOC. Mas o que quer que tenha causado o TOC, não é culpa da criança ou dos pais.

## Como é para crianças com TOC?

As crianças nem sempre falam sobre os medos e comportamentos que o TOC causa. Eles podem sentir envergonhado ou confuso sobre seu medo e mantê-lo para si. Eles podem tentar esconder os rituais que eles fazem. Eles podem se preocupar que os outros vão provocá-los sobre seus medos e rituais.

Crianças com TOC não conseguem parar de se concentrar em suas obsessões. Eles parecem eles têm que fazer os rituais para se proteger contra coisas ruins que temem que possam acontecer. Para algumas crianças, fazer um ritual é a única maneira de sentirem que "está tudo bem".

## O que Os pais podem notar?

Muitas crianças têm TOC por um tempo antes que seus pais, professores ou médicos percebam. Os pais só podem aprender sobre o TOC se o filho contar a eles ou se eles perceberem a criança parece muito preocupada ou está praticando comportamentos que parecem rituais.

Às vezes, os pais podem notar outras dificuldades que podem ser resultado do TOC. Para exemplo, o TOC pode fazer com que as crianças:

* têm dificuldade para se concentrar nos trabalhos escolares ou desfrutar das atividades
* sentir-se e agir de forma irritável, chateada, triste ou ansiosa
* parece não ter certeza se as coisas estão bem
* têm problemas para decidir ou escolher
* demoram muito para fazer as tarefas diárias, como se vestir, organizar uma mochila, fazendo a lição de casa ou tomando banho
* ficam chateados e perdem a paciência se não conseguem fazer algo perfeito ou se algo está fora do lugar
* insista para que um dos pais diga ou faça algo da maneira exata

## Como o TOC é diagnosticado?

Para diagnosticar o TOC, você se encontrará com um psicólogo infantil ou psiquiatra, que irá entreviste você e seu filho para saber mais detalhes. Você e seu filho também podem preencher checklists e questionários. Isso vai ajudar o psicólogo ou psiquiatra faça um diagnóstico. Não há testes de laboratório para diagnosticar o TOC.

Quando o TOC está sendo diagnosticado, pode ser um alívio para as crianças e os pais. OCD pode melhorar com a atenção e o cuidado certos.

## Como o TOC é tratado?

O TOC é tratado com medicamentos e terapia. Para crianças que precisam de remédios, médicos dê SSRIs (inibidores seletivos da recaptação da serotonina), como Zoloft, Prozac e Luvox.

Os terapeutas tratam o TOC com terapia cognitivo-comportamental. Durante esse tipo de conversa e conversa terapia, as crianças aprendem sobre o TOC e começam a entendê-lo melhor. Eles aprendem isso fazendo os rituais mantêm o TOC forte, e não fazer rituais ajuda a enfraquecê-lo. Eles aprenda maneiras de enfrentar os medos, lidar com eles e resistir a fazer rituais. Aprendendo estes habilidades ajudam a interromper o ciclo de TOC.

Parte do tratamento consiste em treinar os pais sobre como podem ajudar os filhos a melhorar. Pais aprender como responder a situações de TOC e como apoiar o progresso de seus filhos sem ceder a rituais.

## O que os pais podem fazer?

Converse com seu filho sobre o que está acontecendo. Fale de forma favorável, escute e mostre amor. Diga algo que funcione para a situação do seu filho, como: "Eu. note que você se preocupa com as cobertas lisas, as meias uniformes e os sapatos alinhados. Percebo que você fica estressado quando não consegue consertar as coisas do jeito certo. "

Digamos que algo chamado OCD pode estar causando a preocupação e a correção. Diga o seu criança que um checkup com um médico pode descobrir se é isso que está acontecendo. Tranquilizar seu filho que isso pode melhorar e que você quer ajudar.

Marque uma consulta com um psiquiatra ou psicólogo infantil. O médico do seu filho pode ajudá-lo a encontrar a pessoa certa.

Participe da terapia do seu filho. Aprenda tudo que puder sobre como os pais podem ajudar quando seu filho tem TOC. Superar o TOC é um processo. Haverá Há muitas consultas de terapia e é importante comparecer a todas. Pratique as coisas o terapeuta recomenda. Incentive seu filho.

Obtenha apoio e dê-o. Existem muitos recursos e suporte para pais e famílias que lidam com o TOC. Saber que você não está sozinho pode ajudar você lida. Compartilhar histórias de sucesso com outros pais pode lhe dar esperança e confiança.