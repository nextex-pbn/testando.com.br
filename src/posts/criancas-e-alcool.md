---
title: Crianças e álcool
permalink: criancas-e-alcool
layout: post.hbs
---

# Crianças e álcool

Por mais que os pais não gostem de pensar nisso, a verdade é que muitas crianças e adolescentes experimentam álcool durante o ensino médio e os anos de faculdade, muito antes de legal para eles beberem. A pesquisa mostrou que quase 80% dos alunos do ensino médio já experimentou álcool.

Embora a experimentação com álcool possa ser comum entre as crianças, não é seguro ou legal. Portanto, é importante começar a discutir o uso e abuso de álcool com seus filhos em uma idade precoce e continue falando sobre isso enquanto eles crescem.

## Os efeitos do abuso de álcool

O álcool interfere na percepção da realidade de uma pessoa e na capacidade de fazer o bem decisões. Isso pode ser particularmente perigoso para crianças e adolescentes que têm menos soluções para problemas e experiência em tomada de decisão.

Os efeitos de curto prazo do consumo de álcool incluem:

* visão, audição e coordenação distorcidas
* percepções e emoções alteradas
* julgamento prejudicado, o que pode levar a acidentes, afogamento e outros comportamentos de risco como sexo inseguro e uso de drogas
* mau hálito
* ressacas

Os efeitos de longo prazo incluem:

* cirrose e câncer de fígado
* perda de apetite
* deficiências graves de vitaminas
* doenças de estômago
* danos ao coração e ao sistema nervoso central
* perda de memória
* um risco aumentado de impotência
* alto risco de overdose

Muito antes de seus filhos terem a chance de beber álcool, você pode aumentar as chances de eles simplesmente dizerem "não".

A infância é uma época de aprendizagem e descoberta, por isso é importante incentivar as crianças para fazer perguntas, mesmo aquelas que podem ser difíceis de responder. Aberto, honesto, adequado à idade a comunicação agora prepara o terreno para seus filhos virem até você mais tarde com outras dificuldades tópicos ou problemas.

## Conversando com crianças sobre álcool### Pré-escolares

Embora as crianças de 3 e 4 anos não estejam prontas para aprender os fatos sobre o álcool ou outros drogas, eles começam a desenvolver as habilidades de tomada de decisão e resolução de problemas que irão precisa mais tarde. Você pode ajudá-los a desenvolver essas habilidades de algumas maneiras simples.

Por exemplo, deixe que as crianças escolham suas próprias roupas e não se preocupe se as escolhas não combinam. Isso permite que eles saibam que você acha que eles são capazes de tomar boas decisões. Atribua tarefas simples e diga às crianças que elas são uma grande ajuda.

E dê um bom exemplo do comportamento que deseja que seus filhos demonstrem. Isso é especialmente verdadeiro nos anos pré-escolares, quando as crianças tendem a imitar as ações dos adultos como forma de aprendizagem. Então, sendo ativo, comendo saudável e bebendo com responsabilidade, os pais ensinam lições importantes aos filhos desde o início.

### De 4 a 7 anos

Crianças desta idade ainda pensam e aprendem principalmente com a experiência e não têm uma boa compreensão das coisas que acontecerão no futuro. Portanto, mantenha as discussões sobre álcool no tempo presente e relacione-os com coisas que as crianças sabem e entendem. Por exemplo, assistir TV com seu filho pode dar uma chance de falar sobre publicidade mensagens. Pergunte sobre os anúncios que você vê e incentive as crianças a fazerem perguntas também.

As crianças estão interessadas em como seus corpos funcionam, então é um bom momento para falar sobre manter a boa saúde e evitar substâncias que possam prejudicar o corpo. Falar sobre como o álcool prejudica a capacidade de uma pessoa de ver, ouvir e andar sem tropeçar; isso altera a maneira como as pessoas se sentem; e torna difícil julgar coisas como se a água é muito profundo ou se houver um carro chegando muito perto. E isso dá às pessoas mau hálito e uma dor de cabeça!

### De 8 a 11 anos

Os últimos anos do ensino fundamental são um período crucial em que você pode influenciar decisões de seu filho sobre o uso de álcool. Crianças nessa idade tendem a adorar aprender fatos, especialmente os estranhos, e estão ansiosos para aprender como as coisas funcionam e quais fontes de informações estão disponíveis para eles.

Portanto, é um bom momento para discutir abertamente os fatos sobre o álcool: seu longo e curto prazo efeitos e consequências, seus efeitos físicos e por que é especialmente perigoso para corpos em crescimento.

As crianças também podem ser fortemente influenciadas pelos amigos agora. Seus interesses podem ser determinados pelo que seus pares pensam. Então ensine seu filho a dizer "não" à pressão dos colegas e discuta a importância de pensar e agir como um indivíduo.

Discussões casuais sobre álcool e amigos podem ocorrer na mesa de jantar como parte de sua conversa normal: "Tenho lido sobre crianças que usam álcool. Você já ouviu falar de crianças que usam álcool ou outras drogas na sua escola? "

### De 12 a 17 anos

Na adolescência, seus filhos devem saber os fatos sobre o álcool e suas atitudes e crenças sobre o abuso de substâncias. Portanto, use este tempo para reforçar o que você já ensinei-os e concentre-se em manter as linhas de comunicação abertas.

Os adolescentes são mais propensos a se envolver em comportamentos de risco e sua necessidade cada vez maior de a independência pode levá-los a desafiar os desejos ou instruções dos pais. Mas se você faz seu filho se sentir aceito e respeitado como indivíduo, você aumenta o chances de que seu filho tente ser aberto com você.

As crianças querem ser queridas e aceitas pelos colegas e precisam de um certo grau de privacidade e confiança. Evite pregações e ameaças excessivas e, em vez disso, enfatize seu amor e preocupação. Mesmo quando estão incomodados com o interesse e as perguntas dos pais, os adolescentes ainda reconhecem que isso vem com o território.

## Ensino Crianças devem dizer "não"

Ensine às crianças uma variedade de abordagens para lidar com ofertas de álcool:

* Incentive-os a fazer perguntas. Se uma bebida de qualquer tipo for oferecida, eles devem pergunte: "O que é?" e "Onde você conseguiu isso?"
* Ensine-os a dizer "não, obrigado" quando a bebida oferecida for alcoólica.
* Lembre-os de deixar qualquer situação desconfortável. Certifique-se de que eles tenham dinheiro para transporte ou um número de telefone onde você ou outro adulto responsável possa ser encontrado.
* Ensine as crianças a nunca aceitar carona de alguém que bebeu. Alguns pais descobrir aquela oferta para resgatar seus filhos em uma situação desconfortável - não perguntas feitas - ajuda a incentivar as crianças a serem honestas e ligar quando precisarem ajuda.

## Fatores de risco

Tempos de transição, como o início da puberdade ou o divórcio dos pais, podem levar crianças ao uso de álcool. Então ensine a seus filhos que, mesmo quando a vida é perturbadora ou estressante, beber álcool como uma fuga pode tornar uma situação muito pior.

Crianças com problemas de autocontrole ou baixa autoestima têm maior probabilidade de abuso de álcool. Eles podem não acreditar que podem lidar com seus problemas e frustrações sem usar algo para fazer com que se sintam melhor.

Crianças sem um senso de conexão com suas famílias ou que se sentem diferentes de alguma forma (aparência, circunstâncias econômicas, etc.) também pode estar em risco. Essa que acham difícil acreditar em si mesmos precisam desesperadamente do amor e do apoio de pais ou outros membros da família. Na verdade, não querendo prejudicar as relações entre eles próprios e os adultos que se preocupam com eles é a razão mais comum que os jovens as pessoas dão por não usar álcool e outras drogas.

## Dicas gerais

Felizmente, os pais podem fazer muito para proteger seus filhos do uso e abuso de álcool:

* Seja um bom modelo. Considere como o uso de álcool ou medicamentos pode influenciar seus filhos. Considere oferecer apenas bebidas não alcoólicas em festas e outras atividades sociais eventos para mostrar aos seus filhos que você não precisa beber para se divertir.
* Eduque-se sobre o álcool para que possa ser um professor melhor. Leia e colete informações que você pode compartilhar com crianças e outros pais.
* Tente estar ciente de como você pode ajudar a construir a autoestima de seu filho. Por exemplo, é mais provável que as crianças se sintam bem consigo mesmas se você enfatizar seus pontos fortes e reforçar positivamente o comportamento saudável.
* Ensine as crianças a administrar o estresse de maneira saudável, por exemplo, buscando a ajuda de um especialista adulto ou participando de uma atividade favorita.

## Reconhecendo os sinais

Apesar de seus esforços, seu filho ainda pode usar - e abusar - de álcool. Como você sabe? Aqui estão alguns sinais de alerta comuns:

* o odor de álcool
* mudança repentina de humor ou atitude
* mudança na frequência ou desempenho na escola
* perda de interesse na escola, esportes ou outras atividades
* problemas de disciplina na escola
* Retirada da família e amigos
* sigilo
* associação com um novo grupo de amigos e relutância em apresentá-los a você
* álcool desaparecendo de sua casa
* depressão e dificuldades de desenvolvimento

É importante não tirar conclusões precipitadas com base em apenas um ou dois sinais. Adolescência é um momento de mudança - física, social, emocional e intelectualmente. Isso pode levar a um comportamento errático e mudanças de humor enquanto as crianças tentam lidar com todos os essas mudanças.

Se o seu filho está usando álcool, geralmente haverá um agrupamento desses sinais, como mudanças de amigos, comportamento, vestido, atitude, humor e notas. Se você ver um número de mudanças, procure todas as explicações conversando com seus filhos, mas não negligencie abuso de substâncias como uma possibilidade.

Outras dicas para tentar:

* Fique de olho nos lugares aonde seus filhos vão.
* Conheça os pais dos amigos de seus filhos.
* Sempre verifique se você tem um número de telefone onde possa falar com seu filho.
* Peça às crianças que façam check-in regularmente quando estiverem longe de casa.
* Quando ficar muito tempo longe de você, seu filho deve verificar em periodicamente com uma ligação, e-mail, texto ou visita a casa.

Para adolescentes, especialmente aqueles com idade suficiente para dirigir, considere negociar e assinar um contrato comportamental. Este contrato deve definir a forma como você espera que seu filho comportar-se e declarar as consequências se seu filho adolescente dirigir sob o efeito de álcool. Segue e retire as chaves do carro, se necessário.

Faça parte do acordo com seu filho adolescente de que você e o resto de sua família também concorde em nunca beber e dirigir. Incentive também comportamentos responsáveis, como planejamento para um motorista designado ou chamar um adulto para obter ajuda em vez de dirigir sob o influência.

É importante manter a comunicação aberta e as expectativas razoáveis. Amarração responsável ações para a liberdade, como um toque de recolher tardio ou uma carteira de motorista podem ser uma poderosa motivador. Ensine a seus filhos que a liberdade só vem com responsabilidade - uma lição isso deve durar uma vida inteira.